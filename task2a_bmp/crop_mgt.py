import os, sys
import pandas as pd

path = 'I:\\projects\\dairy_cap\\dairy_bmp\\run2\\inputs\\NY_TwinBirch_FertUpdate_OnlyLagoon_9yrs.dnd'

file = open(path, 'r')
lines = file.readlines()
file.close()

crop_data = False
found_crop = False
rows = []
for linenum, line in enumerate(lines):
    
    if "Crop_data" in line:
        crop_data = True
    
    if '____Year' in line and crop_data:
        found_crop = True
        headers = []
        row = []

    if crop_data and found_crop:
        items = line.strip().split()
        header,value = items
        value = float(value)
        
        new_header = header.replace('_',' ')
        new_header = new_header.strip()
        new_header = new_header.replace(' ','_')

        row.append(value)
        headers.append(new_header)
    
    if '______None' in line and found_crop:
        rows.append(row)
        found_crop = False
        
df = pd.DataFrame(rows, columns=headers)
df.to_csv('crop_management.csv', index=False)