#!/usr/bin/env python

import os, sys, copy
import datetime as dt
import lines.dndc as dndc
import lines.manuredndc as manuredndc

LINE_LEN = 72
PATH_DNDC_C = 'C:\\DNDC\\project'
PATH_MANUREDNDC_C = 'C:\\ManureDNDC\\project'

def line_length(line):
    
    # correct line length
    ll = len(line)
    if ll < LINE_LEN:
        line = line.replace(' ',' '*(LINE_LEN - ll))
    
    return line

def make_line(line,d):
    
    # replace line variables
    line = line.format(**d)
    
    # correct length
    line = line_length(line)
    
    return line

def make_lines(text_l,d):
    
    dnd_l = []
    
    for line in text_l:
        line = make_line(line,d)
        dnd_l.append(line)
    
    return dnd_l

class Dnd_File:

    def __init__(self,keys,years,project,data_d):
        
        # define DND properties
        self.site       = keys['site']
        # self.scen       = keys['scenario']
        self.soil       = keys['soil']
        self.system     = keys['system']
        self.part       = keys['part']
        self.data       = data_d
        self.project    = project
        
        # define total years
        # years_init = self.data['scenario'][self.scen]['years_init']
        # self.years_init = years_init
        # year_start = self.data['scenario'][self.scen]['year_cal_start']
        # year_end = self.data['scenario'][self.scen]['year_cal_end']
        
        # self.years = year_end - year_start + 1 + years_init
        # self.data['scenario'][self.scen]['years'] = self.years

        self.years      = years[2]

        # equate simulation years and calendar years
        
        # get scenario years info
        # year_cal_start = self.data['scenario'][self.scen]['year_cal_start']
        # year_cal_end = self.data['scenario'][self.scen]['year_cal_end']

        year_cal_start = years[0]
        year_cal_end   = years[1]
        
        # add all summary years to list
        year_l = []
        for year in xrange(year_cal_start, year_cal_end+1):
            year_l.append(year)

        # calculate number of summary years
        years_sum = year_cal_end - year_cal_start + 1
        
        # repeat intialization years
        init_list = year_l[0:years_init]

        # extend the list of years
        year_l = init_list + year_l

        # add to dictionary
        d = {}
        for num,year_cal in enumerate(year_l):
            year_sim = num + 1
            d[year_sim] = year_cal
        self.year_d = d

        
        return

    def site_lines(self):

        # create site info dict
        site_info_d = {}
    
        # pull out data
        site_info_d['site_name'] = self.dnd_name()

        site_info_d['years'] = self.years
        site_info_d['lat'] = self.data['site'][self.site]['lat']
        site_info_d['daily'] = self.data['site'][self.site]['daily']
        site_info_d['units'] = self.data['site'][self.site]['site_units']
        
        return make_lines(dndc.SITE,site_info_d)

    def clim_lines(self):

        # set "clim_files" variable (always equal to "years")
        self.data['site'][self.site]['clim_files'] = self.years
    
        # make climate lines, part 1
        clim_lines = make_lines(dndc.CLIMATE1,self.data['site'][self.site])
        
        # get climate file ID
        try:
            clim_id = int(self.data['site'][self.site]['clim_id'])
        except KeyError:
            clim_id = copy.deepcopy(self.site)
        
        # create list of climate files
        for year in xrange(1,self.years+1):
            year_cal = self.year_d[year]
            path_clim = os.path.join(
                PATH_DNDC_C + '\\' + 
                self.project + '\\' + 
                'weather' + '\\' + 
                str(year_cal) + '\\' + 
                str(clim_id) + '.txt')
            
            line = str(year) + ' ' + path_clim
            clim_lines.append(line_length(line))
        
        # make climate lines, part 2
        clim_lines.extend(
            make_lines(dndc.CLIMATE2,self.data['site'][self.site]))
        
        return clim_lines
        
    def soil_lines(self):
        
        # convert site data to soil dictionary
        soil_d = copy.deepcopy(self.data['soil'][self.site,self.soil])

        # add land-use code to soil dictionary
        soil_d['lu_code'] = self.data['site'][self.site]['lu_code']
        
        # extract clay fraction
        clay = soil_d['clay']

        # extract anncillary soil data from soil table
        for texture in self.data['soil_lu']:
            clay_min = self.data['soil_lu'][texture]['clay_min']
            clay_max = self.data['soil_lu'][texture]['clay_max']
            
            if clay > clay_min and clay <= clay_max:
                soil_d['texture']       = self.data['soil_lu'][texture]['texture']
                soil_d['texture_class'] = self.data['soil_lu'][texture]['texture_class']
                # soil_d['field_cap']     = self.data['soil_lu'][texture]['field_cap']      # USE DB-SPECIFIED VALUES
                # soil_d['wilt_pt']       = self.data['soil_lu'][texture]['wilt_pt']        # USE DB-SPECIFIED VALUES
                # soil_d['hyd_con']       = self.data['soil_lu'][texture]['hyd_con']        # USE DB-SPECIFIED VALUES
                soil_d['porosity']      = self.data['soil_lu'][texture]['porosity']
                break
        
        # make soil lines
        soil_lines = make_lines(
            dndc.SOIL,
            soil_d)
        
        return soil_lines

    def crop_lines(self):

        crop_lines = []

        # initial crop info
        crop_lines.extend(
            make_lines(dndc.CROP_DATA,{'rotations':self.years}))

        # loop through years
        for year in range(1,self.years+1):

            year_cal = self.year_d[year]
        
            # get field key
            key_field = (self.system, self.part, year_cal)
            
            # set rotations
            crop_lines.extend(
                make_lines(dndc.CROP_ROTATIONS,{
                    'rotation': year,
                    'years':    1,
                    'cycles':   1}))
            
            # set cycles
            crops = int(self.data['field'][key_field]['crops'])
            crop_lines.extend(
                make_lines(dndc.CROP_CYCLE,{
                    'cycle': 1,
                    'crops': crops}))

            # loop through crops
            for crop in range(1,crops+1):
                
                # initiate crop dictionary
                crop_d = {'crop':crop}
                
                # get crop data based on crop prefix (cp)
                cp = 'crop_' + str(crop) + '_'

                id_db = self.data['field'][key_field][cp + 'id_db']

                plant_date = self.data['field'][key_field][cp + 'plant_date']
                harv_date = self.data['field'][key_field][cp + 'harv_date']
                
                plant_day = plant_date.day
                plant_mo = plant_date.month
                harv_day = harv_date.day 
                harv_mo = harv_date.month
                
                if np.isnan(harv_mo): harv_mo = 12
                if np.isnan(harv_day): harv_day = 31
                
                crop_d['plant_day'] = plant_day
                crop_d['plant_mo'] = plant_mo
                crop_d['harv_day'] = harv_day
                crop_d['harv_mo'] = harv_mo
                
                plant_dt = dt.date(2000,plant_mo,plant_day)
                harv_dt = dt.date(2000,harv_mo,harv_day)
                
                if harv_dt < plant_dt:
                    crop_d['harv_year'] = 2
                else:
                    crop_d['harv_year'] = 1
                    
                crop_d['cover'] = self.data['field'][key_field][cp + 'cover']
                crop_d['residue'] = self.data['field'][key_field][cp + 'residue']
                
                # combine crop record with crop dict
                for key,value in self.data['crop'][id_db].iteritems():
                    
                    if key == 'max_bm':
                        max_bm = value
                    
                    if key == 'frac_grain':
                        frac_grain = value
                    
                    crop_d[key] = value
                
                # convert maximum biomass to maximum yield
                max_yield = max_bm * frac_grain
                
                crop_d['max_yield'] = max_yield

                crop_lines.extend(
                    make_lines(dndc.CROPS,crop_d))

            # tillage
            crop_lines.extend(self.till_lines(year_cal))
            
            # fertilization
            crop_lines.extend(self.fert_lines(year_cal))
            
            # manure
            crop_lines.extend(self.manu_lines(year_cal))
            
            # mulch
            crop_lines.extend(self.mulch_lines(year_cal))
            
            # flooding
            crop_lines.extend(self.flood_lines(year_cal))
            
            # irrigation
            crop_lines.extend(self.irri_lines(year_cal))
            
            # grazing
            crop_lines.extend(self.graze_lines(year_cal))

            # cutting
            crop_lines.extend(self.cut_lines(year_cal))
        
        crop_lines.extend(dndc.END_OF_FILE)
        
        return crop_lines
        
    def till_lines(self, year_cal):

        # get field key
        key_field = (self.system, self.part, year_cal)
        
        tillages = int(self.data['field'][key_field]['tillages'])
        till_lines = make_lines(dndc.TILL,{'tillages':tillages})

        for till in range(1,tillages+1):
        
            # get till data based on crop prefix (cp)
            tp = 'till_' + str(till) + '_'
            
            date = self.data['field'][key_field][tp + 'date']
            day = date.day
            mo = date.month

            # set depth
            depth = self.data['field'][key_field][tp + 'depth']

            # determine closest category
            depth_d = {
                0:1,
                5:2,
                10:3,
                20:4,
                30:5,
                1000:7}
            # don't really understand the following, but it works...
            #http://stackoverflow.com/questions/12141150/from-list-of-integers-get-number-closest-to-a-given-value
            cat = min(depth_d, key=lambda x:abs(x-depth))
            depth_code = depth_d[cat]

            till_lines.extend(
                make_lines(dndc.TILLS,{
                    'till':till,
                    'day':day,
                    'mo':mo,
                    'depth_code':depth_code}))

            # look for termination
            # add another event if necessary
            if self.data['field'][key_field][tp + 'terminator'] == 1:
                print 'GOT HERE'
                depth = 1000
                cat = min(depth_d, key=lambda x:abs(x-depth))
                depth_code = depth_d[cat]

                till_lines.extend(
                    make_lines(dndc.TILLS,{
                        'till':till,
                        'day':day,
                        'mo':mo,
                        'depth_code':depth_code}))
            else:
                continue
        
        return till_lines

    def fert_lines(self, year_cal):

        # get field key
        key_field = (self.system, self.part, year_cal)
    
        ferts = self.data['field'][key_field]['fertilizations']
        fert_lines = make_lines(dndc.FERT,{'fertilizations':ferts})

        for fert in xrange(1,int(ferts)+1):
            
            fert_d = {}
            
            fert_d['fert'] = fert
            
            # get fert data based on crop prefix (fp)
            fp = 'fert_' + str(fert) + '_'
            
            date = self.data['field'][key_field][fp + 'date']
            fert_d['day'] = date.day
            fert_d['mo'] = date.month
            depth = self.data['field'][key_field][fp + 'depth']
            
            if depth == 0.0:
                fert_d['meth'] = 0
                fert_d['depth'] = 0.2
            else:
                fert_d['meth'] = 1
                fert_d['depth'] = depth
            
            # get fertilizer type and amount
            fert_type = self.data['field'][key_field][fp + 'type'].lower()
            fert_amt = self.data['field'][key_field][fp + 'amt']

            fert_d['nit']       = self.data['fert'][fert_type]['nit'] * fert_amt
            fert_d['ammbic']    = self.data['fert'][fert_type]['ammbic'] * fert_amt
            fert_d['urea']      = self.data['fert'][fert_type]['urea'] * fert_amt
            fert_d['anh']       = self.data['fert'][fert_type]['anh'] * fert_amt
            fert_d['amm']       = self.data['fert'][fert_type]['amm'] * fert_amt
            fert_d['sulf']      = self.data['fert'][fert_type]['sulf'] * fert_amt
            fert_d['phos']      = self.data['fert'][fert_type]['phos'] * fert_amt
            
            for k,v in self.data['field'][key_field].iteritems():
                fert_d[k] = v
            
            fert_lines.extend(
                make_lines(dndc.FERTS,fert_d))
        
        fert_lines.extend(
            make_lines(dndc.FERT_OPTION,{'fert_option':0}))
        
        return fert_lines

    def manu_lines(self, year_cal):

        # get field key
        key_field = (self.system, self.part, year_cal)
        
        # get manure data for the year...
        manus = self.data['field'][key_field]['manures']

        # initiate manure lines
        # manus = len(manures)
        manu_lines = make_lines(dndc.MANU,{'manus':manus})
        
        for manu in xrange(1,int(manus)+1):
            
            # get manure data based on prefix (mp)
            mp = 'manu_' + str(manu) + '_'
            
            # manure dictionary
            manu_d = {'application':manu}

            # get application date
            date = self.data['field'][key_field][mp + 'date']
            manu_d['day'] = date.day
            manu_d['mo'] = date.month

            # retrieve data
            manu_d['date'] =    self.data['field'][key_field][mp + 'date']
            manu_d['type'] =    self.data['field'][key_field][mp + 'type']
            manu_d['dm'] =      self.data['field'][key_field][mp + 'dm']
            manu_d['orgc'] =    self.data['field'][key_field][mp + 'orgc']
            manu_d['totaln'] =  self.data['field'][key_field][mp + 'totaln']
            manu_d['cn'] =      self.data['field'][key_field][mp + 'cn']
            manu_d['orgn'] =    self.data['field'][key_field][mp + 'orgn']
            manu_d['nh4'] =     self.data['field'][key_field][mp + 'nh4']
            manu_d['no3'] =     self.data['field'][key_field][mp + 'no3']
            manu_d['meth'] =    self.data['field'][key_field][mp + 'meth']
            manu_d['depth'] =   self.data['field'][key_field][mp + 'depth']
            
            # make lines
            manu_lines.extend(
                make_lines(dndc.MANUS,manu_d))

        return manu_lines
    
    def mulch_lines(self, year_cal):
        
        ''' NOT FULLY IMPLEMENTED '''
        return make_lines(dndc.MULCH,{})
        
    def flood_lines(self, year_cal):
    
        ''' NOT FULLY IMPLEMENTED '''
        return make_lines(dndc.FLOOD,self.data['field'][(self.system,self.part,year_cal)])
    
    def irri_lines(self, year_cal):

        # get field key
        key_field = (self.system, self.part, year_cal)
        
        # check for irrigation use
        irri_use = int(self.data['field'][key_field]['irri_use'])
        
        # get irrigation info
        if irri_use == 1:
            irris = int(self.data['field'][key_field]['irris'])
            irri_type = int(self.data['field'][key_field]['irri_type'])
            irri_index = int(self.data['field'][key_field]['irri_index'])
            irri_meth = int(self.data['field'][key_field]['irri_meth'])
        else:
            irris = 0
            irri_type = 0 
            irri_index = 0.0
            irri_meth = 0
        
        irri_d = {
            'irris':irris,
            'irri_type':irri_type,
            'irri_index':irri_index,
            'irri_meth':irri_meth}
        
        irri_lines = make_lines(dndc.IRRI,irri_d)
        
        # get application info
        for irri in range(1,irris+1):
            
            # get irri data based on prefix (ip)
            ip = 'irri_' + str(irri) + '_'
            
            date = self.data['field'][key_field][ip + 'date']
            day = date.day
            mo = date.month
            amt = self.data['field'][key_field][ip + 'amt']
            meth = self.data['field'][key_field][ip + 'meth']
            
            app_d = {
                'irri':irri,
                'mo':mo,
                'day':day,
                'amt':amt,
                'meth':meth}
            
            irri_lines.extend(
                make_lines(dndc.IRRIS,app_d))
        
        return irri_lines

    def graze_lines(self, field, mgt_year):
        
        ''' IMPLEMENTED ONLY FOR DAIRY COWS + REPLACEMENTS '''
        
        # get field key
        key_field = (self.site, self.scen, field, mgt_year)
    
        grazes = self.data['field'][key_field]['grazes']
        graze_lines = make_lines(dndc.GRAZE,{'grazes':grazes})

        for graze in xrange(1,int(grazes)+1):
            
            graze_d = {}
            
            graze_d['graze'] = graze
            
            # get grazing data based on graze prefix (gp)
            gp = 'graze_' + str(graze) + '_'
            
            date_start = self.data['field'][key_field][gp + 'start']
            date_end = self.data['field'][key_field][gp + 'end']
            graze_d['day_start'] = date_start.day
            graze_d['mo_start'] = date_start.month
            graze_d['day_end'] = date_end.day
            graze_d['mo_end'] = date_end.month
            
            # get grazing population
            graze_d['head_dairy']   = self.data['field'][key_field][gp + 'head_dairy']
            graze_d['head_replace'] = self.data['field'][key_field][gp + 'head_replace']
            
            # get additional grazing parameters
            graze_d['hours']   = self.data['field'][key_field][gp + 'hours']
            graze_d['feed']    = self.data['field'][key_field][gp + 'feed']
            graze_d['feed_cn'] = self.data['field'][key_field][gp + 'feed_cn']
            graze_d['excreta'] = self.data['field'][key_field][gp + 'excreta']

            graze_lines.extend(
                make_lines(dndc.GRAZES,graze_d))

        return graze_lines

    def cut_lines(self, year_cal):

        ''' NOTE: IMPLEMENTED SO THAT THE WHOLE PLANT IS CUT ABOVE 
                  SOME HEIGHT (I.E. ALL GRAIN IS CUT, AND THE GIVEN
                  CUT FRACTION IS TAKEN FROM LEAF AND STEM) '''
    
        # get field key
        key_field = (self.system, self.part, year_cal)

        cuts = int(self.data['field'][key_field]['cuts'])
        
        # double the cuts
        cuts = cuts * 2
        
        cut_lines = make_lines(dndc.CUT,{'cuts':cuts})
        
        for cut in range(1,cuts+1):

            cut_d = {'cut':cut}

            # get cut data based on crop prefix (cp)
            cp = 'cut_' + str((cut + 1) / 2) + '_'

            # on evens, cut 100% of grain
            if cut % 2 == 0:
                cut_d['frac'] = 1.0
                cut_d['part'] = 1   # only grain
            
            # on odds (actual applications),
            # cut leaf and stem by actual fraction
            else:
                frac = self.data['field'][key_field][cp + 'frac']
                cut_d['frac'] = frac
                cut_d['part'] = 8   # leaf & stem

            date = self.data['field'][key_field][cp + 'date']
            cut_d['day'] = date.day
            cut_d['mo'] = date.month
                
            cut_lines.extend(
                make_lines(dndc.CUTS,cut_d))
        
        return cut_lines 
    
    def dnd_lines(self):
        
        dnd_lines = self.site_lines()
        dnd_lines.extend(self.clim_lines())
        dnd_lines.extend(self.soil_lines())
        dnd_lines.extend(self.crop_lines())
        
        return dnd_lines
    
    def write_dnd(self, path_file):

        file = open(path_file,'wb')
        for line in self.dnd_lines():
            file.write('{0}\r\n'.format(line))
        file.close()
        
        return
    
    def dnd_name(self,filename=False):

        dnd_name = (
            str(self.site) + '-' + 
            str(self.soil) + '-' + 
            str(self.system) + '-' + 
            str(self.part))
        
        if filename:
            dnd_name = (
                dnd_name + 
                '.dnd')                
        
        return dnd_name
    
    def dnd_path(self):
        
        dnd_path = os.path.join(
            PATH_DNDC_C + '\\' + 
            self.project + '\\' + 
            'inputs' + '\\' + 
            str(self.system) + '\\' + 
            self.dnd_name(filename=True))
        
        return dnd_path


class ManureDnd_File:

    def __init__(self,site,clim,scen,project,data_d):
        
        # define DND properties
        self.site = int(site)
        self.clim = int(clim)
        self.scen = int(scen)
        self.data = data_d
        self.project = project
        
        # count fields by site
        #   Yep, this is awful
        if site == 1:
            self.fields = 6 # NY (4 IFSM fields + soy field + manure export field)
        if site == 2:
            self.fields = 5 # WI (4 IFSM fields + soy field)
        
        # define total years
        years_init = self.data['climate'][self.clim]['years_init']
        year_start = self.data['climate'][self.clim]['year_cal_start']
        year_end = self.data['climate'][self.clim]['year_cal_end']
        
        self.years = year_end - year_start + 1 + years_init
        self.data['scenario'][self.scen]['years'] = self.years
        
        # equate simulation years and calendar years
        
        # get climate years info
        year_cal_start = self.data['climate'][self.clim]['year_cal_start']
        year_cal_end = self.data['climate'][self.clim]['year_cal_end']
        
        # add all summary years to list
        year_l = []
        for year in xrange(year_cal_start, year_cal_end+1):
            year_l.append(year)

        # calculate number of summary years
        years_sum = year_cal_end - year_cal_start + 1
        
        # repeat intialization years
        init_list = year_l[0:years_init]

        # extend the list of years
        year_l = init_list + year_l

        # add to dictionary
        d = {}
        for num,year_cal in enumerate(year_l):
            year_sim = num + 1
            d[year_sim] = year_cal
        self.year_d = d

        return

    def site_lines(self):

        # create site info dict
        site_info_d = {}
    
        # pull out data
        site_info_d['site_name'] = self.dnd_name()
        site_info_d['years'] = self.years
        site_info_d['lat'] = self.data['site'][self.site]['lat']
        site_info_d['daily'] = self.data['scenario'][self.scen]['daily']
        site_info_d['units'] = self.data['site'][self.site]['site_units']
        
        return make_lines(manuredndc.SITE,site_info_d)
    
    def ls_lines(self):

        # get years
        year_cal_start = self.data['climate'][self.clim]['year_cal_start']

        # count feedlots
        feedlots = 0
        for key in sorted(self.data['feedlot'].keys()):
            key_site,key_scen,key_fl = key
            if self.site == key_site and self.scen == key_scen:
                feedlots+=1
        
        # start livestock lines
        ls_lines = make_lines(manuredndc.LS,self.data['scenario'][self.scen])
        
        for year in range(1,self.years+1):

            # add year line
            ls_lines.append(make_line(manuredndc.LS_YEAR[0],{'year':year}))
            
            # add feedlots line
            ls_lines.append(make_line(manuredndc.LS_FEEDLOT[0],{'feedlots':feedlots}))
            
            for feedlot in range(1,feedlots+1):
                
                # get feedlot key
                key_fl = (self.site,self.scen,feedlot)

                # get feedlot data
                ls_d = copy.deepcopy(self.data['feedlot'][key_fl])

                # get feedlot type
                feedlot_type = self.data['feedlot'][key_fl]['feedlot_type']
                
                # get feedlot input type
                feedlot_input = self.data['feedlot'][key_fl]['feedlot_input']

                # if specifying herd characteristics...
                if feedlot_input == 0:
                    ls_lines = ls_lines + make_lines(manuredndc.LS_FEEDLOTS,ls_d)
                
                # if specifying herd file...
                if feedlot_input == 1:
                
                    # set C path
                    input_file_name = os.path.join(
                        PATH_DATA_C,
                        self.project,
                        'herd',
                        str(self.herd),
                        herd_file_name)
                    
                    ls_d['input_file_name'] = input_file_name
                
                    ls_lines = ls_lines + make_lines(manuredndc.LS_FEEDLOTS,ls_d)

                # if specifying excretion file...
                if feedlot_input == 2:

                    # set C path
                    filename = feedlot_type + '_' + str(self.site) + '.txt'
                    input_file_name = os.path.join(
                        PATH_MANUREDNDC_C,
                        self.project,
                        'excretion',
                        str(self.scen),
                        filename)
                    
                    ls_d['input_file_name'] = input_file_name
                    
                    ls_lines = ls_lines + make_lines(manuredndc.LS_FEEDLOT_INPUTS,ls_d)

        return ls_lines
        
    def stor_lines(self, stor_type):

        # get storage key
        key_stor = (self.site,self.scen)

        # get variables based on storage type
        if stor_type == 'lagoon':
            lines = manuredndc.LAG
            removals_txt = 'lag_removals'
            lines_removals = manuredndc.LAG_REMOVAL
            
            # fix lagoon rainfall line
            if self.data['storage'][key_stor]['lag_rain'] == 'yes':
                self.data['storage'][key_stor]['lag_rain'] = 0
            else:
                self.data['storage'][key_stor]['lag_rain'] = 1
            
            # manure distribution fields
            dist_prefix = 'lr_'
            dist_l = [
                'lag2field',
                'lag2dig',
                'lag2remain',
                'meth',
                'depth']
            
        if stor_type == 'compost':
            lines = manuredndc.COMP
            removals_txt = 'comp_removals'
            lines_removals = manuredndc.COMP_REMOVAL
            
            # manure distribution fields
            dist_prefix = 'cr_'
            dist_l = [
                'comp2field',
                'comp2market',
                'comp2remain']

        # create storage lines
        stor_lines = make_lines(lines,self.data['storage'][key_stor])

        # get removals
        removals = int(self.data['storage'][key_stor][removals_txt])

        # loop through removals
        for removal in range(1,removals+1):

            # initiate dictionary
            d = {}

            # set field prefix based on removal #
            r = dist_prefix + str(int(removal)) + '_'
            
            # extract data
            d['removal'] = removal
            date = self.data['storage'][key_stor][r + 'date']
            d['mo'] = date.month
            d['day'] = date.day
            for dist in dist_l:
                d[dist] = self.data['storage'][key_stor][r + dist]

            stor_lines = stor_lines + make_lines(lines_removals,d)
        
        return stor_lines
    
    def dig_lines(self):

        # start storage lines
        key_stor = (self.site,self.scen)
        
        return make_lines(manuredndc.DIG,self.data['storage'][key_stor])
    
    def land_lines(self):
        
        # count fields
        # fields = self.data['scenario'][self.scen]['fields']
        # fields = 1 # REPLACED THIS FOR MANUREDNDC DUMMY FIELDS...
        fields = self.fields
        
        # get total area and build a text list of areas
        area_tot = 0.0
        area_text = ' '
        for field in range(1,fields+1):
            key_fields = (self.site, self.scen, field, 1)
            area = self.data['field'][key_fields]['area']
            area = round(area, 4)
            area_tot+=area
            area_text = area_text + ' ' + str(area)
        
        # make land lines
        d = {}
        d['area_total'] = area_tot
        d['fields'] = fields
        d['years'] = self.years
        d['area_text'] = area_text
        land_lines = make_lines(manuredndc.LAND, d)

        # loop through years and fields
        field_lines = []
        for year in xrange(1,self.years+1):
            frac_text = ' '
            for field in range(1,fields+1):
                key_fields = (self.site, self.scen, field, 1)
                manu_frac = self.data['field'][key_fields]['manu_frac']

                frac_text = frac_text + ' ' + str(round(manu_frac, 4))

            d = {}
            d['year']      = year
            d['manu_frac'] = frac_text
            field_lines.extend(make_lines(manuredndc.FIELD, d))

        return land_lines + field_lines

    def clim_lines(self):

        # set "clim_files" variable (always equal to "years")
        self.data['site'][self.site]['clim_files'] = self.years
    
        # make climate lines, part 1
        clim_lines = make_lines(manuredndc.CLIMATE1,self.data['site'][self.site])
        
        # get climate dataset site name
        clim_site = self.data['site'][self.site]['clim_site']
        
        # create list of climate files
        for year in xrange(1,self.years+1):
            year_cal = self.year_d[year]
            path_clim = (
                PATH_MANUREDNDC_C + '\\' + 
                self.project + '\\' + 
                'weather' + '\\' + 
                str(self.clim) + '\\' + 
                str(year_cal) + '\\' + 
                clim_site + '.txt')
            
            line = str(year) + ' ' + path_clim
            clim_lines.append(line_length(line))
        
        # make climate lines, part 2
        clim_lines.extend(
            make_lines(manuredndc.CLIMATE2,self.data['site'][self.site]))
        
        return clim_lines
        
    def soil_lines(self):
        
        # convert site data to soil dictionary
        soil_d = copy.deepcopy(self.data['site'][self.site])
        
        # extract clay fraction
        clay = soil_d['clay']

        # extract anncillary soil data from soil table
        for texture in self.data['soil']:
            clay_min = self.data['soil'][texture]['clay_min']
            clay_max = self.data['soil'][texture]['clay_max']
            
            if clay > clay_min and clay <= clay_max:
                soil_d['texture'] = self.data['soil'][texture]['texture']
                soil_d['texture_class'] = self.data['soil'][texture]['texture_class']
                soil_d['field_cap'] = self.data['soil'][texture]['field_cap']
                soil_d['wilt_pt'] = self.data['soil'][texture]['wilt_pt']
                soil_d['hyd_con'] = self.data['soil'][texture]['hyd_con']
                soil_d['porosity'] = self.data['soil'][texture]['porosity']
                break
        
        # make soil lines
        soil_lines = make_lines(
            manuredndc.SOIL,
            soil_d)
        
        return soil_lines

    def crop_lines(self):

        # get fields
        # fields = self.data['scenario'][self.scen]['fields']
        # fields = 1 # REPLACED THIS FOR MANUREDNDC DUMMY FIELDS...
        fields = self.fields
        
        # initialize crop lines list
        crop_lines = []

        # loop through fields
        for field in range(1,fields+1):
            
            # get field type
            field_type = self.data['field'][self.site,self.scen,field,1]['field_type']
            
            # add in "cycle_years" variable (always same as "years")
            self.data['scenario'][self.scen]['cycle_years'] = self.years
            
            # initial crop info
            crop_lines.extend(
                make_lines(manuredndc.CROP_DATA,self.data['scenario'][self.scen]))

            # loop through years
            for year in range(1,self.years+1):
                
                # set management year based on field # and year
                if field_type == 'alfalfa':
                    if year%4==1: mgt_year=1
                    if year%4==2: mgt_year=2
                    if year%4==3: mgt_year=3
                    if year%4==0: mgt_year=4
                elif field_type == 'grass':
                    if year == 1: mgt_year=1
                    if year > 1:  mgt_year=2
                else:
                    mgt_year = 1

                # get field key
                key_field = (self.site,self.scen,field,mgt_year)

                ### REMOVED THIS FOR DUMMY FIELDS ###
                '''
                mgt_year = 1
                if field == 1:  # ALFALFA
                    if (year-1) % 5 == 0:
                        mgt_year = 1
                    else:
                        mgt_year = 2
                
                if field == 2:  # HAY
                    if year == 1:
                        mgt_year = 1
                    else:
                        mgt_year = 2
                '''
                ### REMOVED THIS FOR DUMMY FIELDS ###
                

                # crops
                crops = int(self.data['field'][key_field]['crops'])
                crop_lines.extend(
                    make_lines(manuredndc.CROP,{'year':year,'crops':crops}))

                # loop through crops
                for crop in range(1,crops+1):
                    
                    # initiate crop dictionary
                    crop_d = {'crop':crop}
                    
                    # get crop data based on crop prefix (cp)
                    cp = 'crop_' + str(crop) + '_'

                    id_db = self.data['field'][key_field][cp + 'id_db']

                    plant_date = self.data['field'][key_field][cp + 'plant_date']
                    harv_date = self.data['field'][key_field][cp + 'harv_date']
                    
                    plant_day = plant_date.day
                    plant_mo = plant_date.month
                    harv_day = harv_date.day 
                    harv_mo = harv_date.month
                    
                    crop_d['plant_day'] = plant_day
                    crop_d['plant_mo'] = plant_mo
                    crop_d['harv_day'] = harv_day
                    crop_d['harv_mo'] = harv_mo
                    
                    plant_dt = dt.date(2000,plant_mo,plant_day)
                    harv_dt = dt.date(2000,harv_mo,harv_day)
                    
                    if harv_dt < plant_dt:
                        crop_d['harv_year'] = 2
                    else:
                        crop_d['harv_year'] = 1
                        
                    crop_d['cover'] = self.data['field'][key_field][cp + 'cover']
                    crop_d['residue'] = self.data['field'][key_field][cp + 'residue']
                    
                    # combine crop record with crop dict
                    for key,value in self.data['crop'][id_db].iteritems():
                        
                        if key == 'max_bm':
                            max_bm = value
                        
                        if key == 'frac_grain':
                            frac_grain = value
                        
                        crop_d[key] = value
                    
                    # convert maximum biomass to maximum yield
                    max_yield = max_bm * frac_grain
                    
                    crop_d['max_yield'] = max_yield

                    crop_lines.extend(
                        make_lines(manuredndc.CROPS,crop_d))

                # tillage
                crop_lines.extend(self.till_lines(field, mgt_year))
                
                # fertilization
                crop_lines.extend(self.fert_lines(field, mgt_year))
                
                # manure
                crop_lines.extend(self.manu_lines())
                
                # mulch
                crop_lines.extend(self.mulch_lines())
                
                # flooding
                crop_lines.extend(self.flood_lines())
                
                # irrigation
                crop_lines.extend(self.irri_lines(field, mgt_year))
                
                # grazing
                crop_lines.extend(self.graze_lines(field, mgt_year))

                # cutting
                crop_lines.extend(self.cut_lines(field, mgt_year))

        return crop_lines
        
    def till_lines(self, field, mgt_year):

        # get field key
        key_field = (self.site,self.scen,field,mgt_year)
        
        tillages = int(self.data['field'][key_field]['tillages'])
        till_lines = make_lines(manuredndc.TILL,{'tillages':tillages})

        for till in range(1,tillages+1):
            # get till data based on crop prefix (cp)
            tp = 'till_' + str(till) + '_'
            
            date = self.data['field'][key_field][tp + 'date']
            day = date.day
            mo = date.month
            depth = self.data['field'][key_field][tp + 'depth']
            
            # look for termination
            if self.data['field'][key_field][tp + 'terminator'] == 1:
                depth = 1000
            
            # determine closest category
            depth_d = {
                0:1,
                5:2,
                10:3,
                20:4,
                30:5,
                1000:7}
            # don't really understand the following, but it works...
            #http://stackoverflow.com/questions/12141150/from-list-of-integers-get-number-closest-to-a-given-value
            cat = min(depth_d, key=lambda x:abs(x-depth))
            depth_code = depth_d[cat]

            till_lines.extend(
                make_lines(manuredndc.TILLS,{
                    'till':till,
                    'day':day,
                    'mo':mo,
                    'depth_code':depth_code}))

        return till_lines

    def fert_lines(self, field, mgt_year):

        # get field key
        key_field = (self.site,self.scen,field,mgt_year)
    
        ferts = self.data['field'][key_field]['fertilizations']
        fert_lines = make_lines(manuredndc.FERT,{'fertilizations':ferts})

        for fert in xrange(1,int(ferts)+1):
            
            fert_d = {}
            
            fert_d['fert'] = fert
            
            # get fert data based on crop prefix (fp)
            fp = 'fert_' + str(fert) + '_'
            
            date = self.data['field'][key_field][fp + 'date']
            fert_d['day'] = date.day
            fert_d['mo'] = date.month
            depth = self.data['field'][key_field][fp + 'depth']
            
            if depth == 0.0:
                fert_d['meth'] = 0
                fert_d['depth'] = 0.2
            else:
                fert_d['meth'] = 1
                fert_d['depth'] = depth
            
            # get fertilizer type and amount
            fert_type = self.data['field'][key_field][fp + 'type'].lower()
            fert_amt = self.data['field'][key_field][fp + 'amt']

            fert_d['nit'] = self.data['fert'][fert_type]['nit'] * fert_amt
            fert_d['ammbic'] = self.data['fert'][fert_type]['ammbic'] * fert_amt
            fert_d['urea'] = self.data['fert'][fert_type]['urea'] * fert_amt
            fert_d['anh'] = self.data['fert'][fert_type]['anh'] * fert_amt
            fert_d['amm'] = self.data['fert'][fert_type]['amm'] * fert_amt
            fert_d['sulf'] = self.data['fert'][fert_type]['sulf'] * fert_amt
            fert_d['phos'] = self.data['fert'][fert_type]['phos'] * fert_amt
            
            for k,v in self.data['field'][key_field].iteritems():
                fert_d[k] = v
            
            fert_lines.extend(
                make_lines(manuredndc.FERTS,fert_d))

        fert_lines.extend(
            make_lines(
                manuredndc.FERT_OPT,
                {'fert_option':0}))

        return fert_lines

    def manu_lines(self):
        
        ''' NOT FULLY IMPLEMENTED '''
        return make_lines(manuredndc.MANU,{})
    
    def mulch_lines(self):
        
        ''' NOT FULLY IMPLEMENTED '''
        return make_lines(manuredndc.MULCH,{})
        
    def flood_lines(self):
    
        ''' NOT FULLY IMPLEMENTED '''
        return make_lines(manuredndc.FLOOD,{})
    
    def irri_lines(self, field, mgt_year):

        # get field key
        key_field = (self.site,self.scen,field,mgt_year)
        
        # check for irrigation use
        irri_use = int(self.data['field'][key_field]['irri_use'])
        
        # get irrigation info
        if irri_use == 1:
            irris = int(self.data['field'][key_field]['irris'])
            irri_type = int(self.data['field'][key_field]['irri_type'])
            irri_index = int(self.data['field'][key_field]['irri_index'])
            irri_meth = int(self.data['field'][key_field]['irri_meth'])
        else:
            irris = 0
            irri_type = 0 
            irri_index = 0.0
            irri_meth = 0
        
        irri_d = {
            'irris':irris,
            'irri_type':irri_type,
            'irri_index':irri_index,
            'irri_meth':irri_meth}
        
        irri_lines = make_lines(manuredndc.IRRI,irri_d)
        
        # get application info
        for irri in range(1,irris+1):
            
            # get irri data based on prefix (ip)
            ip = 'irri_' + str(irri) + '_'
            
            date = self.data['field'][key_field][ip + 'date']
            day = date.day
            mo = date.month
            amt = self.data['field'][key_field][ip + 'amt']
            meth = self.data['field'][key_field][ip + 'meth']
            
            app_d = {
                'irri':irri,
                'mo':mo,
                'day':day,
                'amt':amt,
                'meth':meth}
            
            irri_lines.extend(
                make_lines(manuredndc.IRRIS,app_d))
        
        return irri_lines

    def graze_lines(self, field, mgt_year):
        
        ''' IMPLEMENTED ONLY FOR DAIRY COWS + REPLACEMENTS '''
        
        # get field key
        key_field = (self.site, self.scen, field, mgt_year)
    
        grazes = self.data['field'][key_field]['grazes']
        graze_lines = make_lines(manuredndc.GRAZE,{'grazes':grazes})

        for graze in xrange(1,int(grazes)+1):
            
            graze_d = {}
            
            graze_d['graze'] = graze
            
            # get grazing data based on graze prefix (gp)
            gp = 'graze_' + str(graze) + '_'
            
            date_start = self.data['field'][key_field][gp + 'start']
            date_end = self.data['field'][key_field][gp + 'end']
            graze_d['day_start'] = date_start.day
            graze_d['mo_start'] = date_start.month
            graze_d['day_end'] = date_end.day
            graze_d['mo_end'] = date_end.month
            
            # get grazing population
            graze_d['head_dairy']   = self.data['field'][key_field][gp + 'head_dairy']
            graze_d['head_replace'] = self.data['field'][key_field][gp + 'head_replace']
            
            # get additional grazing parameters
            graze_d['hours']   = self.data['field'][key_field][gp + 'hours']
            graze_d['feed']    = self.data['field'][key_field][gp + 'feed']
            graze_d['feed_cn'] = self.data['field'][key_field][gp + 'feed_cn']
            graze_d['excreta'] = self.data['field'][key_field][gp + 'excreta']

            graze_lines.extend(
                make_lines(manuredndc.GRAZES,graze_d))

        return graze_lines

    def cut_lines(self, field, mgt_year):

        ''' NOTE: IMPLEMENTED SO THAT THE WHOLE PLANT IS CUT ABOVE 
                  SOME HEIGHT (I.E. ALL GRAIN IS CUT, AND THE GIVEN
                  CUT FRACTION IS TAKEN FROM LEAF AND STEM) '''
    
        # get field key
        key_field = (self.site,self.scen,field,mgt_year)

        cuts = int(self.data['field'][key_field]['cuts'])
        
        # double the cuts
        cuts = cuts * 2
        
        cut_lines = make_lines(manuredndc.CUT,{'cuts':cuts})
        
        for cut in range(1,cuts+1):

            cut_d = {'cut':cut}

            # get cut data based on crop prefix (cp)
            cp = 'cut_' + str((cut + 1) / 2) + '_'

            # on evens, cut 100% of grain
            if cut % 2 == 0:
                cut_d['frac'] = 1.0
                cut_d['part'] = 1   # only grain
            
            # on odds (actual applications),
            # cut leaf and stem by actual fraction
            else:
                frac = self.data['field'][key_field][cp + 'frac']
                cut_d['frac'] = frac
                cut_d['part'] = 8   # leaf & stem

            date = self.data['field'][key_field][cp + 'date']
            cut_d['day'] = date.day
            cut_d['mo'] = date.month
                
            cut_lines.extend(
                make_lines(manuredndc.CUTS,cut_d))
        
        return cut_lines 
    
    def dnd_lines(self):
        
        dnd_lines = self.site_lines()
        dnd_lines.extend(self.ls_lines())
        dnd_lines.extend(self.stor_lines('lagoon'))
        dnd_lines.extend(self.stor_lines('compost'))
        dnd_lines.extend(self.dig_lines())
        dnd_lines.extend(self.land_lines())
        dnd_lines.extend(self.clim_lines())
        dnd_lines.extend(self.soil_lines())
        dnd_lines.extend(self.crop_lines())
        
        dnd_lines.extend(manuredndc.END_OF_FILE)
        
        return dnd_lines
    
    def write_dnd(self, path_file):
        
        file = open(path_file,'wb')
        for line in self.dnd_lines():
            file.write('{0}\r\n'.format(line))
        file.close()
        
        return
    
    def dnd_name(self,filename=False):

        dnd_name = (
            str(self.site) + '-' + 
            str(self.clim) + '-' +
            str(self.scen))
        
        if filename:
            dnd_name = (
                dnd_name + 
                '.dnd')                
        
        return dnd_name
    
    def dnd_path(self):
        
        dnd_path = (
            PATH_MANUREDNDC_C + '\\' +
            self.project + '\\' +
            'inputs' + '\\' +
            str(self.scen) + '\\' +
            self.dnd_name(filename=True))
        
        return dnd_path

