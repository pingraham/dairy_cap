#!/usr/bin/env python

import os, sys
import pandas as pd

def xls2dict(df, index_l):
    
    '''
    NOTE: CHANGED THIS TO SPEED THINGS UP
    TAKES A WHILE TO OPEN THIS PARTICULAR DB.XLSX...
    '''
    
    # path_xls = text path
    # sheet = text XLS worksheet name
    # index_l = list of column labels for index 
    # skip_l = list of indices of rows to skip
    
    # read in via pandas
    # NOTE: this won't work if you set an index...
    # df = pd.io.excel.read_excel(path_xls,sheetname=sheet,skiprows=skip_l)

    # convert to list of records
    df_records = df.to_dict(orient='records')
    
    # remove blank lines
    l = []
    for r in df_records:
        count_null = 0
        for value in r.values():
            if pd.isnull(value):  # finds pandas NaN and NaT values
                count_null+=1
        if count_null != len(r): # if the whole line is not null, don't skip it...
            l.append(r)

    # check for duplicate headers
    # Pandas automatically adds an integer count to duplicate fields as:
    #     field     1st instance of field
    #     field.1   2nd instance of field
    # create a list of headers with no counters
    headers = [header.split('.')[0] for header in list(df.columns.values)]
    for item in headers:
        c = headers.count(item)
        # print sheet, item # this line will tell you which sheet and header is duplicated
        assert c == 1       # if the count is not 1, a field name is duplicated

    # initiate dictionary
    d = {}
    
    # add records to dictionary, keyed on index
    key_l = []
    for recnum,record in enumerate(l):
        if len(index_l) > 1:
            # for multi-indices, pull out all index values as tuple
            key = tuple([record[i] for i in index_l])
        else:
            # for single indices, use the index value alone
            key = record[index_l[0]]

        # populate the dictionary
        d[key] = record
        
        # check the keys for duplicates
        # print sheet,key       # this will tell you the duplicated key
        assert key not in key_l # a key is used twice in the table
        key_l.append(key)

    return d

def read_defaults(path_db):

    # initiate dictionary
    def_d = {}
    
    # read in via pandas
    df = pd.io.excel.read_excel(path_db,sheetname='defaults')

    for index,row in df.iterrows():
        table = row.table
        field = row.field
        default = row.default

        if table not in def_d.keys():
            def_d[table] = {}
        
        def_d[table][field] = default
    
    return def_d

def insert_defaults(table_name,table_d,def_d):
    
    # get list of missing fields
    # first extract the field headers from the first record
    keys_table = table_d[table_d.keys()[0]].keys()
    
    # then get the fields from the default table
    try:
        keys_default = def_d[table_name].keys()
    except KeyError:
        return
    
    # then get the difference between them
    keys_update = list(set(keys_default) - set(keys_table))

    # add values to table
    for key in keys_update:
        
        for index_value in table_d.keys():
            
            # create the sub-dictionary
            if key not in table_d[index_value].keys():
                table_d[index_value][key] = {}
            
            table_d[index_value][key] = def_d[table_name][key]
    
    
    
    return

def check_dndc(data_d):
    
    ''' RULES:
    site-scenario combinations:
        must have at least 1 field
    feedlot manure removal:
        fractions must sum to 1.0
    lagoon, compost, and digester removals:
        fractions must sum to 1.0
    fields:
        manure share must sum to 100
    see also:
        xls2dict: duplicate field names not allowed
        xls2dict: duplicate keys not allowed
    ''' 
    
    # generate all site-scenario combinations
    # key_l = [(site,scen) for site in data_d['site'].keys() for scen in data_d['scenario'].keys()]
    # key_s = set(key_l)

    # this loop accomodates cases where a site isn't run for all scenarios
    key_l = []
    for site in data_d['site'].keys():
        site_name = data_d['site'][site]['site_name']
        state = site_name[:2]
        for scen in data_d['scenario'].keys():
            ifsm_name = data_d['scenario'][scen]['ifsm_' + state.lower()]
            if pd.isnull(ifsm_name):
                continue
            else:
                key_l.append((site, scen))

    key_s = set(key_l)
    
    ''' CHECK FIELD MANAGEMENT DATA '''
    field_s = set()
    total_manure_d = {}
    for site,scen,field,mgt_year in data_d['field'].keys():
        field_s.add((site,scen))

        if mgt_year > 1: continue
        
        manu_frac = data_d['field'][site,scen,field,mgt_year]['manu_frac']
        try:
            total_manure_d[site,scen]+=manu_frac
        except KeyError:
            total_manure_d[site,scen] = manu_frac
        
    missing_field_data = list(key_s - field_s)
    # for key in missing_field_data: print key  # this will tell you the missing keys
    assert missing_field_data == []             # if this list is not empty there is missing field data

    for key in total_manure_d:
        # print key, total_manure_d[key]                         # this will tell you which key doesn't add up
        assert round(total_manure_d[key], 2) == 1.0   # the total manure fraction is not 100%

    return

def check_manuredndc(data_d):
    
    ''' RULES:
    site-scenario combinations:
        must have at least 1 feedlot
        must have at least 1 form of storage
        must have at least 1 field
    feedlot manure removal:
        fractions must sum to 1.0
    lagoon, compost, and digester removals:
        fractions must sum to 1.0
    fields:
        manure share must sum to 100
    see also:
        xls2dict: duplicate field names not allowed
        xls2dict: duplicate keys not allowed
    ''' 
    
    # generate all site-scenario combinations
    # key_l = [(site,scen) for site in data_d['site'].keys() for scen in data_d['scenario'].keys()]
    
    # this loop accomodates cases where a site isn't run for all scenarios
    key_l = []
    for site in data_d['site'].keys():
        site_name = data_d['site'][site]['site_name']
        state = site_name[:2]
        for scen in data_d['scenario'].keys():
            ifsm_name = data_d['scenario'][scen]['ifsm_' + state.lower()]
            if pd.isnull(ifsm_name):
                continue
            else:
                key_l.append((site, scen))

    key_s = set(key_l)

    ''' CHECK FOR FEEDLOTS AND REMOVALS '''
    fl_s = set()
    for key in data_d['feedlot'].keys():
        
        site,scen,fl = key
        fl_s.add((site,scen))
        
        # sum liquid manure removal fractions
        remove_liq = (
            data_d['feedlot'][key]['liq2comp'] + 
            data_d['feedlot'][key]['liq2lag'] + 
            data_d['feedlot'][key]['liq2dig'] + 
            data_d['feedlot'][key]['liq2field'] + 
            data_d['feedlot'][key]['liq2remain'])
        
        # sum solid manure removal fractions
        remove_sol = (
            data_d['feedlot'][key]['sol2comp'] + 
            data_d['feedlot'][key]['sol2lag'] + 
            data_d['feedlot'][key]['sol2dig'] + 
            data_d['feedlot'][key]['sol2field'] + 
            data_d['feedlot'][key]['sol2remain'])
            
        # print key                 # this will tell you which feedlot has incorrect fractions
        assert remove_liq == 1.0    # liquid manure removals do not sum to 1.0
        assert remove_sol == 1.0    # solid manure removals do not sum to 1.0
    
    missing_feedlot_data = list(key_s - fl_s)
    for key in missing_feedlot_data: print key    # this will tell you the missing keys
    assert missing_feedlot_data == []               # if this list is not empty there is missing feedlot data

    ''' CHECK FOR STORAGE '''
    stor_s = set()
    for site,scen in data_d['storage'].keys():
        
        stor_s.add((site,scen))
        
        # initiate lagoon, compost, and digester trackers
        lagoon = None
        compost = None
        digester = None
        
        # check for lagoon
        lag_cap = data_d['storage'][(site,scen)]['lag_cap']
        lag_area = data_d['storage'][(site,scen)]['lag_area']
        lag_removals = int(data_d['storage'][(site,scen)]['lag_removals'])

        if lag_cap != 0.0 and lag_area != 0.0 and lag_removals != 0:
            lagoon = 1

        # check for compost
        comp_porosity = data_d['storage'][(site,scen)]['comp_porosity']
        comp_removals = data_d['storage'][(site,scen)]['comp_removals']

        if comp_porosity != 0.0 and comp_removals > 0:
            compost = 1

        # check for digester
        dig_res = data_d['storage'][(site,scen)]['dig_res']
        dig2field = data_d['storage'][(site,scen)]['dig2field']
        dig2lagoon = data_d['storage'][(site,scen)]['dig2lagoon']
        dig2bedding = data_d['storage'][(site,scen)]['dig2bedding']

        if dig_res > 0.0 and (dig2field > 0.0 or dig2lagoon > 0.0 or dig2bedding > 0.0):
            digester = 1

        assert lagoon or compost or digester # a site-scenario is missing storage data
        
        # loop through removals
        if lagoon:
            lag_removals = int(data_d['storage'][(site,scen)]['lag_removals'])
            for removal in xrange(1,lag_removals+1):
                lr = 'lr_' + str(removal) + '_'
                remove_lag = (
                    data_d['storage'][(site,scen)][lr + 'lag2field'] + 
                    data_d['storage'][(site,scen)][lr + 'lag2dig'] + 
                    data_d['storage'][(site,scen)][lr + 'lag2remain'])
                
                # print site,scen,removal   # this will tell you the offending lagoon removal
                assert remove_lag == 1.0    # a lagoon removal does not sum to 1.0

        if compost:
            comp_removals = data_d['storage'][(site,scen)]['comp_removals']
            for removal in xrange(1,comp_removals+1):
                cr = 'cr_' + str(removal) + '_'
                remove_comp = (
                    data_d['storage'][(site,scen)][cr + 'comp2field'] + 
                    data_d['storage'][(site,scen)][cr + 'comp2market'] + 
                    data_d['storage'][(site,scen)][cr + 'comp2remain'])
                
                # print site,scen,removal   # this will tell you the offending compost removal
                assert remove_comp == 1.0   # a compost removal does not sum to 1.0
                
        if digester:
            remove_dig = (
                data_d['storage'][(site,scen)]['dig2field'] + 
                data_d['storage'][(site,scen)]['dig2lagoon'] + 
                data_d['storage'][(site,scen)]['dig2bedding'])
                
            # print site,scen           # this will tell you the offending digester
            assert remove_dig == 1.0    # a digester removal does not sum to 1.0

    missing_storage_data = list(key_s - stor_s)
    # for key in missing_storage_data: print key    # this will tell you the missing keys
    assert missing_storage_data == []               # if this list is not empty there is missing feedlot data

    ''' CHECK FIELD MANAGEMENT DATA '''
    field_s = set()
    total_manure_d = {}
    for site, scen ,field, mgt_year in data_d['field'].keys():

        if mgt_year > 1: continue

        # this loop accomodates cases where a site isn't run for all scenarios
        site_name = data_d['site'][site]['site_name']
        state = site_name[:2]
        ifsm_name = data_d['scenario'][scen]['ifsm_' + state.lower()]
        if pd.isnull(ifsm_name):
            continue
        else:
            field_s.add((site,scen))

        manu_frac = data_d['field'][site, scen, field, mgt_year]['manu_frac']

        try:
            total_manure_d[site,scen]+=manu_frac
        except KeyError:
            total_manure_d[site,scen] = manu_frac

    missing_field_data = list(key_s - field_s)
    # for key in missing_field_data: print key  # this will tell you the missing keys
    assert missing_field_data == []             # if this list is not empty there is missing field data

    for key in total_manure_d:
        # print key                         # this will tell you which key doesn't add up
        assert round(total_manure_d[key], 3) == 1.0   # the total manure fraction is not 100%

    return

def read_db(path_sim, software):

    # initiate data dictionary
    data_d = {}
    
    ''' READ IN TABLES '''
    print '    read in tables'

    path_db = os.path.join(
        path_sim,
        'database.xlsx')
    
    df_dict = {}
    sheets = [
        'site',
        'climate',
        'scenario',
        'crop',
        'soil',
        'fertilizer',
        'defaults']

    if software == 'ManureDNDC':
        sheets.append('feedlot')
        sheets.append('storage')
        # sheets.append('field_manuredndc')
        sheets.append('field_dndc')
        
    if software == 'DNDC':
        sheets.append('field_dndc')

    for sheet in sheets:
        
        if 'field' in sheet:
            key = 'field'
        else:
            key = sheet
        
        df_dict[key] = pd.read_excel(
            path_db,
            sheetname=sheet,
            skiprows=[0])

    site_d  = xls2dict(df_dict['site'],       ['site'])                                 # SITE
    clim_d  = xls2dict(df_dict['climate'],    ['clim'])                                 # CLIMATE
    scen_d  = xls2dict(df_dict['scenario'],   ['scenario'])                             # SCENARIO
    field_d = xls2dict(df_dict['field'],      ['site','scenario','field','mgt_year'])   # FIELD
    crop_d  = xls2dict(df_dict['crop'],       ['id_db'])                                # CROPS
    soil_d  = xls2dict(df_dict['soil'],       ['texture'])                              # SOIL LOOKUP
    fert_d  = xls2dict(df_dict['fertilizer'], ['fert'])                                 # FERTILIZER
    
    if software == 'ManureDNDC':
        fl_d = xls2dict(df_dict['feedlot'],   ['site','scenario','feedlot']) # FEEDLOT
        stor_d = xls2dict(df_dict['storage'], ['site','scenario']          ) # STORAGE

    ''' READ IN DEFAULTS '''
    def_d = read_defaults(path_db)

    ''' INSERT_DEFAULTS '''
    insert_defaults('site',site_d,def_d)
    insert_defaults('scenario',scen_d,def_d)
    insert_defaults('field',field_d,def_d)
    insert_defaults('crop',crop_d,def_d)
    if software == 'ManureDNDC':
        insert_defaults('feedlot',fl_d,def_d)
        insert_defaults('storage',stor_d,def_d)

    ''' BUILD DATA DICTIONARY '''
    data_d['site']     = site_d
    data_d['climate']  = clim_d
    data_d['scenario'] = scen_d
    data_d['field']    = field_d
    data_d['crop']     = crop_d
    data_d['soil']     = soil_d
    data_d['fert']     = fert_d
    if software == 'ManureDNDC':
        data_d['feedlot']  = fl_d
        data_d['storage'] = stor_d

    ''' RUN DATABASE CHECKS '''
    if software == 'DNDC':
        check_dndc(data_d)
    if software == 'ManureDNDC':
        check_manuredndc(data_d)
    
    return data_d

def main():

    # global variables for network paths
    project = 'dairy_bmp'
    path    = ags_network.paths(project)

    # database name
    database = 'database.xlsx'

    ''' SET PATHS '''
    path_db = os.path.join(
        path,database)

    ''' READ IN DATABASE '''
    data_d = read_db(path_db)

if __name__ == '__main__':
    main()
