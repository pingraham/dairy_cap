#!/usr/bin/env python

import os, sys
from os.path import join as opj
import ags_network
import pandas as pd

# global variables for network paths
PROJECT = 'dairy_cap'
PATH    = ags_network.paths(PROJECT)

# task name
TASK = 'task2a_bmp'

### DEFINE INPUT DATA ###

# state abbreviation, site code
STATE_DATA = [
    ('ny', 1),
    ('wi', 2)]

# Al Rotz's fraction for manure dropped in milk center
PARLOR_FRACTION = 0.08
    
# barn number, animal type code, barn type, fraction
BARN_DATA = [
    (1, 1, 'milk',       (1.0 - PARLOR_FRACTION)),
    (2, 2, 'replacement', 1.0),
    (3, 1, 'parlor',      PARLOR_FRACTION)]

def main():
    
    ### SET PATHS ###
    path_task = opj(
        PATH.projects,
        TASK)
    
    path_mgt = opj(
        path_task,
        'management',
        'ifsm')
    
    path_excretion = opj(
        path_task,
        'simulation',
        'excretion')

    ### READ-IN DATA ###
    path_file = opj(
        path_mgt,
        'excretion_ifsm.xlsx')

    df_rate = pd.read_excel(
        path_file,
        sheetname='baseline_rates')
    df_rate.set_index('parameter', inplace=True)
    df_rate['total'] = df_rate['cows'] + df_rate['heifers']

    df_tab = pd.read_excel(
        path_file,
        sheetname='table_data')

    ### CREATE EXCRETION FILES BY SITE AND SCENARIO ###
    for scen, state, site in df_tab[['scen','state','site']].values:

        for barn, animal, barn_type, cow_frac in BARN_DATA:

            ### SET RATE HEADER ###
            if barn_type == 'replacement':
                cow_header = 'heifers'
            else:
                cow_header = 'cows'

            ### EXTRACT BASELINE RATES ###
            baseline_rates = df_rate.loc[df_rate['state']==state][cow_header]

            dm_baseline      = baseline_rates['Total dry matter']
            fecal_n_baseline = baseline_rates['Fecal N']
            urine_n_baseline = baseline_rates['Urine N']
            p_baseline       = baseline_rates['Total P']
            h2o_baseline     = baseline_rates['Total water']

            ### EXTRACT ADJUSTMENTS ###
            
            # from rates
            fecal_n_total = df_rate.loc[df_rate['state']==state]['total']['Fecal N']
            
            urine_n_total = df_rate.loc[df_rate['state']==state]['total']['Urine N']
            
            urine_n_fraction = (
                urine_n_baseline / 
                df_rate.loc[df_rate['state']==state]['total']['Urine N'])

            p_fraction = (
                p_baseline / 
                df_rate.loc[df_rate['state']==state]['total']['Total P'])
            
            # from tables...
            adjustment_row = df_tab.loc[
                (df_tab['scen']==scen) & 
                (df_tab['state']==state)].iloc[0]
            
            dm_rel = adjustment_row['relative DM']
            n_exc  = adjustment_row['Excreted N, kg'] / 365.
            p_exc  = adjustment_row['Excreted P, kg'] / 365.

            ### ADJUST BASELINE VALUES ###
            dm      = dm_baseline * dm_rel    * cow_frac 
            fecal_n = fecal_n_baseline        * cow_frac
            urine_n = (
                (n_exc - fecal_n_total) *
                urine_n_fraction)             * cow_frac
            p       = p_exc * p_fraction      * cow_frac
            h2o     = h2o_baseline * dm_rel   * cow_frac

            data = []
            for jday in range(1, 365+1):
                data.append([
                    jday,
                    dm,
                    fecal_n,
                    urine_n,
                    p,
                    h2o,
                    animal])
            df_final = pd.DataFrame(data)

            ### WRITE EXCRETION FILES ###
            # define headers
            header1 = [
                'Jday',
                'Dry_Matter',
                'Nitrogen_Dung',
                'Nitrogen_Urine',
                'Phosphorus',
                'Water',
                'Animal_Type']

            # define units headers
            header2 = [
                'Jday', # this is inserted when the dataframe is written below...
                'kg',
                'kgN',
                'kgN',
                'kgP',
                'kg',
                'TypeNumber']

            path_scen = opj(
                path_excretion,
                str(scen))
            if not os.path.exists(path_scen):
                os.mkdir(path_scen)

            path_file = opj(
                path_scen,
                barn_type + '_' + str(site) + '.txt')

            pd.DataFrame(index=[], columns=header1).to_csv(path_file, index=False, sep=' ')
            df_final.columns = header2

            df_final.to_csv(
                path_file,
                mode='a',
                index=False,
                header=True,
                sep=' ')

    return df_tab

def verify(df_tab):
    
    ### SET PATHS ###
    path_task = opj(
        PATH.projects,
        TASK)

    path_excretion = opj(
        path_task,
        'simulation',
        'excretion')

    ### READ-IN EXCRETION FILES ###
    dfs = []
    for scen, state, site in df_tab[['scen','state','site']].values:
    
        path_scen = opj(path_excretion, str(scen))
            
        for barn, animal, barn_type, cow_frac in BARN_DATA:
            
            path_file = opj(
                path_scen,
                barn_type + '_' + str(site) + '.txt')
            
            df = pd.read_csv(
                path_file,
                sep=' ',
                skiprows=[1])
            
            df['site'] = site
            df['scen'] = scen
            df['barn_type'] = barn_type
            
            dfs.append(df)
    
    ### CONCATENATE ###
    df = pd.concat(dfs)
    
    ### VERIFY ###
    df = df.groupby(['site', 'scen'], as_index=True).sum()
    
    df['n_file'] = df['Nitrogen_Dung'] + df['Nitrogen_Urine']
    df['p_file'] = df['Phosphorus']
    
    for c in [
        'Jday',
        'Dry_Matter',
        'Nitrogen_Dung',
        'Nitrogen_Urine',
        'Phosphorus',
        'Water',
        'Animal_Type']:
        del df[c]
    
    df_tab.set_index(['site', 'scen'], inplace=True)
    df_tab = df_tab[['Excreted N, kg', 'Excreted P, kg']]
    
    df = df.join(df_tab)
    
    df['n_diff'] = (
        df['n_file'] - 
        df['Excreted N, kg']).round(decimals=6)

    df['p_diff'] = (
        df['p_file'] - 
        df['Excreted P, kg']).round(decimals=6)        
    
    print df

    return
    
if __name__ == '__main__':
    
    print 'create excretion files'
    df_tab = main()
    
    print 'verify results'
    verify(df_tab)