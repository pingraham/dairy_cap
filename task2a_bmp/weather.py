
import os, sys
from os.path import join as opj
import glob
import ags_network
import pandas as pd

# global variables for network paths
PROJECT = 'dairy_cap'
PATH    = ags_network.paths(PROJECT)

# task name
TASK = 'task2_bmp'

def check():    

    ### DEFINE PATHS ###
    path_project = opj(
        PATH.projects,
        TASK)

    path_sim = opj(
        path_project,
        'simulation')

    path_clim = opj(
        path_project,
        'climate_scenarios',
        'climate_scenarios_v1_for_Manure-DNDC')

    ### GLOB ALL CLIMATE DATA ###
    path_glob = opj(
        path_clim,
        '*.txt')
    globber = glob.glob(path_glob)

    ### REPROCESS CLIMATE DATA ###
    
    # initialize a list of data
    data = []
    
    # loop through files
    for path_file in globber:
        
        # extract file info
        filename = os.path.basename(path_file)
        data_name = filename.replace('.txt', '')
        format, day, model, forcing, runspec, site, year = data_name.split('.')

        print filename
        
        # get file lines
        file = open(path_file, 'r')
        lines = file.readlines()
        file.close()

        error_values = (-99.99, -99.9, -9.99)
        
        # loop through lines and replace precip in mm with precip in cm
        for linenum,line in enumerate(lines):
            
            if linenum != 0:
                day, tmax, tmin, pcp, wind = line.split()
                try:
                    day, tmax, tmin, pcp, wind = int(day), float(tmax), float(tmin), float(pcp), float(wind)
                except ValueError:
                    data.append([filename, day, tmax, tmin, pcp, wind])
                
                found_error = False
                for var in (
                    (tmax, 'tmax'), 
                    (tmin, 'tmin'),
                    (pcp, 'pcp'),
                    (wind, 'wind')):
                    
                    if var[0] in error_values:
                        found_error = True
                    
                if found_error:
                    data.append([filename, day, tmax, tmin, pcp, wind])
    
    ### WRITE OUT LINES WITH ERRORS ###
    path_clim = opj(
        path_project,
        'climate_scenarios',
        'bad_lines.csv')
    
    df = pd.DataFrame(data, columns=['filename', 'day', 'tmax', 'tmin', 'pcp', 'wind'])
    df.to_csv(path_clim, index=False)
    
    return

def main():

    ### DEFINE PATHS ###
    path_project = opj(
        PATH.projects,
        TASK)

    path_sim = opj(
        path_project,
        'simulation')

    path_clim = opj(
        path_project,
        'climate_scenarios',
        'climate_scenarios_v1_for_Manure-DNDC')
    
    path_weather = opj(
        path_sim,
        'weather')

    ### READ IN DATABASE CLIMATE DATA ###
    path_db = opj(
        path_sim,
        'database.xlsx')
    
    df_clim = pd.read_excel(
        path_db,
        sheetname='climate',
        skiprows=[0])

    ### GLOB ALL CLIMATE DATA ###
    path_glob = opj(
        path_clim,
        '*.txt')
    globber = glob.glob(path_glob)

    ### REPROCESS CLIMATE DATA ###
    
    # initialize a list of data
    data = []
    
    # loop through files
    for path_file in globber:
        
        # extract file info
        filename = os.path.basename(path_file)
        data_name = filename.replace('.txt', '')
        format, day, model, forcing, runspec, site, year = data_name.split('.')
        
        # get climate dataset code for simulation ("clim")
        clim = df_clim.loc[
            (df_clim['model'] == model) & 
            (df_clim['forcing'] == forcing) & 
            (df_clim['runspec'] == runspec)]['clim'].iloc[0]

        print clim, '-->', model, thing1, thing2, site, year
        
        # get new climate directory for simulation
        path_dir_new = opj(
            path_weather,
            str(clim),
            year)

        if not os.path.exists(path_dir_new):
            os.makedirs(path_dir_new)
        
        # set path for new file
        path_new = opj(
            path_dir_new,
            site + '.txt')
        
        # get file lines
        file = open(path_file, 'r')
        lines = file.readlines()
        file.close()
        
        # open new file
        newfile = open(path_new, 'w')
        
        # loop through lines and replace precip in mm with precip in cm
        for linenum,line in enumerate(lines):
            if linenum != 0:
                day, tmax, tmin, pcp, wind = line.split()
                pcp_cm = str(float(pcp) / 10.)
                line = line.replace(pcp, pcp_cm)

            newfile.write(line)
        
        # close file
        newfile.close()

        # add to list of files
        data.append([clim, filename, format, day, model, forcing, runspec, site, year])
    
    # create a data set that lists all files
    df = pd.DataFrame(data, columns=['dataset_code (clim)', 'filename', 'format', 'day', 'model', 'forcing', 'runspec', 'site', 'year'])
    path_df = opj(
        path_weather,
        'weather_files.csv')
    df.to_csv(path_df, index=False)
    
    return

if __name__ == '__main__':
    # main()
    
    check()