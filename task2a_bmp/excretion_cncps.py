#!/usr/bin/env python

import os, sys
from os.path import join as opj
import ags_network
import pandas as pd

# global variables for network paths
PROJECT = 'dairy_cap'
PATH    = ags_network.paths(PROJECT)

# task name
TASK = 'task2a_bmp'

### DEFINE INPUT DATA ###
STATE_DATA = [
    ('ny', 1),  # state abbreviation, site code
    ('wi', 2)]

BARN_DATA = [
    (1, 1, 'milk'),         # barn number, animal type code, animal type
    (2, 2, 'replacement'),
    (3, 1, 'parlor')]

PARLOR_FRACTION = 0.0208    # Joyce's fraction of cows in milk parlor
    
def main():
    
    ### SET PATHS ###
    path_task = opj(
        PATH.projects,
        TASK)
    
    path_mgt = opj(
        path_task,
        'management',
        'cncps')
    
    path_excretion = opj(
        path_task,
        'simulation',
        'excretion')

    ### CREATE EXCRETION FILES BY SITE AND SCENARIO ###
    for state, site in STATE_DATA:
        
        ### READ-IN DATA ###
        path_file = opj(
            path_mgt,
            'WI NY Herd Data IFSM RationsRevised_pi.xlsx')
        
        df_milk = pd.read_excel(
            path_file,
            sheetname='dndc_' + state + '_milk',
            skiprows=[0])
        
        df_replace = pd.read_excel(
            path_file,
            sheetname='dndc_' + state + '_replace',
            skiprows=[0])

        for barn, animal, barn_type in BARN_DATA:
            
            if barn_type == 'milk' or barn_type == 'parlor':
                df = df_milk.copy(deep=True)
            if barn_type == 'replacement':
                df = df_replace.copy(deep=True)
            
            for scenario in df['scenario'].values:

                ### SET MILK COW FRACTION ###
                if barn_type == 'milk':
                    cow_frac = 1. - PARLOR_FRACTION
                if barn_type == 'parlor':
                    cow_frac = PARLOR_FRACTION
                if barn_type == 'replacement':
                    cow_frac == 1.
                
                                
                print state, barn_type, scenario, cow_frac

                
                ### EXTRACT VALUES ###
                s = df.loc[df['scenario']==scenario].iloc[0]

                data = []
                for jday in range(1, 365+1):
                    data.append([
                        jday,
                        s['Dry_Matter']     * cow_frac,
                        s['Nitrogen_Dung']  * cow_frac,
                        s['Nitrogen_Urine'] * cow_frac,
                        s['Phosphorus']     * cow_frac,
                        s['Water']          * cow_frac,
                        animal])
                df_final = pd.DataFrame(data)

                ### WRITE EXCRETION FILES ###
                # define headers
                header1 = [
                    'Jday',
                    'Dry_Matter',
                    'Nitrogen_Dung',
                    'Nitrogen_Urine',
                    'Phosphorus',
                    'Water',
                    'Animal_Type']

                # define units headers
                header2 = [
                    'Jday', # this is inserted when the dataframe is written below...
                    'kg',
                    'kgN',
                    'kgN',
                    'kgP',
                    'kg',
                    'TypeNumber']

                path_scen = opj(
                    path_excretion,
                    str(scenario))
                if not os.path.exists(path_scen):
                    os.mkdir(path_scen)

                path_file = opj(
                    path_scen,
                    barn_type + '_' + str(site) + '.txt')

                pd.DataFrame(index=[], columns=header1).to_csv(path_file, index=False, sep=' ')
                df_final.columns = header2

                df_final.to_csv(
                    path_file,
                    mode='a',
                    index=False,
                    # index_label='Jday',
                    header=True,
                    sep=' ')

    return

if __name__ == '__main__':
    main()