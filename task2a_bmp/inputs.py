#!/usr/bin/env python

import os, sys
from os.path import join as opj
import ags_network
import pandas as pd
from db import read_db
import dnd

# global variables for network paths
PROJECT = 'dairy_cap'
PATH    = ags_network.paths(PROJECT)

# task name
TASK = 'task2a_bmp'

def parser():

    warning = 'indicate software as -d (--DNDC) or -m (--ManureDNDC)'

    try:
        arg = sys.argv[1]
    except IndexError:
        print warning
        sys.exit()
    
    global SOFTWARE
    
    SOFTWARE = None
    
    if (arg == '-d') or (arg.lower() == '--dndc'):
        SOFTWARE = 'DNDC'
    elif (arg == '-m') or (arg.lower() == '--manuredndc'):
        SOFTWARE = 'ManureDNDC'
    else:
        print warning
        sys.exit()
    
    return

def applications(data_d):
    
    ### SET PATHS ###
    path_app = opj(
        PATH.projects,
        TASK,
        'simulation',
        'results',
        'manure_applications.csv')
    
    ### READ-IN APPLICATIONS ###
    df_app = pd.read_csv(path_app)
    
    ### CREATE A NEW DATA DICTIONARY FOR FIELDS ###
    field_d = {}
    
    ### GET SITE-SCENARIO COMBOS ###
    # this loop accomodates cases where a site isn't run for all scenarios
    site_scens = set()
    for site in data_d['site'].keys():
        site_name = data_d['site'][site]['site_name']
        state = site_name[:2]
        for scen in data_d['scenario'].keys():
            ifsm_name = data_d['scenario'][scen]['ifsm_' + state.lower()]
            if pd.isnull(ifsm_name):
                continue
            else:
                site_scens.add((site, scen))
    
    ### LOOP THROUGH CLIMATE DATA, ETC. ###
    for clim in sorted(data_d['climate'].keys()):
        
        # exclude non-simulated climate data
        if data_d['climate'][clim]['simulate'] == 0:
            continue
        
        # get the right set of years
        year_cal_start = data_d['climate'][clim]['year_cal_start']
        year_cal_end   = data_d['climate'][clim]['year_cal_end']
        years_init   = data_d['climate'][clim]['years_init']

        year_sims = []
        for y in range(years_init):
            year_sims.append(y+1)
        for y in range(year_cal_start, year_cal_end+1):
            year_sims.append(y - year_cal_start + years_init + 1)

        # loop through sites and scenarios
        for site, scen in site_scens:

            # get a list of field IDs
            field_ids = [f+1 for f in range(data_d['scenario'][scen]['fields'])]
        
            # loop through fields
            for field in field_ids: 
                
                field_type = data_d['field'][(site, scen, field, 1)]['field_type']

                # loop through years
                for year_sim in year_sims:

                    if field_type == 'alfalfa':
                        if year_sim%4==1: mgt_year=1
                        if year_sim%4==2: mgt_year=2
                        if year_sim%4==3: mgt_year=3
                        if year_sim%4==0: mgt_year=4
                    elif field_type == 'grass':
                        if year_sim%2==1: mgt_year=1
                        if year_sim%2==0: mgt_year=2
                    else:
                        mgt_year = 1
                    
                    # get key for old dictionary 
                    field_key = (site, scen, field, mgt_year)
                    
                    # set new dictionary key keyed on simulation year
                    new_key = (site, scen, field, year_sim)
                    
                    # set new dictionary value based on old value
                    field_d[new_key] = data_d['field'][(site, scen, field, mgt_year)]
                    
                    ### SET MANURE VALUES ###
                    manus = field_d[new_key]['manures']
                    
                    # get manure application data
                    app = df_app.loc[
                        (df_app['site'] == site) & 
                        (df_app['clim'] == clim) & 
                        (df_app['scen'] == scen) & 
                        (df_app['year_sim'] == year_sim)]
                    
                    print app
                    
                    # manu_1_date	
                    # manu_1_type	
                    # manu_1_dm	
                    # manu_1_orgc	
                    # manu_1_totaln	
                    # manu_1_cn	
                    # manu_1_orgn	
                    # manu_1_nh4	
                    # manu_1_no3	
                    # manu_1_meth	
                    # manu_1_depth

                    
                            
                    
                
                    # print clim, site, scen, field, field_type, year_sim
                
            
            
            
            

                
                
    
    
    sys.exit()
    
    
    return
    
def main():
    
    # parse command line args
    parser()
        
    ### SET PATHS ###
    path_sim = opj(
        PATH.projects,
        TASK,
        'simulation')
        
    path_inputs = opj(
        PATH.scratch,
        TASK,
        'inputs')

    ### READ IN DATABASE ###
    if SOFTWARE == 'DNDC':
        data_d = read_db(path_sim, 'DNDC')
        applications(data_d)
    if SOFTWARE == 'ManureDNDC':
        data_d = read_db(path_sim, 'ManureDNDC')
    
    ### WRITE INPUTS ###
    # create batch list
    batch_l = []

    # create site and scenario lists
    sites = data_d['site'].keys()
    climates = data_d['climate'].keys()
    scenarios = data_d['scenario'].keys()

    # loop through sites
    for site in sorted(sites):
        
        if data_d['site'][site]['simulate'] == 0:
            continue
        
        site_name = data_d['site'][site]['site_name']
        state = site_name[:2]
        
        # loop through climate scenarios
        for clim in sorted(climates):
            
            if data_d['climate'][clim]['simulate'] == 0:
                continue            
            
            # loop through scenarios
            for scen in sorted(scenarios):

                if data_d['scenario'][scen]['simulate'] == 0:
                    continue    
                
                # filter scenarios by site
                ifsm_name = data_d['scenario'][scen]['ifsm_' + state.lower()]
                if pd.isnull(ifsm_name):
                    continue
                
                print 'site:', site, 'climate:', clim, 'scenario:',scen
                
                # create input directory
                path_scen = os.path.join(path_inputs,str(scen))
                if not os.path.exists(path_scen):
                    os.makedirs(path_scen)

                # instantiate Dnd_File object
                if SOFTWARE == 'DNDC':
                    dnd_file = dnd.Dnd_File(site,clim,scen,PROJECT,data_d)
                if SOFTWARE == 'ManureDNDC':
                    dnd_file = dnd.ManureDnd_File(site,clim,scen,PROJECT,data_d)

                # set site name
                dnd_name = dnd_file.dnd_name(filename=True)

                # set input file path
                path_dnd = os.path.join(path_scen,dnd_name)

                # write DND file
                dnd_file.write_dnd(path_dnd)

                # get local (C) path
                path_dnd_c = dnd_file.dnd_path()

                # append path to batch list
                batch_l.append(path_dnd_c)

    # write batch file
    path_batch = os.path.join(path_inputs,'batch.txt')
    batch_file = open(path_batch,'wb')
    batch_file.write('{0}\r\n'.format(len(batch_l)))
    for line in batch_l:
        batch_file.write('{0}\r\n'.format(line))
    batch_file.close()

if __name__ == '__main__':
    main()
