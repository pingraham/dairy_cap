#!/usr/bin/env python

''' MANURE DNDC TEXT '''        

SITE = [
    'DNDC_Input_Parameters',
    '----------------------------------------',
    'Site_infomation',
    '',
    '__Site_name {site_name}',      
    '__Simulated_years {years:.0f}',
    '__Latitude {lat:.4f}',         
    '__Daily_record {daily:.0f}',   
    '__Unit_system {units:.0f}',    
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0'
    ]

LS = [
    '----------------------------------------',
    'Livestock_data:',
    '__Simulated_years {years:.0f}' # scenario
    ]    

LS_YEAR = [
    '____Year {year:.0f}']

LS_FEEDLOT = [
    '______Number_of_feedlots {feedlots:.0f}'   # feedlot
    ]

LS_FEEDLOT_INPUTS = [
    '________Feedlot {feedlot:.0f}',
    '________Feedlot_input_option {feedlot_input:.0f}',
    '________-Feedlot_input_file_name {input_file_name:s}',
    '________Floor_surface_area {floor_area:.4f}',
    '________Floor_type {floor_type:.0f}',
    '________Bedding_material_type {bedding:.0f}',
    '________Bedding_material_C/N {bed_cn:.4f}',
    '________Bedding_frequency {bed_res:.4f}',
    '________Bedding_amount {bed_amt:.4f}',
    '________Ventilation_type {vent_type:.0f}',
    '________Ventilation_rate {vent_rate:.4f}',
    '________Indoor_climate_file {vent_path}',
    '________Manure_removal_frequency {manu_res:.4f}',   
    '________Liquid_to_compost {liq2comp:.4f}',
    '________Liquid_to_lagoon {liq2lag:.4f}',
    '________Liquid_to_digester {liq2dig:.4f}',
    '________Liquid_to_field {liq2field:.4f}',
    '________Liquid_remain_in_feedlot {liq2remain:.4f}',
    '________Solid_to_compost {sol2comp:.4f}',
    '________Solid_to_lagoon {sol2lag:.4f}',
    '________Solid_to_digester {sol2dig:.4f}',
    '________Solid_to_field {sol2field:.4f}',
    '________Solid_remain_in_feedlot {sol2remain:.4f}',
    '________Flushing_liquid_type {flush_type:.0f}',
    '________Flushing_water_m3 {flush_liq:.4f}',
    '________Liquid_fraction 0.0000',
    '________None 0',
    '________None 0',
    '________None 0']
    
LS_FEEDLOTS = [
    '________Feedlot {feedlot:.0f}',
    '________Feedlot_input_option {feedlot_input:.0f}',
    '________Livestock_types {feedlot_types:.0f}',
    '________Livestock_typesID {animal_type:.0f}',
    '________Heads {head:.4f}',
    '________Intake_rate {feed_rate:.4f}',
    '________Crude_protein_percent {feed_cp:.4f}',
    '________Phosphorus_percent {feed_p:.4f}',
    '________None 0',
    '________None 0',
    '________None 0',
    '________None 0',
    '________None 0',
    '________Floor_surface_area {floor_area:.4f}',
    '________Floor_type {floor_type:.0f}',
    '________Bedding_material_type {bedding:.0f}',
    '________Bedding_material_C/N {bed_cn:.4f}',
    '________Bedding_frequency {bed_res:.4f}',
    '________Bedding_amount {bed_amt:.4f}',
    '________Ventilation_type {vent_type:.0f}',
    '________Ventilation_rate {vent_rate:.4f}',
    '________Indoor_climate_file {vent_path}',
    '________Manure_removal_frequency {manu_res:.4f}',   
    '________Liquid_to_compost {liq2comp:.4f}',
    '________Liquid_to_lagoon {liq2lag:.4f}',
    '________Liquid_to_digester {liq2dig:.4f}',
    '________Liquid_to_field {liq2field:.4f}',
    '________Liquid_remain_in_feedlot {liq2remain:.4f}',
    '________Solid_to_compost {sol2comp:.4f}',
    '________Solid_to_lagoon {sol2lag:.4f}',
    '________Solid_to_digester {sol2dig:.4f}',
    '________Solid_to_field {sol2field:.4f}',
    '________Solid_remain_in_feedlot {sol2remain:.4f}',
    '________Flushing_liquid_type {flush_type:.0f}',
    '________Flushing_water_m3 {flush_liq:.4f}',
    '________Liquid_fraction 0.0000',
    '________None 0',
    '________None 0',
    '________None 0']

LAG = [
    '______Lagoon_capacity {lag_cap:.4f}',
    '______Lagoon_surface_area {lag_area:.4f}',
    '______Lagoon_cover_type {lag_cover:.0f}',
    '______Lagoon_rainfall_receival {lag_rain}',
    '______Lagoon_slurry_removal_applications {lag_removals:.0f}']

LAG_REMOVAL = [
    '________Lagoon_slurry_removal {removal:.0f}',
    '________Lagoon_slurry_removal_month {mo:.0f}',
    '________Lagoon_slurry_removal_day {day:.0f}',
    '________Lagoon_slurry_to_field {lag2field:.4f}',
    '________Lagoon_slurry_to_digester {lag2dig:.4f}',
    '________Lagoon_slurry_remain {lag2remain:.4f}',
    '________Land_application_method {meth:.0f}',
    '________Land_application_depth {depth:.4f}']

COMP = [
    '______Compost_porosity {comp_porosity:.4f}',
    '______Compost_cover_type {comp_cover:.0f}',
    '______Compost_removal_applications {comp_removals:.0f}',
    '______Compost_additive_amount {comp_add:.4f}',
    '______Compost_additive_C/N {comp_add_cn:.4f}']

COMP_REMOVAL = [
    '________Compost_removal {removal:.0f}',
    '________Compost_removal_month {mo:.0f}',
    '________Compost_removal_day {day:.0f}',
    '________Compost_manure_to_field {comp2field:.4f}',
    '________Compost_manure_to_market {comp2market:.4f}',
    '________Compost_manure_remain {comp2remain:.4f}']

DIG = [
    '______Digester_type {dig_type:.0f}',
    '______Digester_retention_days {dig_res:.4f}',
    '______Digester_manure_to_field {dig2field:.4f}',
    '______Digester_manure_to_lagoon {dig2lagoon:.4f}',
    '______Digester_manure_to_bedding {dig2bedding:.4f}']

LAND = [
    '______Crop_land_area {area_total:.4f}',
    '______Crop_fields {fields:.0f}',
    '______Simulated_yrs {years:.0f}',
    '________Field_area {area_text:s}']

FIELD = [
    '________Year {year:.0f}',
    '________Manure_fraction {manu_frac:s}']

CLIMATE1 = [
    '',
    '----------------------------------------',
    'Climate_data',
    '',
    '__Climate_data_type {clim_format:.0f}',
    '__N_in_rainfall {ndep:.4f}',
    '__Air_NH3_concentration {clim_nh3:.4f}',
    '__Air_CO2_concentration {clim_co2:.4f}',
    '__Climate_files {clim_files:.0f}']

CLIMATE2 = [
    '__Climate_file_mode {clim_mode:.0f}',
    '__CO2_increase_rate {clim_co2_inc:.4f}',
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0']

SOIL = [
    '----------------------------------------',
    'Soil_data',
    '',
    '__Land_use_ID {lu_code:.0f}',
    '__Soil_texture_ID {texture:.0f}',
    '__Bulk_density {dens:.4f}',
    '__pH {ph:.4f}',
    '__Clay_fraction {clay:.4f}',
    '__Porosity {porosity:.4f}',
    '__Bypass_flow {bypass:.4f}',
    '__Field_capacity {field_cap:.4f}',
    '__Wilting_point {wilt_pt:.4f}',
    '__Hydro_conductivity {hyd_con:.4f}',
    '__Top_layer_SOC {soc:.4f}',
    '__Litter_fraction {frac_litter:.4f}',
    '__Humads_fraction {frac_humads:.4f}',
    '__Humus_fraction {frac_humus:.4f}',
    '__Adjusted_litter_factor {adj_litter:.4f}',
    '__Adjusted_humads_factor {adj_humads:.4f}',
    '__Adjusted_humus_factor {adj_humus:.4f}',
    '__Humads_C/N {cn_humads:.4f}',
    '__Humus_C/N {cn_humus:.4f}',
    '__Black_C {black_c:.4f}',
    '__Black_C_C/N {black_cn:.4f}',
    '__SOC_profile_A {soc_profile_a:.4f}',
    '__SOC_profile_B {soc_profile_b:.4f}',
    '__Initial_nitrate_ppm {nit_ppm:.4f}',
    '__Initial_ammonium_ppm {amm_ppm:.4f}',
    '__Soil_microbial_index {soil_micr_ind:.4f}',
    '__Soil_slope {slope:.4f}',
    '__Lateral_influx_index {lat_inf_ind:.4f}',
    '__Watertable_depth {wt_depth:.4f}',
    '__Water_retension_layer_depth {wrl_depth:.4f}',
    '__Soil_salinity {salinity:.4f}',
    '__SCS_curve_use {scs_curve:.0f}',
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0']

CROP_DATA = [
    '----------------------------------------',
    'Crop_data',
    '',
    'Cropping_systems {crop_systems:.0f}',
    '',
    '__Cropping_system {crop_system:.0f}',
    '__Total_years {years:.0f}',
    '__Years_of_a_cycle {cycle_years:.0f}']

CROP = [
    '',
    '____Year {year:.0f}',
    '____Crops {crops:.0f}']

CROPS = [
    '______Crop# {crop:.0f}',
    '______Crop_ID {id_dndc:.0f}',
    '______Planting_month {plant_mo:.0f}',
    '______Planting_day {plant_day:.0f}',
    '______Harvest_month {harv_mo:.0f}',
    '______Harvest_day {harv_day:.0f}',
    '______Harvest_year {harv_year:.0f}',
    '______Residue_left_in_field {residue:.4f}',
    '______Maximum_yield {max_yield:.4f}',
    '______Leaf_fraction {frac_leaf:.4f}',
    '______Stem_fraction {frac_stem:.4f}',
    '______Root_fraction {frac_root:.4f}',
    '______Grain_fraction {frac_grain:.4f}',
    '______Leaf_C/N {cn_leaf:.4f}',
    '______Stem_C/N {cn_stem:.4f}',
    '______Root_C/N {cn_root:.4f}',
    '______Grain_C/N {cn_grain:.4f}',
    '______Accumulative_temperature {tdd:.4f}',
    '______Optimum_temperature {optt:.4f}',
    '______Water_requirement {h2o_demand:.4f}',
    '______N_fixation_index {nfix:.4f}',
    '______Vascularity {vascularity:.4f}',
    '______If_cover_crop {cover:.0f}',
    '______If_perennial_crop {perennial:.0f}',
    '______If_transplanted {transplanted:.0f}',
    '______Tree_maturity_age {tree_maturity:.4f}',
    '______Tree_current_age 0.0000',
    '______Tree_max_leaf 0.0000',
    '______Tree_min_leaf 0.0000',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '']

TILL = [
    '----------------------------------------',
    '____Till_applications {tillages:.0f}']

TILLS =[
    '______Till# {till:.0f}',
    '______Till_month {mo:.0f}',
    '______Till_day {day:.0f}',
    '______Till_method {depth_code:.0f}']

FERT = [
    '----------------------------------------',
    '____Fertilizer_applications {fertilizations:.0f}']

FERTS = [
    '______Fertilizing# {fert:.0f}',
    '______Fertilizing_month {mo:.0f}',
    '______Fertilizing_day {day:.0f}',
    '______Fertilizing_method {meth:.0f}',
    '______Fertilizing_depth {depth:.4f}',
    '______Nitrate {nit:.4f}',
    '______Ammonium_bicarbonate {ammbic:.4f}',
    '______Urea {urea:.4f}',
    '______Anhydrous_ammonia {anh:.4f}',
    '______Ammonium {amm:.4f}',
    '______Sulphate {sulf:.4f}',
    '______Phosphate {phos:.4f}',
    '______Slow_release_rate {slow_rel:.4f}',
    '______Nitrification_inhibitor_efficiency {nitr_inhib_eff:.4f}',
    '______Nitrification_inhibitor_duration {nitr_inhib_dur:.4f}',
    '______Urease_inhibitor_efficiency {urease_inhib_eff:.4f}',
    '______Urease_inhibitor_duration {urease_inhib_dur:.4f}',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0']

FERT_OPT = [
    '____Fertilization_option {fert_option:.0f}']    

MANU = [
    '----------------------------------------',
    '____Manure_applications 0',
    '----------------------------------------']

MULCH = [
    '____Film_applications 0',
    '____Method 0',
    '----------------------------------------']

FLOOD = [
    '____Flood_applications 0',
    '____Water_control 0',
    '____Flood_water_N 1.0000',
    '____Leak_rate 0.0000',
    '____Water_gather_index 1.0000',
    '____Watertable_file None',
    '____Empirical_para_1 0.0000',
    '____Empirical_para_2 0.0000',
    '____Empirical_para_3 0.0000',
    '____Empirical_para_4 0.0000',
    '____Empirical_para_5 0.0000',
    '____Empirical_para_6 0.0000',
    '----------------------------------------']

IRRI = [
    '____Irrigation_applications {irris:.0f}',
    '____Irrigation_control {irri_type:.0f}',
    '____Irrigation_index {irri_index:.4f}',
    '____Irrigation_method {irri_meth:.0f}']

IRRIS = [
    '______Irrigation# {irri:.0f}',
    '______Irri_month {mo:.0f}',
    '______Irri_day {day:.0f}',
    '______Water_amount {amt:.4f}',
    '______Irri_method {meth:.0f}',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0']
    
GRAZE = [
    '----------------------------------------',
    '____Grazing_applications {grazes:.0f}']

GRAZES = [
    '______Grazing#  {graze:.0f}',
    '______Start_month {mo_start:.0f}',
    '______Start_day {day_start:.0f}',
    '______End_month {mo_end:.0f}',
    '______End_day {mo_end:.0f}',
    '______Dairy_heads {head_dairy:.4f}',
    '______Beef_heads {head_replace:.4f}',
    '______Pig_heads 0.0000',   # not implemented
    '______Sheep_heads 0.0000', # not implemented
    '______Horse_heads 0.0000', # not implemented
    '______Grazing_hours/day {hours:.1f}',
    '______Additional_feed {feed:.4f}',
    '______Additional_feed_C/N {feed_cn:.4f}',
    '______Excreta_handling {excreta:.0f}',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0']
    
CUT = [
    '----------------------------------------',
    '____Cut_applications {cuts:.0f}']

CUTS = [
    '______Cut# {cut:.0f}',
    '______Cut_month {mo:.0f}',
    '______Cut_day {day:.0f}',
    '______Cut_fraction {frac:.4f}',
    '______Cut_part {part:.0f}',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0']

END_OF_FILE = [
    '----------------------------------------']