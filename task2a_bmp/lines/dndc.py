#!/usr/bin/env python

''' DNDC TEXT '''        

SITE = [
    'DNDC_Input_Parameters',
    '----------------------------------------',
    'Site_infomation',
    '',
    '__Site_name {site_name}',      
    '__Simulated_years {years:.0f}',
    '__Latitude {lat:.4f}',         
    '__Daily_record {daily:.0f}',   
    '__Unit_system {units:.0f}',    
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0']

CLIMATE1 = [
    '----------------------------------------',
    'Climate_data',
    '',
    '__Climate_data_type {clim_format:.0f}',
    '__N_in_rainfall {ndep:.4f}',
    '__Air_NH3_concentration {clim_nh3:.4f}',
    '__Air_CO2_concentration {clim_co2:.4f}',
    '__Climate_files {clim_files:.0f}']

CLIMATE2 = [
    '__Climate_file_mode {clim_mode:.0f}',
    '__CO2_increase_rate {clim_co2_inc:.4f}',
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0']

SOIL = [
    '----------------------------------------',
    'Soil_data',
    '',
    '__Land_use_ID {lu_code:.0f}',
    '__Soil_texture_ID {texture:.0f}',
    '__Bulk_density {dens:.4f}',
    '__pH {ph:.4f}',
    '__Clay_fraction {clay:.4f}',
    '__Porosity {porosity:.4f}',
    '__Bypass_flow {bypass:.4f}',
    '__Field_capacity {field_cap:.4f}',
    '__Wilting_point {wilt_pt:.4f}',
    '__Hydro_conductivity {hyd_con:.4f}',
    '__Top_layer_SOC {soc:.4f}',
    '__Litter_fraction {frac_litter:.4f}',
    '__Humads_fraction {frac_humads:.4f}',
    '__Humus_fraction {frac_humus:.4f}',
    '__Adjusted_litter_factor {adj_litter:.4f}',
    '__Adjusted_humads_factor {adj_humads:.4f}',
    '__Adjusted_humus_factor {adj_humus:.4f}',
    '__Humads_C/N {cn_humads:.4f}',
    '__Humus_C/N {cn_humus:.4f}',
    '__Black_C {black_c:.4f}',
    '__Black_C_C/N {black_cn:.4f}',
    '__SOC_profile_A {soc_profile_a:.4f}',
    '__SOC_profile_B {soc_profile_b:.4f}',
    '__Initial_nitrate_ppm {nit_ppm:.4f}',
    '__Initial_ammonium_ppm {amm_ppm:.4f}',
    '__Soil_microbial_index {soil_micr_ind:.4f}',
    '__Soil_slope {slope:.4f}',
    '__Lateral_influx_index {lat_inf_ind:.4f}',
    '__Watertable_depth {wt_depth:.4f}',
    '__Water_retension_layer_depth {wrl_depth:.4f}',
    '__Soil_salinity {salinity:.4f}',
    '__SCS_curve_use {scs_curve:.0f}',
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0']

CROP_DATA = [
    '----------------------------------------',
    'Crop_data',
    '',
    'Cropping_systems {rotations:.0f}']

CROP_ROTATIONS = [
    '',
    '__Cropping_system {rotation:.0f}',
    '__Total_years {years:.0f}',
    '__Years_of_a_cycle {cycles:.0f}']

CROP_CYCLE = [
    '',
    '____Year {cycle:.0f}',
    '____Crops {crops:.0f}']

CROPS = [
    '______Crop# {crop:.0f}',
    '______Crop_ID {id_dndc:.0f}',
    '______Planting_month {plant_mo:.0f}',
    '______Planting_day {plant_day:.0f}',
    '______Harvest_month {harv_mo:.0f}',
    '______Harvest_day {harv_day:.0f}',
    '______Harvest_year {harv_year:.0f}',
    '______Residue_left_in_field {residue:.4f}',
    '______Maximum_yield {max_yield:.4f}',
    '______Leaf_fraction {frac_leaf:.4f}',
    '______Stem_fraction {frac_stem:.4f}',
    '______Root_fraction {frac_root:.4f}',
    '______Grain_fraction {frac_grain:.4f}',
    '______Leaf_C/N {cn_leaf:.4f}',
    '______Stem_C/N {cn_stem:.4f}',
    '______Root_C/N {cn_root:.4f}',
    '______Grain_C/N {cn_grain:.4f}',
    '______Accumulative_temperature {tdd:.4f}',
    '______Optimum_temperature {optt:.4f}',
    '______Water_requirement {h2o_demand:.4f}',
    '______N_fixation_index {nfix:.4f}',
    '______Vascularity {vascularity:.4f}',
    '______If_cover_crop {cover:.0f}',
    '______If_perennial_crop {perennial:.0f}',
    '______If_transplanted {transplanted:.0f}',
    '______Tree_maturity_age {tree_maturity:.4f}',
    '______Tree_current_age {tree_current_age:.4f}',
    '______Tree_max_leaf {tree_max_leaf:.4f}',
    '______Tree_min_leaf {tree_min_leaf:.4f}',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '']

TILL = [
    '----------------------------------------',
    '____Till_applications {tillages:.0f}']

TILLS =[
    '______Till# {till:.0f}',
    '______Till_month {mo:.0f}',
    '______Till_day {day:.0f}',
    '______Till_method {depth_code:.0f}']

FERT = [
    '----------------------------------------',
    '____Fertilizer_applications {fertilizations:.0f}']

FERTS = [
    '______Fertilizing# {fert:.0f}',
    '______Fertilizing_month {mo:.0f}',
    '______Fertilizing_day {day:.0f}',
    '______Fertilizing_method {meth:.0f}',
    '______Fertilizing_depth {depth:.4f}',
    '______Nitrate {nit:.4f}',
    '______Ammonium_bicarbonate {ammbic:.4f}',
    '______Urea {urea:.4f}',
    '______Anhydrous_ammonia {anh:.4f}',
    '______Ammonium {amm:.4f}',
    '______Sulphate {sulf:.4f}',
    '______Phosphate {phos:.4f}',
    '______Slow_release_rate {slow_rel:.4f}',
    '______Nitrification_inhibitor_efficiency {nitr_inhib_eff:.4f}',
    '______Nitrification_inhibitor_duration {nitr_inhib_dur:.4f}',
    '______Urease_inhibitor_efficiency {urease_inhib_eff:.4f}',
    '______Urease_inhibitor_duration {urease_inhib_dur:.4f}',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0']

FERT_OPTION = [
    '____Fertilization_option {fert_option:.0f}']                  

MANU = [
    '----------------------------------------',
    '____Manure_applications {manus:.0f}']

MANUS = [
    '______Manuring# {application:.0f}',
    '______Manuring_month {mo:.0f}',
    '______Manuring_day {day:.0f}',
    '______Manure_amount {orgc:.5f}',
    '______Manure_C/N {cn:.4f}',
    '______Manure_type {type:.0f}',
    '______Manuring_method {meth:.0f}',
    '______Manure_depth {depth:.4f}',
    '______Manure_OrgN {orgn:.4f}',
    '______Manure_NH4 {nh4:.4f}',
    '______Manure_NO3 {no3:.4f}',
    '______None 0']

MULCH = [
    '----------------------------------------',
    '____Film_applications 0',
    '____Method 0',
    '----------------------------------------']

FLOOD = [
    '____Flood_applications {floods:.0f}',
    '____Water_control {water_cont:.0f}',
    '____Flood_water_N {flood_n:.4f}',
    '____Leak_rate {leak_rate:.4f}',
    '____Water_gather_index {water_gather:.4f}',
    '____Watertable_file None',
    '____Empirical_para_1 0.0000',
    '____Empirical_para_2 0.0000',
    '____Empirical_para_3 0.0000',
    '____Empirical_para_4 0.0000',
    '____Empirical_para_5 0.0000',
    '____Empirical_para_6 0.0000']

FLOODS = [
    '______Flooding# {flood:.0f}',
    '______Start_month {mo_start:.0f}',
    '______Start_day {day_start:.0f}',
    '______End_month {mo_end:.0f}',
    '______End_day {day_end:.0f}',
    '______Water_N 0.0000',
    '______Alter_wet_dry {awd:.0f}',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0']

IRRI = [
    '----------------------------------------',
    '____Irrigation_applications {irris:.0f}',
    '____Irrigation_control {irri_type:.0f}',
    '____Irrigation_index {irri_index:.4f}',
    '____Irrigation_method {irri_meth:.0f}']

IRRIS = [
    '______Irrigation# {irri:.0f}',
    '______Irri_month {mo:.0f}',
    '______Irri_day {day:.0f}',
    '______Water_amount {amt:.4f}',
    '______Irri_method {meth:.0f}',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0']
    
GRAZE = [
    '----------------------------------------',
    '____Grazing_applications {grazes:.0f}']

GRAZES = [
    '______Grazing#  {graze:.0f}',
    '______Start_month {mo_start:.0f}',
    '______Start_day {day_start:.0f}',
    '______End_month {mo_end:.0f}',
    '______End_day {mo_end:.0f}',
    '______Dairy_heads {head_dairy:.4f}',
    '______Beef_heads {head_replace:.4f}',
    '______Pig_heads 0.0000',   # not implemented
    '______Sheep_heads 0.0000', # not implemented
    '______Horse_heads 0.0000', # not implemented
    '______Grazing_hours/day {hours:.1f}',
    '______Additional_feed {feed:.4f}',
    '______Additional_feed_C/N {feed_cn:.4f}',
    '______Excreta_handling {excreta:.0f}',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0']
    
CUT = [
    '____Cut_applications {cuts:.0f}']

CUTS = [
    '______Cut# {cut:.0f}',
    '______Cut_month {mo:.0f}',
    '______Cut_day {day:.0f}',
    '______Cut_fraction {frac:.4f}',
    '______Cut_part {part:.0f}',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0']

END_OF_FILE = [
    '----------------------------------------']