
import os, sys
from os.path import join as opj
import shutil
import hashlib
import glob
import time
import zipfile
import datetime as dt

### PATHS
LOCAL_PROJECTS   = 'C:\\ManureDNDC\\projects\\'
NETWORK_PROJECTS = 'S:'

### PATHS / NAMES
PROJECT = 'dairy_cap'
TASK    = 'task2_bmp'
PATH_LOCAL = opj(
    LOCAL_PROJECTS,
    PROJECT)
PATH_NETWORK = opj(
    NETWORK_PROJECTS,
    PROJECT,
    TASK)

###
NAP_TIME = 60*2

def md5(path_file):
    '''
    lifted this from:
    http://stackoverflow.com/questions/16874598/how-do-i-calculate-the-md5-checksum-of-a-file-in-python
    and
    http://stackoverflow.com/questions/3431825/generating-an-md5-checksum-of-a-file
    '''
    
    # check that path exists
    if not os.path.exists(path_file):
        return None
    
    hash_md5 = hashlib.md5()
    with open(path_file, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def get_dir_l(path_batch):
    """
    This function gets a list of batch result directories and returns:
     (1) the list of directories
     (2) the path of the "active" directory (i.e. the one DNDC is currently processing)
    """
    
    # if the batch directory path does not exist yet, return an empty list
    if os.path.exists(path_batch): pass
    else:
        return [],''
    
    # create a list of directories in the batch directory
    list = glob.glob(path_batch + '\\*')
    list.sort()
    dir_l = []
    for item in list:
        if os.path.isdir(item):
            dir_l.append(item)
    
    # if there are no results yet, return the empty list
    if len(dir_l) == 0: return dir_l,''
    
    # if there is only one directory, return the directory list and the one path
    if len(dir_l) == 1: return dir_l,dir_l[-1]
    
    # if there is more than one result directory,
    # return the directory list and the last active path
    mdir_l = []
    for dir in dir_l:
        # get the modification time for the directory
        mod_time = os.path.getmtime(dir)
        mdir_l.append((mod_time,dir))
    mdir_l.sort()
    
    return dir_l,mdir_l[-1][-1]

def copy_and_remove(path_file, path_copy):
    
    ''' MOVE FILE '''
    try:
        shutil.copy2(
            path_file,
            path_copy)

    except:
        pass
    
    ''' VERIFY MOVE '''
    md5_orig = md5(path_file)
    md5_copy = md5(path_copy)
    
    result = 'checksum error - not copied'
    if md5_orig == md5_copy:
        os.remove(path_file)
        result = 'copied'

    return result
    
def copy_and_zip(dir_l, active_dir_path, path_batch, complete):
    
    # define paths
    path_zip = os.path.join(
        PATH_NETWORK,
        'zip')

    # loop through the directories and process them
    for dir in dir_l:

        # if the directory to process is still active (and the batch is still running)
        # do not process the directory
        if dir == active_dir_path and complete == 0: continue
        
        # get the directory name
        dirpath, dirname = os.path.split(dir)

        # create the zip file
        zfilepath = os.path.join(
            path_batch,
            dirname + '-' + os.getenv('COMPUTERNAME') + '.zip')
        zfile = zipfile.ZipFile(zfilepath,'w')

        # loop through the files and add them to the zip file
        for root, dirs, files in os.walk(dir):
            for file in files:
                path_actual = os.path.join(root, file)
                path_zippable = path_actual.replace(dirpath, '')
                zfile.write(path_actual, path_zippable, zipfile.ZIP_DEFLATED)

        zfile.close()
        
        # remove result directory
        shutil.rmtree(dir)
        
        ''' MOVE ZIPFILE FROM BATCH DIRECTORY '''
        copy2path = zfilepath.replace(path_batch, path_zip)
        result = copy_and_remove(zfilepath, copy2path)
        
        print '    ', dirname, result

    ''' CHECK FOR LEFTOVERS '''
    remaining_zips = glob.glob(opj(path_batch,'*.zip'))
    if len(remaining_zips) > 0:
        print  '   copying remaining zip files...'
        for path_file in remaining_zips:
            zipname = os.path.basename(path_file)
            copy2path = path_file.replace(path_batch, path_zip)
            result = copy_and_remove(path_file, copy2path)
            print '    ', zipname, result

    return

def process_batch():

    # define batch directory
    path_batch = opj(
        'C:\\',
        'ManureDNDC',
        'RESULT',
        'Record',
        'Batch')

    # create the output info
    computer = os.getenv('COMPUTERNAME') # computer name

    # initiate a variable that tracks whether or not the run is complete.
    complete = 0
    
    # initiate a variable that tracks the last active result path
    last_active_dir_path = ''
    
    # while DNDC is still creating new directories, keep going...
    while complete == 0:

        # get a list of site directories in C:\DNDC\RESULT\Record\Batch\
        # get the path of the last directory (the latest modify time)
        dir_l, active_dir_path = get_dir_l(path_batch)

        if len(dir_l) == 0:
            print 'waiting for batch run start...'
            time.sleep(NAP_TIME)
            continue
        
        if len(dir_l) == 1 and active_dir_path != last_active_dir_path:
            print 'waiting for results to accumulate...'
            time.sleep(NAP_TIME)
            continue
        
        if len(dir_l) == 1 and active_dir_path == last_active_dir_path:
            print 'only one result directory...'
            print 'waiting to see if DNDC is done...'
            mod_time = os.path.getmtime(active_dir_path)
            time.sleep(NAP_TIME)
            new_mod_time = os.path.getmtime(active_dir_path)
            if mod_time == new_mod_time:
                print 'it looks like DNDC is done...'
                complete = 1
            else:
                continue

        print 'zipping and removing results'
        copy_and_zip(
            dir_l,
            active_dir_path,
            path_batch,
            complete)

        if complete == 0:
            print 'napping @', dt.datetime.now()
            time.sleep(NAP_TIME)
        
        last_active_dir_path = active_dir_path
    
    return

if __name__ == '__main__':
    process_batch()
    
