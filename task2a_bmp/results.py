#!/usr/bin/env python

import os, sys
from os.path import join as opj
import ags_network
import convert
from utilities import jday_to_date
import pandas as pd
import numpy as np
import glob
import zipfile
import datetime as dt
from collections import OrderedDict as OD

# global variables for network paths
PROJECT = 'dairy_cap'
PATH    = ags_network.paths(PROJECT)

# task name
TASK = 'task2a_bmp'

def parser():

    warning = 'indicate post-processing step as -d (--DNDC) or -m (--ManureDNDC)'

    try:
        arg = sys.argv[1]
    except IndexError:
        print warning
        sys.exit()

    global SOFTWARE

    SOFTWARE = None

    if (arg == '-d') or (arg.lower() == '--dndc'):
        SOFTWARE = 'DNDC'
    elif (arg == '-m') or (arg.lower() == '--manuredndc'):
        SOFTWARE = 'ManureDNDC'
    else:
        print warning
        sys.exit()

    return

def read_db(path_db):

    # initialize DB
    db = {}

    ### GET SITES ###
    df_site = pd.read_excel(
        path_db,
        sheetname='site',
        skiprows=[0])
    df_site.set_index('site', inplace=True)

    db['site'] = df_site

    ### GET SCENARIOS ###
    df_scen = pd.read_excel(
        path_db,
        sheetname='scenario',
        skiprows=[0])
    df_scen.set_index('scenario', inplace=True)

    db['scen'] = df_scen

    ### GET CLIMATE ###
    df_clim = pd.read_excel(
        path_db,
        sheetname='climate',
        skiprows=[0])
    df_clim.set_index('clim', inplace=True)

    db['clim'] = df_clim

    # create a thing that equates simulation and calendar years
    dfs = []
    for clim in list(df_clim.index):
        years_init = db['clim']['years_init'][clim]
        year_cal_start = db['clim']['year_cal_start'][clim]
        year_cal_end = db['clim']['year_cal_end'][clim]

        rows = []
        for year_num, year_cal in enumerate(range(year_cal_start - years_init, year_cal_end+1)):
            year_sim = year_num + 1
            rows.append([clim, year_sim, year_cal])
        df = pd.DataFrame(rows, columns=['clim','year_sim','year_cal'])
        df.set_index(['clim','year_sim'], inplace=True)
        dfs.append(df)
    df_years = pd.concat(dfs)
    db['years'] = df_years

    ### GET FIELDS ###
    df_field = pd.read_excel(
        path_db,
        sheetname='field_dndc',
        skiprows=[0])
    df_field.dropna(subset=['scenario'], inplace=True)
    df_field.set_index(['site', 'scenario', 'field', 'mgt_year'], inplace=True)

    db['field'] = df_field

    ### GET CROPS ###
    df_crop = pd.read_excel(
        path_db,
        sheetname='crop',
        skiprows=[0])
    df_crop.set_index('id_db', inplace=True)

    db['crop'] = df_crop

    ### GET FEEDLOTS ###
    df_fl = pd.read_excel(
        path_db,
        sheetname='feedlot',
        skiprows=[0])
    df_fl.set_index(['scenario', 'feedlot'], inplace=True)

    db['feedlot'] = df_fl

    return db

def get_livestock_table(lines, index_start, index_end, index_name):

    # convert feedlot lines to table
    table = lines[index_start:index_end]
    table = [l.strip().split() for l in table]
    table = [x for x in table if x != []]

    # convert to DataFrame
    df = pd.DataFrame(table[1:], columns=table[0])
    
    # change index name and set it
    df.columns = ['result'] + df.columns.tolist()[1:]
    df.set_index('result', inplace=True)

    # convert bad output to NaN
    df = df.replace(
        to_replace='-nan(ind)',
        value=np.nan)
    
    # convert to float
    df = df.astype('float')
    
    # sum columns
    df['total'] = df.sum(axis=1)

    # convert bad output to NaN
    # for c in df.columns:
        # df[c] = pd.to_numeric(df[c], errors='coerce')

    return df

def get_livestock(lines):

    # structure dictionary
    data = {
        'herd':     {},
        'feedlots': {},
        'compost':  None,
        'digester': None,
        'lagoon':   None,
        'area':     {},
        'fields':   {}}

    for linenum,line in enumerate(lines):
        
        # find herd sizes by feedlot
        if line[:8] == 'FEEDLOT:':
            
            number    = int(line.strip().split(':')[-1])
            herd_size = int(lines[linenum+1].strip().split(':')[-1]) 
            
            data['herd'][number] = herd_size

        # find feedlots
        if line[:8] == 'FEEDLOT_':

            # get feedlot number
            number = int(line.strip().split()[0].split('_')[-1])

            df = get_livestock_table(
                lines,
                linenum,
                linenum+27,
                'FEEDLOT_' + str(number))
            data['feedlots'][number] = df

        # find compost
        if line[:7] == 'COMPOST':

            df = get_livestock_table(
                lines,
                linenum,
                linenum+18,
                'COMPOST')
            data['compost'] = df

        # find digester
        if line[:8] == 'DIGESTER':

            df = get_livestock_table(
                lines,
                linenum,
                linenum+17,
                'DIGESTER')
            data['digester'] = df

        # find lagoon
        if line[:6] == 'LAGOON':

            df = get_livestock_table(
                lines,
                linenum,
                linenum+18,
                'LAGOON')
            data['lagoon'] = df

        # find fields
        if line[:6] == 'FIELD_':
            
            # get field number
            number = int(line.strip().split()[0].split('_')[1])

            # get area
            area = float(line.split()[0].split('_')[-1].replace('ha',''))
            data['area'][number] = area
            
            # get table as DF
            df = get_livestock_table(
                lines,
                linenum,
                linenum+34,
                line.strip().split()[0])
            data['fields'][number] = df

    return data

def get_crop(path_crop, years_init):

    # initialize dictionary
    data = {}

    # get all Field directories
    path_glob = os.path.join(
        path_crop,
        'Field_*')
    globber = glob.glob(path_glob)

    for path_field in globber:

        dirname = os.path.basename(path_field)
        number = int(dirname.split('_')[-1])

        path_summary = os.path.join(
            path_field,
            'Multi_year_summary.csv')

        df = pd.read_csv(
            path_summary,
            skiprows=[0,1,3])

        df.columns = [c.strip() for c in list(df.columns)]
        df.set_index('Year', inplace=True)

        df = df.loc[df.index > years_init]

        data[number] = df

    return data

def get_cncps():
    
    ### SET PATHS ###
    path_task = opj(
        PATH.projects,
        TASK)
    
    path_mgt = opj(
        path_task,
        'management',
        'cncps')
    
    ### READ-IN DATA ###
    path_data = opj(
        path_mgt,
        'WI NY Herd Data IFSM RationsRevised_pi.xlsx')
    
    dfs = []
    for state, site in [
        ('ny', 1),
        ('wi', 2)]:
        
        # set fields to retain
        index_fields = [
            'scenario',
            'site']
        data_fields = [
            'cows', 
            'milk', 
            'enteric_ch4']
        
        ### EXTRACT MILK COW DATA ###
        df_milk = pd.read_excel(
            path_data,
            sheetname='dndc_' + state + '_milk',
            skiprows=[0])
        df_milk['site'] = site

        # set index
        df_milk = df_milk[index_fields + data_fields]
        df_milk.set_index(index_fields, inplace=True)
        
        # retain number of milk cows
        df_milk['milk_cows'] = df_milk['cows']
        
        ### EXTRACT REPLACEMENT COWS ###
        df_replace = pd.read_excel(
            path_data,
            sheetname='dndc_' + state + '_replace',
            skiprows=[0])
        df_replace['site'] = site
        
        # set index
        df_replace = df_replace[index_fields + data_fields]
        df_replace.set_index(index_fields, inplace=True)
        
        # account for milk cows
        df_replace['milk_cows'] = 0
        
        ### PROCESS FINAL DATAFRAME ###
        # sum cows and daily rates
        df = (df_milk + df_replace)
        
        # calculate annual value
        df['milk'] = df['milk'] * 365.
        df['enteric_ch4'] = df['enteric_ch4'] * 365.
        
        # append to list
        dfs.append(df)
    
    ### CONCATENATE ###
    df = pd.concat(dfs)

    return df


def applications():

    ### SET PATHS ###
    path_sim = opj(
        PATH.projects,
        TASK,
        'simulation')

    path_results = opj(
        path_sim,
        'results')

    path_zip = opj(
        PATH.scratch,
        TASK,
        'zip')

    ### READ SIMULATION DATABASE ###
    path_db = opj(
        path_sim,
        'database.xlsx')
    db = read_db(path_db)

    ### GLOB ZIP FILES ###
    path_glob = opj(
        path_zip,
        'Case*.zip')
    globber = glob.glob(path_glob)

    ### PROCESS RESULTS ###

    # initialize list of DFs
    dfs = []

    # loop through result directories
    for path_zip in globber:

        path_dir, filename = os.path.split(path_zip)
        dirname = os.path.basename(path_dir)
        site, clim, scen = [int(i) for i in filename.split('-')[1:4]]

        print site, clim, scen

        # open zip archive
        zfile = zipfile.ZipFile(path_zip, 'r')

        # get list of top-level directories
        # (assumes there's only 1 directory in zip file)
        case_dir = zfile.namelist()[0].split('/')[0]

        ### LOOP THROUGH ALL YEARS (INCLUDE INITIALIZATION YEARS) ###
        year_cal_start = db['clim']['year_cal_start'][clim]
        year_cal_end   = db['clim']['year_cal_end'][clim]
        years_init   = db['clim']['years_init'][clim]

        # initialize list of years
        years = []

        # part 1: initialization
        for y in range(years_init):
            year_sim = y+1
            year_cal = range(year_cal_start, year_cal_end+1)[y]
            years.append((year_sim, year_cal))

        # part 2: summary years
        for year_cal in range(year_cal_start, year_cal_end+1):
            year_sim = year_cal - year_cal_start + years_init + 1
            years.append((year_sim, year_cal))

        for year_sim, year_cal in years:

            # get daily lagoon file
            zpath_file = case_dir + '/LivestockSystem/Day_Lagoon_' + str(year_sim) + '.csv'

            file = zfile.open(zpath_file)

            df = pd.read_csv(
                file,
                skiprows=[0,1,2,4,5],
                sep=',')

            df['site'] = site
            df['clim'] = clim
            df['scen'] = scen

            df['year_sim'] = year_sim
            df['year_cal'] = year_cal

            df['date'] = pd.to_datetime(
                df['year_cal'] * 1000 +
                df['Day'],
                format='%Y%j')

            df['applied'] = (
                df['Removed_C'] +
                df['Removed_N'] +
                df['water_to_field'] +
                df['P_to_field'])

            df_app = df.loc[df['applied'] > 0.].copy(deep=True)

            df_app = df_app[[
                'site',
                'clim',
                'scen',
                'year_sim',
                'year_cal',
                'date',
                'applied',
                'Removed_C',
                'Removed_N',
                'water_to_field',
                'P_to_field']]

            # get urea, NH4, and NO3
            df_pool = df.copy(deep=True)
            df_pool['date'] = df_pool['date'] + dt.timedelta(days=1)

            df_pool = df_pool.loc[df_pool['date'].isin(df_app['date'].tolist())]

            value_fields = [
                'date',
                'N_pool',
                'MON-pool',
                'Urea-pool',
                'NH4-pool',
                'NO3-pool',
                'NH3-pool',
                'NO2-pool',
                'NO-pool',
                'N2O-pool',
                'N2-pool']

            df_app = df_app.join(
                df_pool[value_fields].set_index('date'),
                on='date')

            df_app.columns = [
                'site',
                'clim',
                'scen',
                'year_sim',
                'year_cal',
                'date',
                'applied',
                'app_c',
                'app_n',
                'app_h2o',
                'app_p',
                'pool_n',
                'pool_mon',
                'pool_urea',
                'pool_nh4',
                'pool_no3',
                'pool_nh3',
                'pool_no2',
                'pool_no',
                'pool_n2o',
                'pool_n2']

            dfs.append(df_app)

    ### CONCATENATE DFs ###
    df = pd.concat(dfs)

    ### WRITE RESULTS ###
    path_apps = opj(
        path_results,
        'manure_applications.csv')

    df.to_csv(path_apps, index=False)

    return

def feed_efficiency():
    
    ### SET PATHS ###
    path_sim = opj(
        PATH.projects,
        TASK,
        'simulation')

    path_results = opj(
        path_sim,
        'results')

    path_zip = opj(
        PATH.scratch,
        TASK,
        'zip')
        # 'zip_new_livestock')

    ### READ SIMULATION DATABASE ###
    path_db = opj(
        path_sim,
        'database.xlsx')
    db = read_db(path_db)

    ### GLOB ZIP FILES ###
    path_glob = opj(
        path_zip,
        'Case*.zip')
    globber = glob.glob(path_glob)

    ### PROCESS RESULTS ###

    # initialize rows for table
    rows = []

    # loop through result directories
    for path_zip in globber:

        path_dir, filename = os.path.split(path_zip)
        dirname = os.path.basename(path_dir)
        site, clim, scen = [int(i) for i in filename.split('-')[1:4]]
        
        # get site name
        site_name = db['site']['site_name'][site]
        
        # get climate dataset name
        clim_name = db['clim']['model'][clim]
        
        # get scenario name
        scen_name = db['scen']['name_tables'][scen]

        # if site == 2: continue
        # if scen > 1: continue
        
        print site, clim, scen

        # open zip archive
        zfile = zipfile.ZipFile(path_zip, 'r')

        # get list of top-level directories
        # (assumes there's only 1 directory in zip file)
        case_dir = zfile.namelist()[0].split('/')[0]

        ### LOOP THROUGH ALL YEARS (INCLUDE INITIALIZATION YEARS) ###
        year_cal_start = db['clim']['year_cal_start'][clim]
        year_cal_end   = db['clim']['year_cal_end'][clim]
        years_init   = db['clim']['years_init'][clim]

        # initialize list of years
        years = []

        # get summary years
        for year_cal in range(year_cal_start, year_cal_end+1):
            year_sim = year_cal - year_cal_start + years_init + 1
            years.append((year_sim, year_cal))

        # loop through summary years
        for year_sim, year_cal in years:

            # get annual livestock report
            zpath_file = case_dir + '/LivestockSystem/AnnualReport_LivestockSystem_yr' + str(year_sim) + '.txt'

            # loop through lines
            file = zfile.open(zpath_file)
            lines = file.readlines()
            file.close()
            
            # extract
            data = get_livestock(lines)
            
            # get feed characteristics
            for line in lines:
                if 'FEED:' in line:
                    line = line.strip()
                    dmi, cp, p = line.split()
                    dmi = float(dmi.replace('FEED:rate:','').replace('_kg_DM/head/day;','')) * 365.
                    cp_frac = float(cp.replace('crude_protein:','').replace('_percent;','')) * 0.01
                    p_frac = float(p.replace('P:','').replace('_percent','')) * 0.01
                    # example line:
                    #   FEED:rate:21.20_kg_DM/head/day; crude_protein:16.96_percent; P:0.35_percent
                    break

            # get herd size
            herd = float(data['herd'][1])
            
            # get milkbarn (feedlot #1) table
            df_milk = data['feedlots'][1]
            
            feed_cp  = dmi * cp_frac
            feed_c   = df_milk['C_kg']['Feed_intake'] / herd
            feed_n   = df_milk['N_kg']['Feed_intake'] / herd
            feed_h2o = df_milk['Water_kg']['Feed_intake'] / herd
            feed_p   = df_milk['P_kg']['Feed_intake'] / herd
            milk_cp  = feed_cp * 0.18   # from Justin: DNDC's apparent efficiency (not 25%)
            milk_c   = df_milk['C_kg']['Milk_production'] / herd
            milk_n   = df_milk['N_kg']['Milk_production'] / herd
            milk_h2o = df_milk['Water_kg']['Milk_production'] / herd
            milk_p   = df_milk['P_kg']['Milk_production'] / herd

            # feed fractions
            feed_total = (
                feed_c + 
                feed_n +
                feed_h2o +
                feed_p)
            feed_frac_cp  = feed_cp / feed_total
            feed_frac_c	  = feed_c / feed_total 
            feed_frac_n	  = feed_n / feed_total
            feed_frac_h2o = feed_h2o / feed_total
            feed_frac_p   = feed_p / feed_total
            
            # milk fractions
            milk_total = (
                milk_c + 
                milk_n +
                milk_h2o +
                milk_p)
            milk_frac_cp  = milk_cp / milk_total
            milk_frac_c	  = milk_c / milk_total 
            milk_frac_n	  = milk_n / milk_total
            milk_frac_h2o = milk_h2o / milk_total
            milk_frac_p   = milk_p / milk_total

            # feed N efficiency
            feed_eff = milk_n / feed_n

            # define row
            row = [
                site,
                site_name,
                scen,
                scen_name,
                clim,
                clim_name,
                year_sim,
                year_cal,
                herd,
                dmi,
                cp_frac,
                p_frac,
                feed_cp, 
                feed_c,  
                feed_n,  
                feed_h2o,
                feed_p,
                feed_frac_cp,
                feed_frac_c,	 
                feed_frac_n,	 
                feed_frac_h2o,
                feed_frac_p,
                milk_cp, 
                milk_c,  
                milk_n,  
                milk_h2o,
                milk_p,
                milk_frac_cp,
                milk_frac_c,	 
                milk_frac_n,	 
                milk_frac_h2o,
                milk_frac_p,
                feed_eff]
            
            # append to rows
            rows.append(row)
    
    # define header
    header = [
        'site',
        'site_name',
        'scen',
        'scen_nam',
        'clim',
        'clim_name',
        'year_sim',
        'year_cal',
        'milkcows',
        'dmi',
        'cp_frac',
        'p_frac',
        'feed_cp', 
        'feed_c',  
        'feed_n',  
        'feed_h2o',
        'feed_p',  
        'feed_frac_cp',
        'feed_frac_c',	 
        'feed_frac_n',	 
        'feed_frac_h2o',
        'feed_frac_p',
        'milk_cp', 
        'milk_c',  
        'milk_n',  
        'milk_h2o',
        'milk_p',
        'milk_frac_cp',
        'milk_frac_c',	 
        'milk_frac_n',	 
        'milk_frac_h2o',
        'milk_frac_p',
        'feed_eff']
    
    # create DF
    df = pd.DataFrame(rows, columns=header)

    # group by site / scen / clim
    index_fields = [
        'site',
        'site_name',
        'scen',
        'scen_nam',
        'clim',
        'clim_name']
    df = df.groupby(index_fields, as_index=False).mean()
    del df['year_sim']
    del df['year_cal']
    
    # write df
    path_df = opj(
        path_sim, 
        'results',
        'feed_efficiency.csv')
    df.to_csv(path_df, index=False)
    
    return

def summarize():

    ### SET PATHS ###
    path_sim = opj(
        PATH.projects,
        TASK,
        'simulation')

    path_results = opj(
        path_sim,
        'results')

    path_zip = opj(
        PATH.scratch,
        TASK,
        'zip')
        # 'zip_test')

    ### READ SIMULATION DATABASE ###
    path_db = opj(
        path_sim,
        'database.xlsx')
    db = read_db(path_db)

    ### GET CNCPS MILK AND ENTERIC EMISSIONS ###
    ''' WE'RE NOT USING CNCPS AS OF 10/3/2016 
    df_cncps = get_cncps()
        WE'RE NOT USING CNCPS AS OF 10/3/2016 '''

    ### GLOB ZIP FILES ###
    path_glob = opj(
        path_zip,
        'Case*.zip')
    globber = glob.glob(path_glob)

    ### PROCESS RESULTS ###

    # initialize lists of DFs
    farm_dfs = []
    field_dfs = []

    # loop through result directories
    for path_zip in globber:

        path_dir, filename = os.path.split(path_zip)
        dirname = os.path.basename(path_dir)
        site, clim, scen = [int(i) for i in filename.split('-')[1:4]]
        
        # get scenario name
        scen_name = db['scen']['name_tables'][scen]

        print site, clim, scen

        # open zip archive
        zfile = zipfile.ZipFile(path_zip, 'r')

        # get list of top-level directories
        # (assumes there's only 1 directory in zip file)
        case_dir = zfile.namelist()[0].split('/')[0]

        ### LOOP THROUGH ALL YEARS (INCLUDE INITIALIZATION YEARS) ###
        year_cal_start = db['clim']['year_cal_start'][clim]
        year_cal_end   = db['clim']['year_cal_end'][clim]
        years_init   = db['clim']['years_init'][clim]

        # initialize list of years
        years = []

        # get summary years
        for year_cal in range(year_cal_start, year_cal_end+1):
            year_sim = year_cal - year_cal_start + years_init + 1
            years.append((year_sim, year_cal))

        # loop through summary years
        for year_sim, year_cal in years:

            # get annual livestock report
            zpath_file = case_dir + '/LivestockSystem/AnnualReport_LivestockSystem_yr' + str(year_sim) + '.txt'

            # loop through lines
            file = zfile.open(zpath_file)
            lines = file.readlines()
            file.close()

            # extract
            data = get_livestock(lines)

            ### BARNS (FEEDLOTS)        ###
            ### "Animal & barn" SECTION ###

            # convert DFs to Panel and sum
            p_barn = pd.Panel(
                {
                    'milk': data['feedlots'][1], 
                    'replacement': data['feedlots'][2],
                    'parlor': data['feedlots'][3]
                    })
            df_barn = p_barn.sum(axis=0)

            # extract barn variables
            
            # NOTE: we're not reporting milk (using IFSM results)
            milk = np.nan
            
            # NOTE: we have no meat production value because excretion 
            #       data from IFSM was used...
            meat = np.nan
            
            excreted_n = (
                df_barn['N_kg']['Urine_excretion'] + 
                df_barn['N_kg']['Feces_excretion'])
            excreted_p = (
                df_barn['P_kg']['Urine_excretion'] + 
                df_barn['P_kg']['Feces_excretion'])                
            
            # NOTE: do not report enteric N2O (using IFSM results)
            n2o_enteric = np.nan
            
            # NOTE: enteric CH4 is based on IFSM simulations
            ch4_enteric = np.nan

            nh3_barn = df_barn['N_kg']['Floor_NH3_emission']
            n2o_barn = df_barn['N_kg']['Floor_N2O_emission']
            n2_barn = df_barn['N_kg']['Floor_N2_emission']
            no_barn = df_barn['N_kg']['Floor_NO_emission']          
            n_from_barn = (
                df_barn['N_kg']['Export_to_compost'] + 
                df_barn['N_kg']['Export_to_lagoon'] + 
                df_barn['N_kg']['Export_to_digester'])
            p_from_barn = (
                df_barn['P_kg']['Export_to_compost'] + 
                df_barn['P_kg']['Export_to_lagoon'] + 
                df_barn['P_kg']['Export_to_digester'])            

            ### STORAGE (LAGOON, DIGESTER, COMPOST ###
            ### "Manure management" SECTION        ###
            
            # extract storage N variables
            nh3_storage = (
                data['compost']['N_kg']['Compost_NH3_emission'] + 
                data['digester']['N_kg']['Digester_NH3_emission'] + 
                data['lagoon']['N_kg']['Lagoon_NH3_emission'])
            n2o_storage = (
                data['compost']['N_kg']['Compost_N2O_emission'] + 
                data['digester']['N_kg']['Digester_N2O_emission'] + 
                data['lagoon']['N_kg']['Lagoon_N2O_emission'])
            n2_storage = (
                data['compost']['N_kg']['Compost_N2_emission'] + 
                data['digester']['N_kg']['Digester_N2_emission'] + 
                data['lagoon']['N_kg']['Lagoon_N2_emission'])
            no_storage = (
                data['compost']['N_kg']['Compost_NO_emission'] + 
                data['digester']['N_kg']['Digester_NO_emission'] + 
                data['lagoon']['N_kg']['Lagoon_NO_emission'])

            ###################################################
            ### NOTE: SEPARATED BIOGAS FROM OTHER EMISSIONS ###
            ###################################################
            
            '''
            # account for digester-combusted CH4 (assume 97% efficiency
            # - based on AGGP work and Al Rotz e-mail, 7/13/2016, 11:37am)
            ch4_digester = data['digester']['C_kg']['Digester_CH4_emission'] * 0.03
            co2_digester = (
                data['digester']['C_kg']['Digester_CH4_emission'] * 0.97 + 
                data['digester']['C_kg']['Digester_CO2_emission'])
            '''

            # digester
            biogas = data['digester']['C_kg']['Digester_CH4_emission']
            co2_digester = data['digester']['C_kg']['Digester_CO2_emission']
            
            '''
            # account for flare efficiency (assume 97% efficiency)
            # - based on Al Rotz e-mail, 7/13/2016, 12:16pm)
            if 'flare' in scen_name:
                ch4_lagoon = data['lagoon']['C_kg']['Lagoon_CH4_emission'] * 0.03
                co2_lagoon = (
                    data['lagoon']['C_kg']['Lagoon_CH4_emission'] * 0.97 + 
                    data['lagoon']['C_kg']['Lagoon_CO2_emission'])
            else:
                ch4_lagoon = data['lagoon']['C_kg']['Lagoon_CH4_emission']
                co2_lagoon = data['lagoon']['C_kg']['Lagoon_CO2_emission']
            
            # extract storage C variables
            ch4_storage = (
                data['compost']['C_kg']['Compost_CH4_emission'] + 
                ch4_digester + 
                ch4_lagoon)
            '''
            
            # compost
            ch4_compost = data['compost']['C_kg']['Compost_CH4_emission']
            
            ''' IGNORE SOLID STORAGE (COMPOST) EMISSIONS FOR NY PER AL ROTZ/IFSM '''
            site_name = db['site']['site_name'][site]
            if site_name.lower() == 'ny':
                ch4_compost = 0.0
            
            # lagoon
            ch4_lagoon = data['lagoon']['C_kg']['Lagoon_CH4_emission']
            co2_lagoon = data['lagoon']['C_kg']['Lagoon_CO2_emission']
            
            # account for flared lagoon
            if 'flare' in scen_name:
                biogas = biogas + ch4_lagoon
                ch4_lagoon = 0.
            
            # extract storage C variables
            ch4_storage = (
                ch4_compost + 
                ch4_lagoon)

            # exported manure N  (NY only)
            if site == 1:
                if scen == 17:
                    n_export_compost = (
                        data['compost']['N_kg']['Exported_to_market'] *
                        (0.12/0.4704)) # 12% to export, ~47% to bedding
                else:
                    n_export_compost = (
                        data['compost']['N_kg']['Exported_to_market'] *
                        (0.12/0.4712)) # 12% to export, ~47% to bedding

                n_export_field = (  # field 6 is the export field...
                    data['fields'][6]['N_kg']['Influx_from_lagoon'] + 
                    data['fields'][6]['N_kg']['Influx_from_compost'] + 
                    data['fields'][6]['N_kg']['Influx_from_digester'] + 
                    data['fields'][6]['N_kg']['Influx_from_barn'])

                n_export = n_export_compost + n_export_field
            
            else:
                n_export = 0.
                
            p_export = np.nan # more potentially bad P data...
            
            ### FIELD APPLICATIONS ###
            ### "Manure" SECTION   ###
            
            # get total area
            area_total = (
                data['area'][1] + 
                data['area'][2] + 
                data['area'][3] + 
                data['area'][4])

            # convert DFs to Panel and sum
            p_fields = pd.Panel({
                1: data['fields'][1],
                2: data['fields'][2],
                3: data['fields'][3],
                4: data['fields'][4]})
            df_fields = p_fields.sum(axis=0)
            
            # extract applications
            applied_manure_n = (
                df_fields['N_kg']['Influx_from_lagoon'] + 
                df_fields['N_kg']['Influx_from_compost'] + 
                df_fields['N_kg']['Influx_from_digester'] + 
                df_fields['N_kg']['Influx_from_barn'])
            applied_fert_n = df_fields['N_kg']['Influx_from_fertilizer']
            applied_manure_p = (
                df_fields['P_kg']['Influx_from_lagoon'] + 
                df_fields['P_kg']['Influx_from_compost'] + 
                df_fields['P_kg']['Influx_from_digester'] + 
                df_fields['P_kg']['Influx_from_barn'])
            applied_fert_p = df_fields['P_kg']['Influx_from_fertilizer']
            
            ### COMBINED RESULTS      ###
            ### "Environment" SECTION ###

            nh3_total = (
                nh3_barn + 
                nh3_storage + 
                df_fields['N_kg']['Soil_NH3_emission'])
            denitrif = (
                n2o_barn +
                n2_barn + 
                no_barn + 
                n2o_storage + 
                n2_storage + 
                no_storage + 
                df_fields['N_kg']['Soil_N2O_emission'] + 
                df_fields['N_kg']['Soil_N2_emission'] + 
                df_fields['N_kg']['Soil_NO_emission'])
            n_leaching = df_fields['N_kg']['Soil_leaching_loss']
            n_runoff = (
                data['compost']['N_kg']['Compost_runoff_loss'] + 
                df_fields['N_kg']['Soil_runoff_loss'])
            n_erosion = np.nan
            
            # P values appear to be bad at field-level...
            p_leaching = np.nan
            # p_leaching = df_fields['P_kg']['Soil_leaching_loss']
            # p_runoff = (
                # data['compost']['P_kg']['Compost_runoff_loss'] + 
                # df_fields['P_kg']['Soil_runoff_loss'])
            p_runoff = np.nan   
            p_erosion = np.nan  
            # P values appear to be bad at field-level...
            
            ch4_total = (
                df_barn['C_kg']['Floor_CH4_emission'] + 
                ch4_storage + 
                df_fields['C_kg']['Soil_CH4_emission'])

            n2o_total = (
                df_barn['N_kg']['Enteric_N2O_emission'] + 
                df_barn['N_kg']['Floor_N2O_emission'] + 
                n2o_storage + 
                df_fields['N_kg']['Soil_N2O_emission'])
            
            # it turns out that this is anthropogenic CO2
            # (obviously not tracked in DNDC...)            
            # co2_total = (
                # df_barn['C_kg']['Animal_CO2_respiration'] + 
                # df_barn['C_kg']['Floor_CO2_emission'] + 
                # data['compost']['C_kg']['Compost_CO2_emission'] + 
                # co2_digester + 
                # co2_lagoon + 
                # df_fields['C_kg']['Soil_CO2_emission'])
            co2_total = np.nan
            
            dsoc_total = df_fields['C_kg']['Soil_sequestration']

            ### CREATE FINAL DICTIONARY FOR THIS YEAR ###
            
            # set blank value
            blank = np.nan
            
            # create OrderedDict object for DataFrame 
            od = OD()
            
            # add data
            od['blank.1']           = blank
            od['blank.2']           = blank
            od['yield_alfalfa']     = np.nan  # fill in later...
            od['yield_corn_silage'] = np.nan  # fill in later...
            od['yield_grain']       = np.nan  # fill in later...
            od['milk.1']            = milk
            od['blank.3']           = blank
            od['excreted_n.1']      = excreted_n
            od['excreted_p.1']      = excreted_p
            od['applied_manure_n']  = applied_manure_n
            od['applied_fert_n']    = applied_fert_n
            od['applied_manure_p']  = applied_manure_p
            od['applied_fert_p']    = applied_fert_p
            od['blank.4']           = blank
            od['nh3_total']         = nh3_total
            od['denitrif']          = denitrif
            od['n_leaching']        = n_leaching
            od['n_runoff']          = n_runoff
            od['n_erosion']         = n_erosion
            od['p_leaching']        = p_leaching
            od['p_runoff']          = p_runoff
            od['p_erosion']         = p_erosion
            od['ch4_total']         = ch4_total
            od['n2o_total']         = n2o_total
            od['co2_total']         = co2_total
            od['dsoc_total']        = dsoc_total
            od['biogas']            = biogas
            od['blank.5']           = blank
            od['blank.6']           = blank
            od['blank.7']           = blank
            od['milk.2']            = milk
            od['meat']              = meat
            od['excreted_n.2']      = excreted_n
            od['excreted_p.2']      = excreted_p
            od['n2o_enteric']       = n2o_enteric
            od['ch4_enteric']       = ch4_enteric
            od['nh3_barn']          = nh3_barn
            od['n2o_barn']          = n2o_barn
            od['n2_barn']           = n2_barn
            od['no_barn']           = no_barn
            od['n_from_barn']       = n_from_barn
            od['p_from_barn']       = p_from_barn
            od['blank.8']           = blank
            od['n_export']          = n_export
            od['p_export']          = p_export
            od['nh3_storage']       = nh3_storage
            od['n2o_storage']       = n2o_storage
            od['n2_storage']        = n2_storage
            od['no_storage']        = no_storage
            od['ch4_storage']       = ch4_storage
            od['blank.9']           = blank
            
            ### GET DATA BY CROP          ###
            ### "Field processes" SECTION ###
            # for field in sorted(data['fields'].keys()):
            for field in range(1, db['scen']['fields'][scen]+1):

                od['crop' + str(field)]                  = np.nan  # fill in later...
                od['crop' + str(field) + '_ha']          = data['area'][field]    
                od['crop' + str(field) + '_yield_total'] = np.nan  # fill in later...
                od['crop' + str(field) + '_n_fix_total'] = data['fields'][field]['N_kg']['Crop_N_fixation']
                od['crop' + str(field) + '_n2o_total']   = data['fields'][field]['N_kg']['Soil_N2O_emission']
                od['crop' + str(field) + '_no_total']    = data['fields'][field]['N_kg']['Soil_NO_emission']
                od['crop' + str(field) + '_n2_total']    = data['fields'][field]['N_kg']['Soil_N2_emission']
                od['crop' + str(field) + '_nh3_total']   = data['fields'][field]['N_kg']['Soil_NH3_emission']
                od['crop' + str(field) + '_n_leaching']  = data['fields'][field]['N_kg']['Soil_leaching_loss']
                od['crop' + str(field) + '_n_runoff']    = data['fields'][field]['N_kg']['Soil_runoff_loss']
                od['crop' + str(field) + '_n_erosion']   = np.nan  # not tracked by DNDC
                
                # od['crop' + str(field) + '_p_leaching']  = data['fields'][field]['P_kg']['Soil_leaching_loss']
                od['crop' + str(field) + '_p_leaching']  = np.nan  # output can't be used (DNDC bug)
                
                # od['crop' + str(field) + '_p_runoff']    = data['fields'][field]['P_kg']['Soil_runoff_loss']
                od['crop' + str(field) + '_p_runoff']    = np.nan  # output can't be used (DNDC bug)
                
                od['crop' + str(field) + '_p_erosion']   = np.nan  # not tracked by DNDC
                
                # od['crop' + str(field) + '_sop']         = data['fields'][field]['P_kg']['Soil_sequestration']
                od['crop' + str(field) + '_sop']         = np.nan  # output can't be used (DNDC bug)
                
                od['crop' + str(field) + '_ch4_total']   = data['fields'][field]['C_kg']['Soil_CH4_emission']
                od['crop' + str(field) + '_dsoc']        = data['fields'][field]['C_kg']['Soil_sequestration']
            
            ### CREATE DATAFRAME FOR THIS YEAR ##
            
            # add identifying information
            od['site'] = site
            od['clim'] = clim
            od['scen'] = scen
            od['year_sim'] = year_sim
            od['year_cal'] = year_cal
            
            # create DataFrame
            for key, value in od.iteritems():
                od[key] = [value]
            df = pd.DataFrame.from_dict(od)
            
            farm_dfs.append(df)

        ### YIELD ###
        yield_dfs = []
        fields = db['scen']['fields'][scen]
        for field_number in range(1,fields+1):

            # get all multi_year_summary files as DFs
            zpath_file = case_dir + '/CropSystem/Field_' + str(field_number) + '/Multi_year_summary.csv'

            file = zfile.open(zpath_file)
            df = pd.read_csv(
                file,
                skiprows=[0,1,3,4],
                sep=',')
            file.close()

            df.columns = [c.strip() for c in list(df.columns)]
            # df.set_index('Year', inplace=True)

            # pare to summary years
            df = df.loc[df['Year'] > years_init]

            # convert bad output to NaN
            # for c in df.columns:
                # df[c] = pd.to_numeric(df[c], errors='coerce')
            
            # get field type
            field_type = db['field']['field_type'][site, scen, field_number, 1]
            
            # get number of crops
            crops = db['field']['crops'][site, scen, field_number, 1]
            
            # get area
            area = data['area'][field_number]

            # extract yield
            if field_type == 'alfalfa' or field_type == 'grass':
                df['yield_' + field_type] = area * (df['Cut_CropC'] / 0.4) / 1000.  # convert to DM and tonne
                
            elif field_type == 'corn_silage':
                
                if crops == 1:
                    
                    # straight corn silage yield
                    df['yield_' + field_type] = area * ((
                        df['GrainC1'] +
                        df['LeafC1'] * 0.9 + 
                        df['StemC1'] * 0.9) / 0.4) / 1000.  # convert to DM and tonne
                
                if crops == 2:
                
                    # corn silage yield when following winter crop (rye)
                    df['yield_' + field_type] = area * ((
                        df['GrainC2'] +
                        df['LeafC2'] * 0.9 + 
                        df['StemC2'] * 0.9) / 0.4) / 1000.  # convert to DM and tonne
                
                '''  THIS SUMS SILAGE CORN AND RYE
                     DON'T DO THIS FOR NOW...
                if crops == 2 and 'Cover' not in scen_name:
                    
                    # rye yield preceding silage corn
                    df['yield_' + field_type] = (
                        df['yield_' + field_type] +
                        area * ((
                            df['GrainC1'] +
                            df['LeafC1'] * 0.9 + 
                            df['StemC1'] * 0.9) / 0.4) / 1000.)  # convert to DM and tonne
                '''
                
            else:
                df['yield_' + field_type] = area * (df['GrainC1'] / 0.4) / 1000.  # convert to DM and tonne
            
            # set field yield
            df['crop' + str(field_number) + '_yield_total'] = df['yield_' + field_type]
            
            # set ID info
            df['site'] = site
            df['clim'] = clim
            df['scen'] = scen
            df['year_sim'] = df['Year']
            
            # pare DF
            df = df[[
                'site',
                'clim',
                'scen',
                'year_sim',
                'yield_' + field_type,
                'crop' + str(field_number) + '_yield_total']]

            # append to list
            yield_dfs.append(df)
        
        # combine yield DFs
        join_fields = ['site','clim','scen','year_sim']
        df_yield = yield_dfs[0]
        for df in yield_dfs[1:]:
            df_yield = df_yield.join(
                df.set_index(join_fields),
                on=join_fields)
    
        # set grain yield
        df_yield['yield_grain'] = df_yield['yield_corn_grain']
        try:
            df_yield['yield_grain'] = df_yield['yield_grain'] + df_yield['yield_oats']
        except KeyError:
            pass

        field_dfs.append(df_yield)
        
    # concatenate farm results
    df = pd.concat(farm_dfs)
    
    # combine field results
    df_yield = pd.concat(field_dfs)

    # replace yield values in main DF with yield DF
    index_fields = ['site','clim','scen','year_sim']
    df.set_index(index_fields, inplace=True)
    df_yield.set_index(index_fields, inplace=True)

    ### WRITE OUT YIELD DATA ###
    path_yield = opj(
        path_results,
        'ScenarioData_yield.csv')
    df_yield.to_csv(
        path_yield,
        index=True,
        index_label=index_fields)
    
    
    for c in df.columns:
        if 'yield' not in c: continue
        df[c] = df_yield[c]

    ### WRITE OUT ALL DATA ###
    df.reset_index(inplace=True)
    path_df = opj(
        path_results,
        'ScenarioData_all.csv')
    df.to_csv(path_df,index=False)
        
    ### CALCULATE MEAN ANNUAL VALUES ###
    
    df = df.groupby(['site', 'clim', 'scen'], as_index=False).mean()
    del df['year_sim']
    del df['year_cal']

    # fill in crop names
    for site, scen, field, mgt_year in db['field'].index:
        if mgt_year > 1: continue
        
        df.loc[
            (df['site'] == site) & 
            (df['scen'] == scen), 'crop' + str(field)] = db['field']['field_type'][site, scen, field, 1]
    
    # add scenario name
    for scen in sorted(list(set(df['scen'].tolist()))):
        df.loc[df['scen'] == scen, 'scen_name'] = db['scen']['name_tables'][scen]
    df.set_index('scen_name', inplace=True)
    del df['scen']
    
    # pare to historical climate dataset only
    df = df.loc[df['clim'] == 1]
    del df['clim']

    ### WRITE OUT TABLE BY SITE ###
    for site in sorted(list(set(df['site'].tolist()))):
        
        df_site = df.loc[df['site'] == site]
        del df_site['site']
        
        site_name = db['site']['site_name'][site]
        
        path_site = opj(
            path_results,
            'ScenarioData_' + site_name + '.csv')
        
        df_site.transpose().to_csv(path_site)

    return

def calibrate_yield():
    
    ### SET PATHS ###
    path_sim = opj(
        PATH.projects,
        TASK,
        'simulation')

    path_results = opj(
        path_sim,
        'results')

    path_zip = opj(
        PATH.scratch,
        TASK,
        'zip')

    ### READ SIMULATION DATABASE ###
    path_db = opj(
        path_sim,
        'database.xlsx')
    db = read_db(path_db)

    # add site ID to crop table
    db['crop'].loc[
        db['crop']['crop_name'].str.contains('ny'), 
            'state_abbrev'] = 'NY'
            
    db['crop'].loc[
        db['crop']['crop_name'].str.contains('wi'), 
            'state_abbrev'] = 'WI'

    ### GLOB ZIP FILES ###
    path_glob = opj(
        path_zip,
        'Case*.zip')
    globber = glob.glob(path_glob)

    ### PROCESS RESULTS ###

    # initialize rows for DNDC yield table
    rows = []

    # loop through result directories
    for path_zip in globber:

        path_dir, filename = os.path.split(path_zip)
        dirname = os.path.basename(path_dir)
        site, clim, scen = [int(i) for i in filename.split('-')[1:4]]
        
        # get state
        state = db['site'].loc[site]['site_name']

        # get scenario name
        scen_name = db['scen']['name_tables'][scen]

        # open zip archive
        zfile = zipfile.ZipFile(path_zip, 'r')

        # get list of top-level directories
        # (assumes there's only 1 directory in zip file)
        case_dir = zfile.namelist()[0].split('/')[0]

        # get years
        year_cal_start = db['clim']['year_cal_start'][clim]
        year_cal_end   = db['clim']['year_cal_end'][clim]
        years_init   = db['clim']['years_init'][clim]
        
        ### YIELD ###
        yield_dfs = []
        fields = db['scen']['fields'][scen]
        for field_number in range(1,fields+1):

            # get all multi_year_summary files as DFs
            zpath_file = case_dir + '/CropSystem/Field_' + str(field_number) + '/Multi_year_summary.csv'

            file = zfile.open(zpath_file)
            df = pd.read_csv(
                file,
                skiprows=[0,1,3,4],
                sep=',')
            file.close()

            df.columns = [c.strip() for c in list(df.columns)]

            # pare to summary years
            df = df.loc[df['Year'] > years_init]

            # get area
            # area = data['area'][field_number]

            # get field type
            field_type = db['field']['field_type'][site, scen, field_number, 1]

            print state, site, clim, scen, field_type
            
            # get number of crops
            crops = db['field']['crops'][site, scen, field_number, 1]
            
            ### LOOP THROUGH YEARS ###
            for i, data in df.iterrows():
                
                year = data['Year']

                ### LOOP THROUGH CROPS ###
                for crop in range(1, crops+1):

                    id_dndc = data['Crop' + str(crop)]
                    grainc = data['GrainC' + str(crop)]
                    leafc = data['LeafC' + str(crop)]
                    stemc = data['StemC' + str(crop)]
                    rootc = data['RootC' + str(crop)]
                    cutc = data['Cut_CropC']

                    # deal with IDs
                    if field_type == 'alfalfa': 
                        if id_dndc == 0:
                            id_dndc = 10
                    if field_type == 'grass':
                        if id_dndc == 0:
                            id_dndc = 5                       

                    # get crop name
                    crop_name = db['crop'].loc[
                        (db['crop']['id_dndc'] == id_dndc) & 
                        (db['crop']['state_abbrev'] == state)]['crop_name'].iloc[0]
                    crop_name = crop_name.replace('_' + state.lower(), '')
                    if 'annual' in crop_name or 'perennial' in crop_name:
                        crop_name = crop_name.split('_')[0]

                    # extract yield
                    yieldc = grainc
                    source = 'grain field'

                    if crop_name == 'alfalfa' or crop_name == 'grass':
                        yieldc = cutc
                        source = 'cut crop C'

                    silage_crops = [
                        'corn_silage',
                        'rye']

                    if crop_name in silage_crops:
                        yieldc = (
                            grainc +
                            leafc * 0.9 + 
                            stemc * 0.9)
                        source = 'silage calc'

                    # append to list
                    rows.append([
                        site,
                        scen,
                        year,
                        crop_name,
                        yieldc])

    ### CREATE DNDC YIELD DATAFRAME ###
    df_dndc = pd.DataFrame(rows)
    df_dndc.columns = [
        'site',
        'scen',
        'year',
        'crop_name',
        'yieldc']
    
    # pivot on crop name
    df_dndc = pd.pivot_table(
        data    = df_dndc,
        values  = 'yieldc',
        index   = ['site','scen'],
        columns = 'crop_name')
    df_dndc.columns = [c + '-dndc' for c in df_dndc.columns]
    
    ### COMPARE TO IFSM YIELD ###
    
    # get IFSM yield file as DF
    df_ifsm = pd.read_csv(
        opj(
            path_results,
            'yield_ifsm.csv'))
    df_ifsm.set_index(['site', 'scen'], inplace=True)
    df_ifsm.columns = [c + '-ifsm' for c in df_ifsm.columns]

    # create comparison table
    df_compare = df_ifsm.join(df_dndc)
    
    # calculate DNDC yield as a % of IFSM
    for ci in df_ifsm.columns:
        c = ci.replace('-ifsm','')
        cd = ci.replace('-ifsm','-dndc')
        df_compare[c + '-%'] = df_compare[cd] / df_compare[ci]
        
    # write out calibration file
    path_cal = opj(
        path_results,
        'yield_ifsm_vs_dndc.csv')
    df_compare.to_csv(path_cal)
    
    return
    
def yield_joyce():
    
    ### SET PATHS ###
    path_sim = opj(
        PATH.projects,
        TASK,
        'simulation')

    path_results = opj(
        path_sim,
        'results')

    path_zip = opj(
        PATH.scratch,
        TASK,
        'zip')

    ### READ SIMULATION DATABASE ###
    path_db = opj(
        path_sim,
        'database.xlsx')
    db = read_db(path_db)

    ### GET CROPS ###
    df_crop = pd.read_excel(
        path_db,
        sheetname='crop',
        skiprows=[0])
    df_crop.set_index('id_db', inplace=True)
    db['crop'] = df_crop
    
    ### GLOB ZIP FILES ###
    path_glob = opj(
        path_zip,
        'Case*.zip')
    globber = glob.glob(path_glob)

    ### PROCESS RESULTS ###
    
    # initialize list of DFs
    yield_dfs = []
    
    # loop through result directories
    for path_zip in globber:

        path_dir, filename = os.path.split(path_zip)
        dirname = os.path.basename(path_dir)

        case, site, clim, scen = filename.split('.')[0].split('-')[:4]
        site_clim_scen = site + '-' + clim + '-' + scen
        site, clim, scen = int(site), int(clim), int(scen)

        # get scenario name
        scen_name = db['scen']['name_tables'][scen]

        # open zip archive
        zfile = zipfile.ZipFile(path_zip, 'r')

        # get list of top-level directories
        # (assumes there's only 1 directory in zip file)
        case_dir = zfile.namelist()[0].split('/')[0]

        ### LOOP THROUGH ALL YEARS (INCLUDE INITIALIZATION YEARS) ###
        year_cal_start = db['clim']['year_cal_start'][clim]
        year_cal_end   = db['clim']['year_cal_end'][clim]
        years_init   = db['clim']['years_init'][clim]

        # initialize list of years
        years = []

        # get summary years
        for year_cal in range(year_cal_start, year_cal_end+1):
            year_sim = year_cal - year_cal_start + years_init + 1
            years.append((year_sim, year_cal))

        ### YIELD ###
        fields = db['scen']['fields'][scen]
        for field_number in range(1,fields+1):

            field_dir = 'Field_' + str(field_number)

            print case, site_clim_scen, field_dir

            # get all multi_year_summary files as DFs
            zpath_file = (
                case_dir + 
                '/CropSystem/' + 
                field_dir + 
                '/Multi_year_summary.csv')

            file = zfile.open(zpath_file)
            df = pd.read_csv(
                file,
                skiprows=[0,1,3,4],
                sep=',')
            file.close()

            df.columns = [c.strip() for c in list(df.columns)]

            # pare to summary years
            df = df.loc[df['Year'] > years_init]
            
            # get field type
            field_type = db['field']['field_type'][site, scen, field_number, 1]

            # get area
            area = db['field']['area'][site, scen, field_number, 1]

            # get number of crops
            crops = db['field']['crops'][site, scen, field_number, 1]

            # add index info
            df['case'] = site
            df['site_clim_scen'] = site_clim_scen
            df['field'] = field_dir
            
            ### FIRST CROP ###
            
            id_db = db['field']['crop_1_id_db'][site, scen, field_number, 1]
            residue_frac = db['field']['crop_1_residue'][site, scen, field_number, 1]

            id_dndc = db['crop']['id_dndc'][id_db]
            
            frac_grain = db['crop']['frac_grain'][id_db]
            frac_leaf  = db['crop']['frac_leaf'][id_db]
            frac_stem  = db['crop']['frac_stem'][id_db]
            
            agbm_frac = frac_grain + frac_leaf + frac_stem
            
            cn_grain   = db['crop']['cn_grain'][id_db]
            cn_leaf    = db['crop']['cn_leaf'][id_db]
            cn_stem    = db['crop']['cn_stem'][id_db]

            # extract yield
            if field_type in ['corn_grain', 'oat']:

                yieldc = df['GrainC1'].mean() * area
                yieldn = yieldc / cn_grain

                residuec_leaf = df['LeafC1'].mean() * residue_frac
                residuec_stem = df['StemC1'].mean() * residue_frac
                residuec = residuec_leaf + residuec_stem

                residuen_leaf = residuec_leaf / cn_leaf
                residuen_stem = residuec_stem / cn_stem
                residuen = residuen_leaf + residuen_stem

            if field_type == 'alfalfa' or field_type == 'grass':

                yieldc = df['Cut_CropC'].mean() * area
                yieldn = (
                    (yieldc * (frac_grain / agbm_frac)) / cn_grain + 
                    (yieldc * (frac_leaf  / agbm_frac)) / cn_leaf + 
                    (yieldc * (frac_stem  / agbm_frac)) / cn_stem)
                    
                residuec = df['ShootLitterC_input'].mean() * residue_frac
                residuen = (
                    (residuec * (frac_grain / agbm_frac)) / cn_grain + 
                    (residuec * (frac_leaf  / agbm_frac)) / cn_leaf + 
                    (residuec * (frac_stem  / agbm_frac)) / cn_stem)
                
            if field_type == 'corn_silage':

                if crops == 1:

                    yieldc_grain = area * df['GrainC1'].mean()
                    yieldc_leaf = area * df['LeafC1'].mean() * (1. - residue_frac)
                    yieldc_stem = area * df['StemC1'].mean() * (1. - residue_frac)

                    residuec_leaf = df['LeafC1'].mean() * residue_frac
                    residuec_stem = df['StemC1'].mean() * residue_frac

                if crops == 2:

                    yieldc_grain = area * df['GrainC2'].mean()
                    yieldc_leaf = area * df['LeafC2'].mean() * (1. - residue_frac)
                    yieldc_stem = area * df['StemC2'].mean() * (1. - residue_frac)

                    residuec_leaf = df['LeafC2'].mean() * residue_frac
                    residuec_stem = df['StemC2'].mean() * residue_frac

                yieldc = yieldc_grain + yieldc_leaf + yieldc_stem

                yieldn_grain = yieldc_grain / cn_grain
                yieldn_leaf  = yieldc_leaf  / cn_leaf
                yieldn_stem  = yieldc_stem  / cn_stem
                
                yieldn = yieldn_grain + yieldn_leaf + yieldn_stem
                    
                residuec = residuec_leaf + residuec_stem

                residuen_leaf = residuec_leaf / cn_leaf
                residuen_stem = residuec_stem / cn_stem

                residuen = residuen_leaf + residuen_stem

            # initialize data variable
            data = OD()
                
            # populate data
            data['case'] = case
            data['site_clim_scen'] = " " + site_clim_scen
            data['field'] = field_dir
            
            data['Crop_ID.1'] = id_dndc
            data['kg C.1']    = yieldc
            data['kg N.1']    = yieldn
            data['kg H2O.1']  = np.nan
            data['kg P.1']    = np.nan

            data['Crop_ID.1.res'] = id_dndc
            data['kg C.1.res']    = residuec
            data['kg N.1.res']    = residuen
            data['kg H2O.1.res']  = np.nan
            data['kg P.1.res']    = np.nan
            
            data['Crop_ID.2'] = np.nan
            data['kg C.2']    = np.nan
            data['kg N.2']    = np.nan
            data['kg H2O.2']  = np.nan
            data['kg P.2']    = np.nan

            data['Crop_ID.2.res'] = np.nan
            data['kg C.2.res']    = np.nan
            data['kg N.2.res']    = np.nan
            data['kg H2O.2.res']  = np.nan
            data['kg P.2.res']    = np.nan
            
            ### SECOND CROP ###
            if crops == 2:
                    
                id_db = db['field']['crop_2_id_db'][site, scen, field_number, 1]
                residue_frac = db['field']['crop_2_residue'][site, scen, field_number, 1]

                id_dndc = db['crop']['id_dndc'][id_db]
                
                frac_grain = db['crop']['frac_grain'][id_db]
                frac_leaf  = db['crop']['frac_leaf'][id_db]
                frac_stem  = db['crop']['frac_stem'][id_db]
                
                agbm_frac = frac_grain + frac_leaf + frac_stem
                
                cn_grain   = db['crop']['cn_grain'][id_db]
                cn_leaf    = db['crop']['cn_leaf'][id_db]
                cn_stem    = db['crop']['cn_stem'][id_db]
                
                # yield
                yieldc_grain = area * df['GrainC1'].mean()
                yieldc_leaf = area * df['LeafC1'].mean() * (1. - residue_frac)
                yieldc_stem = area * df['StemC1'].mean() * (1. - residue_frac)

                yieldc = yieldc_grain + yieldc_leaf + yieldc_stem

                yieldn_grain = yieldc_grain / cn_grain
                yieldn_leaf  = yieldc_leaf  / cn_leaf
                yieldn_stem  = yieldc_stem  / cn_stem
                
                yieldn = yieldn_grain + yieldn_leaf + yieldn_stem
                    
                residuec_leaf = df['LeafC1'].mean() * residue_frac
                residuec_stem = df['StemC1'].mean() * residue_frac

                residuec = residuec_leaf + residuec_stem

                residuen_leaf = residuec_leaf / cn_leaf
                residuen_stem = residuec_stem / cn_stem

                residuen = residuen_leaf + residuen_stem
                
                data['Crop_ID.2'] = id_dndc
                data['kg C.2']    = yieldc
                data['kg N.2']    = yieldn
                data['kg H2O.2']  = np.nan
                data['kg P.2']    = np.nan

                data['Crop_ID.2.res'] = id_dndc
                data['kg C.2.res']    = residuec
                data['kg N.2.res']    = residuen
                data['kg H2O.2.res']  = np.nan
                data['kg P.2.res']    = np.nan
            
            for thing in data: data[thing] = [data[thing]]
            
            df = pd.DataFrame(data)
            yield_dfs.append(df)

    df = pd.concat(yield_dfs)
    
    path_df = opj(
        path_results,
        'yield_for_joyce.csv')
    df.to_csv(path_df, index=False)

    return

def barn2field_joyce():
    
    ### SET PATHS ###
    path_sim = opj(
        PATH.projects,
        TASK,
        'simulation')

    path_results = opj(
        path_sim,
        'results')

    path_zip = opj(
        PATH.scratch,
        TASK,
        'zip')

    ### READ SIMULATION DATABASE ###
    path_db = opj(
        path_sim,
        'database.xlsx')
    db = read_db(path_db)

    ### GLOB ZIP FILES ###
    path_glob = opj(
        path_zip,
        'Case*.zip')
    globber = glob.glob(path_glob)

    ### PROCESS RESULTS ###
    
    # initialize list of DFs
    dfs = []
    
    # loop through result directories
    for path_zip in globber:

        path_dir, filename = os.path.split(path_zip)
        dirname = os.path.basename(path_dir)
        
        case, site, clim, scen = filename.split('.')[0].split('-')[:4]
        site_clim_scen = site + '-' + clim + '-' + scen
        site, clim, scen = int(site), int(clim), int(scen)

        # get scenario name
        scen_name = db['scen']['name_tables'][scen]

        # open zip archive
        zfile = zipfile.ZipFile(path_zip, 'r')

        # get list of top-level directories
        # (assumes there's only 1 directory in zip file)
        case_dir = zfile.namelist()[0].split('/')[0]

        ### LOOP THROUGH ALL YEARS (INCLUDE INITIALIZATION YEARS) ###
        year_cal_start = db['clim']['year_cal_start'][clim]
        year_cal_end   = db['clim']['year_cal_end'][clim]
        years_init   = db['clim']['years_init'][clim]

        # initialize list of years
        years = []

        # get summary years
        for year_cal in range(year_cal_start, year_cal_end+1):
            year_sim = year_cal - year_cal_start + years_init + 1
            years.append((year_sim, year_cal))

        ### REMOVALS ###

        for lot in [1,2]:

            print case, site_clim_scen, lot
            
            for year_sim, year_cal in years:

                # get all multi_year_summary files as DFs
                zpath_file = (
                    case_dir + 
                    '/LivestockSystem/' + 
                    'Day_FeedlotManureRemove_yr' + str(year_sim) + '_lot' + str(lot) + '.csv')

                file = zfile.open(zpath_file)
                df = pd.read_csv(
                    file,
                    skiprows=[0,1,2],
                    sep=',')
                file.close()

                df.columns = [c.strip() for c in list(df.columns)]

                # pare to removal days
                df = df.loc[df['Day'].isin([135,270])]
                
                # add index info
                df['case_num'] = int(case.replace('Case',''))
                df['case'] = case
                df['site_clim_scen'] = " " + site_clim_scen
                df['feedlot'] = lot
                df['field'] = 1
                df['year'] = year_sim
                df['doy'] = df['Day']
                
                df = df[[
                    'case_num',
                    'case',
                    'site_clim_scen',
                    'feedlot',
                    'field',
                    'year',
                    'doy',
                    'to_field-C',
                    'to_field-N',
                    'to_field-H2O',
                    'to_field-P']]

                for field in [1,2,3,4]:
                    
                    df_field = df.copy(deep=True)
                    
                    df_field['field'] = field
                    
                    fracs = {
                        1: {
                            1: 0.05,
                            2: 0.15,
                            3: 0.39,
                            4: 0.41},
                        2: {
                            1: 0.00,
                            2: 0.35,
                            3: 0.35,
                            4: 0.30}}
                    
                    for c in [
                        'to_field-C',
                        'to_field-N',
                        'to_field-H2O',
                        'to_field-P']:
                        
                        frac = fracs[site][field]
                    
                        df_field[c] = df_field[c] * frac
                    
                    # sum by year
                    df_sum = df_field.groupby([
                                    'case_num',
                                    'case',
                                    'site_clim_scen',
                                    'feedlot',
                                    'field',
                                    'year'],
                                as_index=False).sum()
                    del df_sum['doy']

                    dfs.append(df_sum)

    df = pd.concat(dfs)

    df = df.groupby([
                'case_num',
                'case',
                'site_clim_scen',
                'feedlot',
                'field'], 
            as_index=False).mean()
    del df['year']

    df.sort(columns=[
                'case_num',
                'feedlot',
                'field'],
            inplace=True)
    del df['case_num']

    path_df = opj(
        path_results,
        'barn2field_for_joyce.csv')
    df.to_csv(path_df, index=False)

    return

def extract_all():

    ### SET PATHS ###
    path_sim = opj(
        PATH.projects,
        TASK,
        'simulation')

    path_results = opj(
        path_sim,
        'results')

    path_zip = opj(
        PATH.scratch,
        TASK,
        'zip')
        # 'zip_test')

    ### READ SIMULATION DATABASE ###
    path_db = opj(
        path_sim,
        'database.xlsx')
    db = read_db(path_db)

    ### GLOB ZIP FILES ###
    path_glob = opj(
        path_zip,
        'Case*.zip')
    globber = glob.glob(path_glob)

    ### PROCESS RESULTS ###

    # initialize lists of DFs
    farm_dfs = []
    field_dfs = []

    # loop through result directories
    for path_zip in globber:

        path_dir, filename = os.path.split(path_zip)
        dirname = os.path.basename(path_dir)
        site, clim, scen = [int(i) for i in filename.split('-')[1:4]]
        
        # get site name
        site_name = db['site']['site_name'][site]
        
        # get climate dataset name
        clim_name = db['clim']['model'][clim]
        
        # get scenario name
        scen_name = db['scen']['name_tables'][scen]

        print site, clim, scen

        # open zip archive
        zfile = zipfile.ZipFile(path_zip, 'r')

        # get list of top-level directories
        # (assumes there's only 1 directory in zip file)
        case_dir = zfile.namelist()[0].split('/')[0]

        ### LOOP THROUGH ALL YEARS (INCLUDE INITIALIZATION YEARS) ###
        year_cal_start = db['clim']['year_cal_start'][clim]
        year_cal_end   = db['clim']['year_cal_end'][clim]
        years_init   = db['clim']['years_init'][clim]

        # initialize list of years
        years = []

        # get summary years
        for year_cal in range(year_cal_start, year_cal_end+1):
            year_sim = year_cal - year_cal_start + years_init + 1
            years.append((year_sim, year_cal))

        # loop through summary years
        for year_sim, year_cal in years:

            # get annual livestock report
            zpath_file = case_dir + '/LivestockSystem/AnnualReport_LivestockSystem_yr' + str(year_sim) + '.txt'

            # loop through lines
            file = zfile.open(zpath_file)
            lines = file.readlines()
            file.close()

            # extract
            data = get_livestock(lines)
            
            print data.keys()
            sys.exit()
    
    return
    
def chart_data():
    
    ### SET PATHS ###
    path_sim = opj(
        PATH.projects,
        TASK,
        'simulation')

    path_results = opj(
        path_sim,
        'results')
    
    ### READ IN RESULTS ###
    dfs = []
    for state in ['NY', 'WI']:
        
        path_file = opj(
            path_results,
            'ScenarioData_' + state + '.csv')
        
        df = pd.read_csv(path_file)
        df.columns = ['scenario'] + df.columns.tolist()[1:]
        df.set_index('scenario', inplace=True)
        df = df.T
        
        for c in df.columns:
            if 'blank.' in c:
                del df[c]
        
        df['state'] = state
        
        # print df['ch4_total'].astype(float)
        # sys.exit()

        # calculate total milk production
        if state == 'NY': cows = 1500.
        if state == 'WI': cows = 150.
        
        df['milk'] = df['milk.1'].astype(float) * cows
        
        # calculate GWP
        df['ch4_gwp'] = convert.to_gwp(df['ch4_total'].astype(float), 'ch4')
        df['n2o_gwp'] = convert.to_gwp(df['n2o_total'].astype(float), 'n2o')
        df['gwp']     = df['ch4_gwp'] + df['n2o_gwp']
        df['gwpi']    = df['gwp'] / df['milk']
        
        dfs.append(df)
    
    df = pd.concat(dfs)
    df.reset_index(inplace=True)
    df.set_index(['state', 'index'], inplace=True)
    
    path_file = opj(
        path_results,
        'chart_data.csv')
    df.to_csv(
        path_file,
        index_labels=['state', 'scenario'])
        
    
    return
    
if __name__ == '__main__':

    ### GET POST-PROCESSING STEP ###
    parser()
    if SOFTWARE == 'ManureDNDC':

        
        # applications()
        # feed_efficiency()
        # extract_all()
        
        ### CRITICAL FOR RESULTS ###
        summarize()
        # calibrate_yield()
        # yield_joyce()
        # barn2field_joyce()
        ### CRITICAL FOR RESULTS ###
        
        ### FOR REVIEWING RESULTS ###
        # chart_data()
        ### FOR REVIEWING RESULTS ###

    if SOFTWARE == 'DNDC':
        pass
