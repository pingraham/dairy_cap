
import os, sys
from os.path import join as opj
import glob, copy
import pandas as pd

PATH = 'I:\\projects\\dairy_cap\\wicst_n2o'
RUN = 'run1'
PATH_RUN = os.path.join(PATH, RUN)
PROJECT = 'wicst_n2o'
DATABASE = 'database.xlsx'

def main():
    
    # get FieldManage files
    path_glob = opj(
        PATH_RUN,
        'results',
        'Batch',
        'Case*-cs4*',
        'Day_FieldManage_*.csv')
    globber = glob.glob(path_glob)
    
    dfs = []
    
    for path_file in globber:
        
        path_dir, filename = os.path.split(path_file)
        path_dir, dirname = os.path.split(path_dir)
        
        case, site, soil, system, part = dirname.split('-')
        site, soil, part = int(site), int(soil), int(part)
        
        year = int(filename.split('.')[0].split('_')[-1]) + 1992
        
        print site, system, year
        
        df = pd.read_csv(
            path_file,
            skiprows=[0,1,3])
        
        df['system'] = system
        df['site'] = site
        df['year'] = year
        
        df['date'] = pd.to_datetime(
            df['year'] * 1000 +
            df['Day'],
            format='%Y%j')
        
        df = df.loc[df['Plant_cut (kg C/ha)'] != 0.]
        
        dfs.append(df)
    
    df = pd.concat(dfs)
    
    # make it pretty
    df = df[[
        'site', 
        'system',
        'year',
        'Day',
        'date',
        'Plant_cut (kg C/ha)']]
    
    # loop through and number days
    last_year = None
    cut = 0
    for rec, data in df.iterrows():
        
        year = data['year']
        
        if last_year != year:
            cut = 1
        else:
            cut+=1
        
        df.loc[rec, 'cut'] = cut
        
        last_year = copy.deepcopy(year)
    
    df.sort_values(by=['site','system', 'date'], inplace=True)
    
    path_df = opj(
        PATH_RUN,
        'results',
        'yield',
        'alfalfa_cuts.csv')
    
    df.to_csv(path_df, index=False)

    return

if __name__ == '__main__':
    main()
