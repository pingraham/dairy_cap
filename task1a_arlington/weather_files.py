
import os, sys
from os.path import join as opj
import pandas as pd

PATH = 'I:\\projects\\dairy_cap\\wicst_n2o'
RUN=1
SITE=1

def main():
    
    # read in weather spreadsheet as DF
    path_xlsx = opj(
        PATH,
        'management',
        'WICST_Weather.xlsx')
    df_weather = pd.read_excel(
        path_xlsx,
        sheetname='WICST flagged weather')

    # create year and day-of-year column
    # df_weather['Date'] = pd.to_datetime(df_weather['Date'])
    df_weather['year'] = df_weather['Date'].dt.year
    df_weather['doy'] = df_weather['Date'].dt.dayofyear
    
    # convert precip
    df_weather['PRCP'] = df_weather['PRCP'] / 10.
    
    # get a list of years
    years = list(set(df_weather['year'].values))
    years.sort()
    
    for year in years:
        print year
        
        # cut out year
        df = df_weather.loc[df_weather['year'] == year].copy(deep=True)
        
        # pare down columns
        df = df[[
            'doy',
            'TMAX',
            'TMIN',
            'PRCP',
            'WSPD',
            'SRAD',
            'RH']]
            
        path_dir = opj(
            PATH,
            'run' + str(RUN),
            'weather',
            str(year))
        if not os.path.exists(path_dir):
            os.mkdir(path_dir)
        path_file = opj(
            path_dir,
            str(SITE) + '.txt')
        
        df.to_csv(
            path_file,
            sep='\t',
            index=False,
            header=[str(year)] + ['']*6)
    
    return

if __name__ == '__main__':
    main()
