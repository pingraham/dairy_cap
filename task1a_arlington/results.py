import os, sys
from os.path import join as opj
import glob
import math
import datetime as dt
import pandas as pd
import numpy as np
import ags_network

# global variables for network paths
PROJECT = 'dairy_cap'
PATH    = ags_network.paths(PROJECT)

# database name
DATABASE = 'database.xlsx'

# task name
TASK = 'task1a_arlington'

def extract():

    ### SET PATHS ###
    path_db = opj(
        PATH.projects,
        TASK,
        'calibration',
        DATABASE)
        
    path_results = opj(
        PATH.projects,
        TASK,
        'calibration',
        'results')

    path_batch = opj(
        path_results,
        'Batch')

    ### READ IN DATABASE ###
    data_dict = {}
    sheets = ['field']
    for sheet in sheets:
        data_dict[sheet] = pd.read_excel(
            path_db, 
            sheetname=sheet, 
            skiprows=[0])
    df_field = data_dict['field']
    
    ### READ-IN RESULTS ###
    path_glob = opj(
        path_batch,
        'Case*')
    globber = glob.glob(path_glob)

    results = []
    for path_case in globber:
        
        # get the directory name
        dirpath,dirname = os.path.split(path_case)

        case, site, soil, system, part = dirname.split('-')
        site, soil, part = int(site), int(soil), int(part)

        print '    ', dirname
        
        ### EXTRACT C VARIABLES (DSOC, CH4) ###
        c_globber = glob.glob(os.path.join(path_case, 'Day_SoilC_*.csv'))
        
        dfs = []
        for path_file in c_globber:

            filename = os.path.basename(path_file)
            year_sim = int(filename.replace('.csv','').split('_')[-1])
            
            # lookup field data
            df_sys = df_field.loc[
                (df_field['system'] == system) & 
                (df_field['partition'] == part)].copy(deep=True)
            df_sys.index = range(1,len(df_sys)+1)
            
            year_cal   = df_sys['year'][year_sim]
            phase_year = df_sys['phase_year'][year_sim]
            phase_crop = df_sys['phase_crop'][year_sim]
            part_name  = df_sys['partition_name'][year_sim]
            fraction   = df_sys['fraction'][year_sim]

            df_c = pd.read_csv(
                path_file,
                skiprows=[0],
                sep=',')

            # add date
            df_c['year_sim'] = year_sim
            df_c['year_cal'] = year_cal
            df_c['date'] = pd.to_datetime(
                df_c['year_cal'] * 1000 +
                df_c['Day'],
                format='%Y%j')
            
            df_c['site']       = site
            df_c['system']     = system
            df_c['phase_year'] = phase_year
            df_c['phase_crop'] = phase_crop
            df_c['partition']  = part
            df_c['part_name']  = part_name
            df_c['part_fraction']   = fraction

            # extract variables
            df_c = df_c[[
                'site',
                'system',
                'phase_year',
                'phase_crop',
                'partition',
                'part_name',
                'part_fraction',
                'year_sim',
                'year_cal',
                'date',
                'SOC',
                # 'Eco-respiration',
                'Soil-heterotrophic-respiration',
                'CH4-flux']]
            
            dfs.append(df_c)
        df_c = pd.concat(dfs)

        ### EXTRACT N VARIABLES (N2O, NH3, AND N LEACHING) ###
        n_globber = glob.glob(os.path.join(path_case, 'Day_SoilN_*.csv'))
        
        dfs = []
        for path_file in n_globber:

            filename = os.path.basename(path_file)
            year_sim = int(filename.replace('.csv','').split('_')[-1])
            
            # lookup field data
            df_sys = df_field.loc[
                (df_field['system'] == system) & 
                (df_field['partition'] == part)].copy(deep=True)
            df_sys.index = range(1,len(df_sys)+1)
            
            year_cal   = df_sys['year'][year_sim]
            phase_year = df_sys['phase_year'][year_sim]
            phase_crop = df_sys['phase_crop'][year_sim]
            part_name  = df_sys['partition_name'][year_sim]
            fraction   = df_sys['fraction'][year_sim]

            # extract N variables (N2O, NH3, and N leaching)
            df_n = pd.read_csv(
                path_file,
                skiprows=[0, 1, 3, 4],
                sep=',')

            # calculate indirect N2O
            df_n['n2oi'] = (
                ((df_n['NH3-flux'] + df_n['NO-flux']) * 0.01) + 
                (df_n['NO3-leach'] * 0.015))

            # add date
            df_n['year_sim'] = year_sim
            df_n['year_cal'] = year_cal
            df_n['date'] = pd.to_datetime(
                df_n['year_cal'] * 1000 +
                df_n['Day'],
                format='%Y%j')

            # extract variables
            df_n['site']       = site
            df_n['system']     = system
            df_n['phase_year'] = phase_year
            df_n['phase_crop'] = phase_crop
            df_n['partition']  = part
            df_n['part_name']  = part_name
            df_n['part_fraction']   = fraction

            # extract variables
            df_n = df_n[[
                'site',
                'system',
                'phase_year',
                'phase_crop',
                'partition',
                'part_name',
                'part_fraction',
                'year_sim',
                'year_cal',
                'date',
                'N2O-flux',
                'NO-flux',
                'NH3-flux',
                'NO3-leach',
                'n2oi',
                'NH4+',
                'NO3-']]

            dfs.append(df_n)
        df_n = pd.concat(dfs)

        ### EXTRACT SOIL CLIMATE VARIABLES (VSWC and temperature) ###
        clim_globber = glob.glob(os.path.join(path_case, 'Day_SoilClimate_*.csv'))
        
        dfs = []
        for path_file in clim_globber:

            filename = os.path.basename(path_file)
            year_sim = int(filename.replace('.csv','').split('_')[-1])
            
            # lookup field data
            df_sys = df_field.loc[
                (df_field['system'] == system) & 
                (df_field['partition'] == part)].copy(deep=True)
            df_sys.index = range(1,len(df_sys)+1)
            
            year_cal   = df_sys['year'][year_sim]
            phase_year = df_sys['phase_year'][year_sim]
            phase_crop = df_sys['phase_crop'][year_sim]
            part_name  = df_sys['partition_name'][year_sim]
            fraction   = df_sys['fraction'][year_sim]

            # extract soil climate variables (temperature and WFPS)
            df_clim = pd.read_csv(
                path_file,
                skiprows=[0, 1, 3],
                sep=',')

            # extract temperature
            df_clim['temp_1cm'] = df_clim['1cm']
            df_clim['temp_5cm'] = df_clim['5cm']
            df_clim['temp_10cm'] = df_clim['10cm']
            df_clim['temp_0-10cm'] = (
                df_clim['temp_1cm']  +
                df_clim['temp_5cm']  +
                df_clim['temp_10cm']) / 3.

            # extract WFPS
            df_clim['wfps_1cm'] = df_clim['1cm.1']
            df_clim['wfps_5cm'] = df_clim['5cm.1']
            df_clim['wfps_10cm'] = df_clim['10cm.1']
            df_clim['wfps_0-10cm'] = (
                df_clim['wfps_1cm']  +
                df_clim['wfps_5cm']  +
                df_clim['wfps_10cm']) / 3.
            
            # add date
            df_clim['year_sim'] = year_sim
            df_clim['year_cal'] = year_cal
            df_clim['date'] = pd.to_datetime(
                df_clim['year_cal'] * 1000 +
                df_clim['Day'],
                format='%Y%j')

            # extract variables
            df_clim['site']       = site
            df_clim['system']     = system
            df_clim['phase_year'] = phase_year
            df_clim['phase_crop'] = phase_crop
            df_clim['partition']  = part
            df_clim['part_name']  = part_name
            df_clim['part_fraction']   = fraction

            # extract variables
            df_clim = df_clim[[
                'site',
                'system',
                'phase_year',
                'phase_crop',
                'partition',
                'part_name',
                'part_fraction',
                'year_sim',
                'year_cal',
                'date',
                'temp_1cm',
                'temp_5cm',
                'temp_10cm',
                'temp_0-10cm',
                'wfps_1cm',
                'wfps_5cm',
                'wfps_10cm',
                'wfps_0-10cm']]

            dfs.append(df_clim)
        df_clim = pd.concat(dfs)
        
        # join N to C
        index_fields = [
            'site',
            'system',
            'phase_year',
            'phase_crop',
            'partition',
            'part_name',
            'part_fraction',
            'year_sim',
            'year_cal',
            'date']
        df = df_c.join(
            df_n.set_index(index_fields),
            on=index_fields)
        
        # join Climate to DF
        df = df.join(
            df_clim.set_index(index_fields),
            on=index_fields)
        
        results.append(df)
    
    df_daily = pd.concat(results)
    df_daily.sort(['site', 'system', 'partition', 'year_cal'], inplace=True)

    df_daily.reset_index(inplace=True)
    del df_daily['index']

    # join observed data
    path_obs = opj(
        PATH.projects,
        TASK,
        'management',
        'WICST_Observations_dndc_formatted.xlsx')
    df_obs = pd.read_excel(
        path_obs,
        sheetname='dndc_formatted')

    del df_obs['year_cal']
    del df_obs['doy']

    df_obs.columns = ['obs_' + c for c in df_obs.columns]
    df_obs.set_index(['obs_system', 'obs_date'], inplace=True)

    df_daily = pd.merge(
       left=df_daily,
       right=df_obs,
       left_on=['system', 'date'],
       right_index=True,
       how='left')

    ### GAP-FILL MEASURED N2O WITH ATTENUATION ###
    print '     gap-filling observed N2O...'

    # initialize estimated N2O column
    df_daily['obs_n2o_est'] = np.nan

    # initialize estimate N2O variable
    n2o_est = 0.

    # loop through sites and systems
    ss = list(set([tuple(l) for l in df_daily[['site', 'system', 'year_cal']].values.tolist()]))
    ss.sort()

    for site, system, year in ss:
        
        df = df_daily.loc[
            (df_daily['site'] == site) & 
            (df_daily['system'] == system) & 
            (df_daily['year_cal'] == year)]
        
        # get measurement season
        df_measured = df.loc[df['obs_n2o'].notnull()]
        
        if df_measured.empty:
            continue

        print '        ', site, system, year
            
        date1 = df_measured['date'].min()
        date2 = df_measured['date'].max()
        date2 = date2 + dt.timedelta(days=7)

        dr = pd.date_range(date1, date2)
        
        df_measured = df.loc[df['date'].isin(dr)]

        i_n2o_obs = df_measured.columns.tolist().index('obs_n2o') + 1

        for t in df_measured.itertuples():

            # extract measured N2O
            i = t[0]
            n2o_obs = t[i_n2o_obs]

            # if N2O was not measured on this day...
            if np.isnan(n2o_obs):

                # ...calculate estimated N2O based on 
                # prior day estimated (or actual) N2O
                n2o_est = math.exp(1.)**-0.223 * n2o_est
                # incidentially this is an exponential decay
                # function based on data from Michel Cavigelli

            # if N2O was measured on this day...
            else:

                # set estimated N2O to measured N2O
                n2o_est = n2o_obs

            # populate DF with estimated value
            df_daily.loc[i, 'obs_n2o_est'] = n2o_est

    ### INTERPOLATE DAILY CO2 ###
    print '    interpolate daily CO2'
    df_daily['obs_co2_est'] = df_daily['obs_co2'].interpolate(method='linear')
    df_daily.loc[df_daily['obs_n2o_est'].isnull(), 'obs_co2_est'] = np.nan
            
    ### WRITE DAILY VALUES ###    
    print '    write daily values'
    path_daily = os.path.join(
        path_results,
        'results_daily.csv')
    df_daily.to_csv(path_daily, index=False)

    ### CALCULATE ANNUAL RESULTS ###
    print '    calculate annual results'
    
    # calculate annual results
    index_fields = [
        'site',
        'system',
        'phase_year',
        'phase_crop',
        'partition',
        'part_name',
        'year_sim',
        'year_cal']
    
    # select in-season only...
    df_seasonal = df_daily.loc[df_daily['obs_n2o_est'].notnull()].copy(deep=True)
    
    gb_annual = df_seasonal.groupby(
        index_fields)

    # calculate SOC
    df_first = gb_annual.first()[['SOC']]
    df_last  = gb_annual.last()[['SOC']]
    df_soc = df_first.join(
        df_last,
        lsuffix='_init',
        rsuffix='_end')
    df_soc['dsoc'] = df_soc['SOC_end'] - df_soc['SOC_init']

    # calculate all others and join to SOC
    variables = [
        # 'Eco-respiration',
        'Soil-heterotrophic-respiration',
        'CH4-flux',
        'N2O-flux',
        'NO-flux',
        'NH3-flux',
        'NO3-leach',
        'n2oi',
        'obs_gas_flux_n',
        'obs_n2o_est',
        'obs_ch4',
        'obs_co2_est']

    df_var = gb_annual.sum()
    df_annual = df_soc.join(
        df_var[variables])
    df_annual.reset_index(inplace=True)

    df_annual.sort(['site', 'system', 'partition', 'year_cal'], inplace=True)
    
    # write annual results
    path_annual = os.path.join(
        path_results,
        'results_annual.csv')
    df_annual.to_csv(path_annual, index=False)

    ### WEIGHT RESULTS ACROSS PARTITIONS ###
    index_fields = [
        'site',
        'system',
        'phase_year',
        'phase_crop',
        'year_sim',
        'year_cal',
        'date']

    # value_fields = [
        # 'SOC',
        # 'CH4-flux',
        # 'N2O-flux',
        # 'NO-flux',
        # 'NH3-flux',
        # 'NO3-leach',
        # 'n2oi',
        # 'obs_gas_flux_n',
        # 'obs_n2o',
        # 'obs_n2o_sd',
        # 'obs_ch4',
        # 'obs_ch4_sd',
        # 'obs_co2',
        # 'obs_co2_sd',
        # 'obs_wfps',
        # 'obs_wfps_sd',
        # 'obs_soiltemp_c',
        # 'obs_soiltemp_c_sd',
        # 'obs_soiln_n',
        # 'obs_NH4',
        # 'obs_NH4_sd',
        # 'obs_NO3',
        # 'obs_NO3_sd']
    
    part_fields = [
        'part',
        'part_name',
        'part_fraction'] 
    
    value_fields = [i for i in df_daily.columns if i not in part_fields]
    value_fields = [i for i in value_fields if i not in index_fields]
    
    for c in value_fields:
        df_daily[c] = df_daily[c] * df_daily['part_fraction']
    
    df_weighted = df_daily.groupby(index_fields, as_index=False).sum()
    
    df_weighted.sort(['site', 'system', 'year_cal'], inplace=True)
    
    path_weighted = os.path.join(
        path_results,
        'results_daily_whole_field.csv')
    df_weighted.to_csv(path_weighted, index=False)
    
    ### SPLIT FILES BY SYSTEM ###
    # systems = {
        # 'cs1':  [2010, 2011],
        # 'cs2a': [2010, 2011],
        # 'cs2b': [2010, 2011],
        # 'cs4a': [2010, 2011, 2012, 2013, 2014],
        # 'cs4b': [2010, 2011, 2012, 2013, 2014],
        # 'cs4c': [2010, 2011, 2012, 2013, 2014],
        # 'cs4d': [2010, 2011, 2012, 2013, 2014]}
    
    # for system, years in sorted(systems.items()):
        # df = df_weighted.loc[
            # (df_weighted['system'] == system) &  
            # (df_weighted['year_cal'].isin(years))]
        # path_df = os.path.join(
            # path_results,
            # 'results_daily_whole_field_' + system + '.csv')
        # df.to_csv(path_df, index=False)

    # calculate annual weighted results
    index_fields = [
        'site',
        'system',
        'phase_year',
        'phase_crop',
        'year_sim',
        'year_cal']
    
    # calculate on in-season results only
    df_seasonal = df_weighted.loc[df_weighted['obs_n2o_est'].notnull()].copy(deep=True)
    
    gb_annual = df_seasonal.groupby(
        index_fields)

    # calculate SOC
    df_first = gb_annual.first()[['SOC']]
    df_last  = gb_annual.last()[['SOC']]
    df_soc = df_first.join(
        df_last,
        lsuffix='_init',
        rsuffix='_end')
    df_soc['dsoc'] = df_soc['SOC_end'] - df_soc['SOC_init']

    # calculate all others and join to SOC
    variables = [
        # 'Eco-respiration',
        'Soil-heterotrophic-respiration',
        'CH4-flux',
        'N2O-flux',
        'NO-flux',
        'NH3-flux',
        'NO3-leach',
        'n2oi',
        'obs_gas_flux_n',
        'obs_n2o_est',
        'obs_ch4',
        'obs_co2_est']
    df_var = gb_annual.sum()
    df_annual = df_soc.join(
        df_var[variables])
    df_annual.reset_index(inplace=True)

    df_annual.sort(['site', 'system', 'year_cal'], inplace=True)
    
    # write annual results
    print '    write daily values'
    path_annual = os.path.join(
        path_results,
        'results_annual_whole_field.csv')
    df_annual.to_csv(path_annual, index=False)

    return

if __name__ == '__main__':

    extract()
    