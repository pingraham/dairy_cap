
import os, sys
import glob
import pandas as pd

PATH = 'I:\\projects\\dairy_cap\\wicst_n2o\\run1\\results'

path_glob = os.path.join(
    PATH,
    'Batch',
    'Case*')
    # 'Multi_year_summary.csv')

globber = glob.glob(path_glob)

dfs = []
for path_dir in globber:
    
    dirname = os.path.basename(path_dir)
    system, part = dirname.split('-')[3:5]
    part = int(part)
    path_file = os.path.join(
        path_dir,
        'Multi_year_summary.csv')
    df = pd.read_csv(path_file, skiprows=[0,1,3,4])
    df.columns = [c.strip() for c in df.columns]
    
    df['system'] = system
    df['year_cal'] = df['Year'] + 1992
    
    df.set_index(['system','year_cal','Year'], inplace=True)
    
    if system == 'cs2a' or system == 'cs2b':
        part_frac = 0.5
    else:
        part_frac = 1.0

    for c in df.columns:
        df[c] = df[c] * part_frac

    dfs.append(df)
    
df = pd.concat(dfs)

df = df.groupby(level=['system','year_cal','Year']).sum()

path_df = os.path.join(
    PATH,
    'multi_year_combined.csv')
df.to_csv(path_df, index_label=['system','year_cal','year_sim'])    

