import os, sys
from os.path import join as opj
import glob
import math
import datetime as dt
import pandas as pd
import numpy as np
import ags_network

'''
USE THIS FOR FINAL DELIVERY
'''

# global variables for network paths
PROJECT = 'dairy_cap'
PATH    = ags_network.paths(PROJECT)

# database name
DATABASE = 'database.xlsx'

# task name
TASK = 'task1a_arlington'

SITE     = 2
POROSITY = 0.451 # (used DNDC default for site 2)

def extract():

    ### SET PATHS ###
    path_db = opj(
        PATH.projects,
        TASK,
        'calibration',
        DATABASE)
        
    path_results = opj(
        PATH.projects,
        TASK,
        'calibration',
        'results')

    path_batch = opj(
        path_results,
        'Batch')

    ### READ IN DATABASE ###
    data_dict = {}
    sheets = ['field']
    for sheet in sheets:
        data_dict[sheet] = pd.read_excel(
            path_db, 
            sheetname=sheet, 
            skiprows=[0])
    df_field = data_dict['field']
    
    ### READ-IN RESULTS ###
    path_glob = opj(
        path_batch,
        'Case*')
    globber = glob.glob(path_glob)

    results = []
    for path_case in globber:
        
        # get the directory name
        dirpath,dirname = os.path.split(path_case)

        case, site, soil, system, part = dirname.split('-')
        site, soil, part = int(site), int(soil), int(part)

        print '    ', dirname
        
        ### EXTRACT C VARIABLES (DSOC, CH4) ###
        c_globber = glob.glob(os.path.join(path_case, 'Day_SoilC_*.csv'))
        
        dfs = []
        for path_file in c_globber:

            filename = os.path.basename(path_file)
            year_sim = int(filename.replace('.csv','').split('_')[-1])
            
            # lookup field data
            df_sys = df_field.loc[
                (df_field['system'] == system) & 
                (df_field['partition'] == part)].copy(deep=True)
            df_sys.index = range(1,len(df_sys)+1)
            
            year_cal   = df_sys['year'][year_sim]
            phase_year = df_sys['phase_year'][year_sim]
            phase_crop = df_sys['phase_crop'][year_sim]
            part_name  = df_sys['partition_name'][year_sim]
            fraction   = df_sys['fraction'][year_sim]

            df_c = pd.read_csv(
                path_file,
                skiprows=[0],
                sep=',')
            
            df_c.columns = [c.strip() for c in df_c.columns]
            
            # add date
            df_c['year_sim'] = year_sim
            df_c['year_cal'] = year_cal
            df_c['date'] = pd.to_datetime(
                df_c['year_cal'] * 1000 +
                df_c['Day'],
                format='%Y%j')
            
            df_c['site']       = site
            df_c['system']     = system
            df_c['phase_year'] = phase_year
            df_c['phase_crop'] = phase_crop
            df_c['partition']  = part
            df_c['part_name']  = part_name
            df_c['part_fraction']   = fraction

            # sum SOC for 0-20cm layer
            df_c['SOC0-20cm'] = (
                df_c['SOC0-10cm'] + 
                df_c['SOC10-20cm'])
            
            # deal with respiration
            df_c['plant_respiration'] = (
                df_c['Leaf-respiration'] + 
                df_c['Stem-respiration'] + 
                df_c['Root-respiration'])
            df_c['microbial_respiration'] = df_c['Soil-heterotrophic-respiration']
            
            # extract variables
            df_c = df_c[[
                'site',
                'system',
                'phase_year',
                'phase_crop',
                'partition',
                'part_name',
                'part_fraction',
                'year_sim',
                'year_cal',
                'date',
                'Very labile litter',
                'Labile litter',
                'Resistant litter',
                'Microbe',
                'Humads',
                'Humus',
                'SOC',
                'SOC0-20cm',
                'SOC0-10cm',
                'SOC10-20cm',
                'SOC20-30cm',
                'SOC30-40cm',
                'SOC40-50cm',
                'plant_respiration',
                'microbial_respiration',
                'Eco-respiration',
                'NEE']]
            
            dfs.append(df_c)
        df_c = pd.concat(dfs)

        ### EXTRACT N VARIABLES (N2O, NH3, AND N LEACHING) ###
        n_globber = glob.glob(os.path.join(path_case, 'Day_SoilN_*.csv'))
        
        dfs = []
        for path_file in n_globber:

            filename = os.path.basename(path_file)
            year_sim = int(filename.replace('.csv','').split('_')[-1])
            
            # lookup field data
            df_sys = df_field.loc[
                (df_field['system'] == system) & 
                (df_field['partition'] == part)].copy(deep=True)
            df_sys.index = range(1,len(df_sys)+1)
            
            year_cal   = df_sys['year'][year_sim]
            phase_year = df_sys['phase_year'][year_sim]
            phase_crop = df_sys['phase_crop'][year_sim]
            part_name  = df_sys['partition_name'][year_sim]
            fraction   = df_sys['fraction'][year_sim]

            # extract N variables (N2O, NH3, and N leaching)
            df_n = pd.read_csv(
                path_file,
                skiprows=[0, 1, 3, 4],
                sep=',')

            # add date
            df_n['year_sim'] = year_sim
            df_n['year_cal'] = year_cal
            df_n['date'] = pd.to_datetime(
                df_n['year_cal'] * 1000 +
                df_n['Day'],
                format='%Y%j')

            # extract variables
            df_n['site']       = site
            df_n['system']     = system
            df_n['phase_year'] = phase_year
            df_n['phase_crop'] = phase_crop
            df_n['partition']  = part
            df_n['part_name']  = part_name
            df_n['part_fraction']   = fraction

            # sum nitrification and denitrification across whole profile
            df_n['nitrification'] = (
                df_n['nitrification'] + 
                df_n['nitrification.1'] + 
                df_n['nitrification.2'] + 
                df_n['nitrification.3'] + 
                df_n['nitrification.4'])
            df_n['denitrification'] = (
                df_n['denitrification'] + 
                df_n['denitrification.1'] + 
                df_n['denitrification.2'] + 
                df_n['denitrification.3'] + 
                df_n['denitrification.4'])

            # extract variables
            df_n = df_n[[
                'site',
                'system',
                'phase_year',
                'phase_crop',
                'partition',
                'part_name',
                'part_fraction',
                'year_sim',
                'year_cal',
                'date',
                'N2O-flux',
                'NO-flux',
                'N2-flux',
                'NH3-flux',
                'NO3-leach',
                'DON-leach',
                'ION-runoff',
                'NH4+',
                'NO3-',
                'N_fixation',
                'nitrification',
                'denitrification']]

            dfs.append(df_n)
        df_n = pd.concat(dfs)

        ### EXTRACT SOIL CLIMATE VARIABLES (VSWC and temperature) ###
        clim_globber = glob.glob(os.path.join(path_case, 'Day_SoilClimate_*.csv'))
        
        dfs = []
        for path_file in clim_globber:

            filename = os.path.basename(path_file)
            year_sim = int(filename.replace('.csv','').split('_')[-1])
            
            # lookup field data
            df_sys = df_field.loc[
                (df_field['system'] == system) & 
                (df_field['partition'] == part)].copy(deep=True)
            df_sys.index = range(1,len(df_sys)+1)
            
            year_cal   = df_sys['year'][year_sim]
            phase_year = df_sys['phase_year'][year_sim]
            phase_crop = df_sys['phase_crop'][year_sim]
            part_name  = df_sys['partition_name'][year_sim]
            fraction   = df_sys['fraction'][year_sim]

            # extract soil climate variables (temperature and WFPS)
            df_clim = pd.read_csv(
                path_file,
                skiprows=[0, 1, 3],
                sep=',')

            # extract temperature
            df_clim['temp_1cm'] = df_clim['1cm']
            df_clim['temp_5cm'] = df_clim['5cm']
            df_clim['temp_10cm'] = df_clim['10cm']
            df_clim['temp_20cm'] = df_clim['20cm']
            df_clim['temp_0-20cm'] = (
                df_clim['temp_1cm']  +
                df_clim['temp_5cm']  +
                df_clim['temp_10cm'] + 
                df_clim['temp_20cm']) / 4.

            # extract WFPS
            df_clim['wfps_1cm'] = df_clim['1cm.1']
            df_clim['wfps_5cm'] = df_clim['5cm.1']
            df_clim['wfps_10cm'] = df_clim['10cm.1']
            df_clim['wfps_20cm'] = df_clim['20cm.1']
            df_clim['wfps_0-20cm'] = (
                df_clim['wfps_1cm']  +
                df_clim['wfps_5cm']  +
                df_clim['wfps_10cm'] + 
                df_clim['wfps_20cm']) / 4.
            
            # convert to VSWC
            df_clim['vswc_1cm']    = df_clim['wfps_1cm']    * POROSITY
            df_clim['vswc_5cm']    = df_clim['wfps_5cm']    * POROSITY
            df_clim['vswc_10cm']   = df_clim['wfps_10cm']   * POROSITY
            df_clim['vswc_20cm']   = df_clim['wfps_20cm']   * POROSITY
            df_clim['vswc_0-20cm'] = df_clim['wfps_0-20cm'] * POROSITY
            
            # add date
            df_clim['year_sim'] = year_sim
            df_clim['year_cal'] = year_cal
            df_clim['date'] = pd.to_datetime(
                df_clim['year_cal'] * 1000 +
                df_clim['Day'],
                format='%Y%j')

            # extract variables
            df_clim['site']       = site
            df_clim['system']     = system
            df_clim['phase_year'] = phase_year
            df_clim['phase_crop'] = phase_crop
            df_clim['partition']  = part
            df_clim['part_name']  = part_name
            df_clim['part_fraction']   = fraction

            # extract variables
            df_clim = df_clim[[
                'site',
                'system',
                'phase_year',
                'phase_crop',
                'partition',
                'part_name',
                'part_fraction',
                'year_sim',
                'year_cal',
                'date',
                'temp_1cm',
                'temp_5cm',
                'temp_10cm',
                'temp_20cm',
                'temp_0-20cm',
                'wfps_1cm',
                'wfps_5cm',
                'wfps_10cm',
                'wfps_20cm',
                'wfps_0-20cm',
                'vswc_1cm',
                'vswc_5cm',
                'vswc_10cm',
                'vswc_20cm',
                'vswc_0-20cm']]

            dfs.append(df_clim)
        df_clim = pd.concat(dfs)
            
        ### EXTRACT WATER VARIABLES (ET) ###
        w_globber = glob.glob(os.path.join(path_case, 'Day_SoilWater_*.csv'))
        
        dfs = []
        for path_file in w_globber:

            filename = os.path.basename(path_file)
            year_sim = int(filename.replace('.csv','').split('_')[-1])
            
            # lookup field data
            df_sys = df_field.loc[
                (df_field['system'] == system) & 
                (df_field['partition'] == part)].copy(deep=True)
            df_sys.index = range(1,len(df_sys)+1)
            
            year_cal   = df_sys['year'][year_sim]
            phase_year = df_sys['phase_year'][year_sim]
            phase_crop = df_sys['phase_crop'][year_sim]
            part_name  = df_sys['partition_name'][year_sim]
            fraction   = df_sys['fraction'][year_sim]

            # extract soil climate variables (temperature and WFPS)
            df_w = pd.read_csv(
                path_file,
                skiprows=[0],
                sep=',')

            df_w.columns = [c.strip() for c in df_w.columns]
                
            # add date
            df_w['year_sim'] = year_sim
            df_w['year_cal'] = year_cal
            df_w['date'] = pd.to_datetime(
                df_w['year_cal'] * 1000 +
                df_w['Day'],
                format='%Y%j')

            # extract variables
            df_w['site']       = site
            df_w['system']     = system
            df_w['phase_year'] = phase_year
            df_w['phase_crop'] = phase_crop
            df_w['partition']  = part
            df_w['part_name']  = part_name
            df_w['part_fraction']   = fraction

            # calculate AET
            df_w['AET'] = (
                df_w['Transpiration'] + 
                df_w['Evaporation'])
            
            # extract variables
            df_w = df_w[[
                'site',
                'system',
                'phase_year',
                'phase_crop',
                'partition',
                'part_name',
                'part_fraction',
                'year_sim',
                'year_cal',
                'date',
                'PET',
                'AET',
                'Transpiration',
                'Evaporation']]

            dfs.append(df_w)
            
        df_w = pd.concat(dfs)

        # join N to C
        index_fields = [
            'site',
            'system',
            'phase_year',
            'phase_crop',
            'partition',
            'part_name',
            'part_fraction',
            'year_sim',
            'year_cal',
            'date']
        df = df_c.join(
            df_n.set_index(index_fields),
            on=index_fields)
        
        # join Climate to DF
        df = df.join(
            df_clim.set_index(index_fields),
            on=index_fields)
        
        # join water to DF
        df = df.join(
            df_w.set_index(index_fields),
            on=index_fields)        
        
        results.append(df)
    
    df_daily = pd.concat(results)
    df_daily.sort(['site', 'system', 'partition', 'year_cal'], inplace=True)

    df_daily.reset_index(inplace=True)
    del df_daily['index']

    ### WRITE DAILY VALUES ###    
    path_daily = os.path.join(
        path_results,
        'extracted',
        'extracted_daily.csv')
    df_daily.to_csv(path_daily, index=False)
    
    return df_daily

def annual():
    
    ### READ-IN DAILY RESULTS
    path_daily = os.path.join(
        PATH.projects,
        TASK,
        'calibration',
        'results',
        'extracted',
        'extracted_daily.csv')
    df_daily = pd.read_csv(path_daily)
    
    ### CALCULATE ANNUAL RESULTS ###

    # calculate annual results
    index_fields = [
        'site',
        'system',
        'phase_year',
        'phase_crop',
        'partition',
        'part_name',
        'year_sim',
        'year_cal']

    gb_annual = df_daily.groupby(
        index_fields)

    # loop through all pools
    pools = [
        'Very labile litter',
        'Labile litter',
        'Resistant litter',
        'Microbe',
        'Humads',
        'Humus',
        'SOC0-20cm',
        'SOC0-10cm',
        'SOC10-20cm',
        'SOC20-30cm',
        'SOC30-40cm',
        'SOC40-50cm',
        'SOC',
        'NH4+',
        'NO3-']

    dfs = []
    for pool in pools:

        # calculate SOC and dSOC
        df_first = gb_annual.first()[[pool]]
        df_last  = gb_annual.last()[[pool]]
        df_pool = df_first.join(
            df_last,
            lsuffix='_init',
            rsuffix='_end')
        
        if pool == 'SOC':
            df_pool['dsoc'] = df_pool['SOC_end'] - df_pool['SOC_init']
        
        dfs.append(df_pool)
    
    df_pools = pd.concat(dfs, axis=1)

    # calculate all others and join to SOC
    variables = [
        'Eco-respiration',
        'NEE',
        'N2O-flux',
        'NO-flux',
        'N2-flux',
        'NH3-flux',
        'NO3-leach',
        'DON-leach',
        'ION-runoff',
        'N_fixation',
        'nitrification',
        'denitrification',
        'PET',
        'AET',
        'Transpiration',
        'Evaporation']

    df_var = gb_annual.sum()
    df_annual = df_pools.join(
        df_var[variables])
    df_annual.reset_index(inplace=True)

    df_annual.sort(['site', 'system', 'partition', 'year_cal'], inplace=True)
    
    # extract yield
    path_glob = opj(
        PATH.projects,
        TASK,
        'calibration',
        'results',
        'Batch',
        'Case*')
    globber = glob.glob(path_glob)

    results = []
    for path_case in globber:
        path_multi_year = opj(
            path_case,
            'Multi_year_summary.csv')

        dirpath,dirname = os.path.split(path_case)            
        case, site, soil, system, part = dirname.split('-')
        site, soil, part = int(site), int(soil), int(part)

        df_yield = pd.read_csv(
            path_multi_year,
            skiprows=[0,1,3,4])
        df_yield.columns = [c.strip() for c in df_yield.columns]
        
        # set indices
        df_yield['site']       = site
        df_yield['system']     = system
        df_yield['partition']  = part
        df_yield['year_sim']   = df_yield['Year']

        # grain corn, soybean
        df_yield['yield'] = df_yield['GrainC1']
        
        # alfalfa
        df_yield.loc[
            df_yield['Crop1'].isin([0,10]), 
                'yield'] = df_yield['Cut_CropC']
        results.append(df_yield)
    df_yield = pd.concat(results)
    
    # join yield to df_annual
    index_fields = [
        'site',
        'system',
        'partition',
        'year_sim']

    df_annual = df_annual.join(
        df_yield.set_index(index_fields)[['yield']],
        on=index_fields)
    
    # write annual results
    path_annual = os.path.join(
        PATH.projects,
        TASK,
        'calibration',
        'results',
        'extracted',
        'extracted_annual.csv')
    df_annual.to_csv(path_annual, index=False)
    
    return df_annual
    
def weight_daily(df_annual):

    ### READ-IN DAILY RESULTS
    path_daily = os.path.join(
        PATH.projects,
        TASK,
        'calibration',
        'results',
        'extracted',
        'extracted_daily.csv')
    df_daily = pd.read_csv(path_daily)

    ### JOIN YIELD PRIOR TO WEIGHTING ###
    index_fields = [
        'site',
        'system',
        'phase_year',
        'phase_crop',
        'partition',
        'part_name',
        'year_sim',
        'year_cal']
    df_daily = df_daily.join(
        df_annual.set_index(index_fields)[['yield']],
        on=index_fields)
    
    ### WEIGHT RESULTS ACROSS PARTITIONS ###
    index_fields = [
        'site',
        'system',
        'phase_year',
        'phase_crop',
        'year_sim',
        'year_cal',
        'date']

    # value_fields = [
        # 'Very labile litter',
        # 'Labile litter',
        # 'Resistant litter',
        # 'Microbe',
        # 'Humads',
        # 'Humus',
        # 'SOC',
        # 'SOC0-20cm',
        # 'SOC0-10cm',
        # 'SOC10-20cm',
        # 'SOC20-30cm',
        # 'SOC30-40cm',
        # 'SOC40-50cm',
        # 'Eco-respiration',
        # 'NEE',
        # 'N2O-flux',
        # 'NO-flux',
        # 'N2-flux',
        # 'NH3-flux',
        # 'NO3-leach',
        # 'DON-leach',
        # 'ION-runoff',
        # 'NH4+',
        # 'NO3-',
        # 'N_fixation',
        # 'nitrification',
        # 'denitrification',
        # 'temp_1cm',
        # 'temp_5cm',
        # 'temp_10cm',
        # 'temp_20cm',
        # 'temp_0-20cm',
        # 'wfps_1cm',
        # 'wfps_5cm',
        # 'wfps_10cm',
        # 'wfps_20cm',
        # 'wfps_0-20cm',
        # 'vswc_1cm',
        # 'vswc_5cm',
        # 'vswc_10cm',
        # 'vswc_20cm',
        # 'vswc_0-20cm',
        # 'PET',
        # 'AET',
        # 'Transpiration',
        # 'Evaporation']

    part_fields = [
        'partition',
        'part_name',
        'part_fraction'] 

    value_fields = [i for i in df_daily.columns if i not in part_fields]
    value_fields = [i for i in value_fields if i not in index_fields]

    for c in value_fields:
        df_daily[c] = df_daily[c] * df_daily['part_fraction']

    df_weighted = df_daily.groupby(index_fields, as_index=False).sum()

    # deal with yield
    df_mean = df_daily.groupby(index_fields, as_index=False).mean()
    del df_weighted['yield']
    df_weighted = df_weighted.join(
        df_mean.set_index(index_fields)[['yield']],
        on=index_fields)

    df_weighted.sort(['site', 'system', 'year_cal'], inplace=True)

    # remove site field (not needed)
    del df_weighted['site']

    # remove partition fields
    del df_weighted['partition']
    del df_weighted['part_fraction']

    path_weighted = os.path.join(
        PATH.projects,
        TASK,
        'calibration',
        'results',
        'extracted',
        'results_daily_whole_field.csv')
    # write everything but yield...
    df_weighted[df_weighted.columns[0:-1]].to_csv(path_weighted, index=False)

    return df_weighted

def weight_annual(df_weighted):

    ### ANNUAL WEIGHTED ###
    
    # calculate annual weighted results
    index_fields = [
        # 'site',
        'system',
        'phase_year',
        'phase_crop',
        'year_sim',
        'year_cal']
    
    gb_annual = df_weighted.groupby(
        index_fields)

    # loop through all pools
    pools = [
        'Very labile litter',
        'Labile litter',
        'Resistant litter',
        'Microbe',
        'Humads',
        'Humus',
        'SOC0-20cm',
        'SOC0-10cm',
        'SOC10-20cm',
        'SOC20-30cm',
        'SOC30-40cm',
        'SOC40-50cm',
        'SOC',
        'NH4+',
        'NO3-']

    dfs = []
    for pool in pools:

        # calculate SOC and dSOC
        df_first = gb_annual.first()[[pool]]
        df_last  = gb_annual.last()[[pool]]
        df_pool = df_first.join(
            df_last,
            lsuffix='_init',
            rsuffix='_end')
        
        if pool == 'SOC':
            df_pool['dsoc'] = df_pool['SOC_end'] - df_pool['SOC_init']
        
        dfs.append(df_pool)
    
    df_pools = pd.concat(dfs, axis=1)

    # calculate all others and join to SOC
    variables = [
        'plant_respiration',
        'microbial_respiration',
        'Eco-respiration',
        'NEE',
        'N2O-flux',
        'NO-flux',
        'N2-flux',
        'NH3-flux',
        'NO3-leach',
        'DON-leach',
        'ION-runoff',
        'N_fixation',
        'nitrification',
        'denitrification',
        'PET',
        'AET',
        'Transpiration',
        'Evaporation',
        'yield']

    df_var = gb_annual.sum()
    df_annual = df_pools.join(
        df_var[variables])
    df_annual.reset_index(inplace=True)

    # deal with yield
    df_mean = gb_annual.mean()
    del df_annual['yield']
    df_annual = df_annual.join(
        df_mean[['yield']],
        on=index_fields)
    
    # df_annual.sort(['site', 'system', 'year_cal'], inplace=True)
    df_annual.sort(['system', 'year_cal'], inplace=True)
    
    # write annual results
    path_annual = os.path.join(
        PATH.projects,
        TASK,
        'calibration',
        'results',
        'extracted',
        'results_annual_whole_field.csv')
    df_annual.to_csv(path_annual, index=False)

    return

if __name__ == '__main__':

    print 'extract daily results'
    df_daily = extract()

    print 'calculate annual results'
    df_annual = annual()

    print 'weight daily results'
    df_weighted = weight_daily(df_annual)
    
    print 'weight annual results'
    weight_annual(df_weighted)
    
    
    