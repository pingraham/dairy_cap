import os, sys
import pandas as pd

# def xls2dict(path_xls,sheet,index_l,skip_l):
def xls2dict(df, index_l):
    
    '''
    NOTE: CHANGED THIS TO SPEED THINGS UP
    TAKES A WHILE TO OPEN THIS PARTICULAR DB.XLSX...
    '''
    
    # path_xls = text path
    # sheet = text XLS worksheet name
    # index_l = list of column labels for index 
    # skip_l = list of indices of rows to skip
    
    # read in via pandas
    # NOTE: this won't work if you set an index...
    # df = pd.io.excel.read_excel(path_xls,sheetname=sheet,skiprows=skip_l)

    # convert to list of records
    df_records = df.to_dict(orient='records')
    
    # remove blank lines
    l = []
    for r in df_records:
        count_null = 0
        for value in r.values():
            if pd.isnull(value):  # finds pandas NaN and NaT values
                count_null+=1
        if count_null != len(r): # if the whole line is not null, don't skip it...
            l.append(r)

    # check for duplicate headers
    # Pandas automatically adds an integer count to duplicate fields as:
    #     field     1st instance of field
    #     field.1   2nd instance of field
    # create a list of headers with no counters
    headers = [header.split('.')[0] for header in list(df.columns.values)]
    for item in headers:
        c = headers.count(item)
        # print sheet, item # this line will tell you which sheet and header is duplicated
        assert c == 1       # if the count is not 1, a field name is duplicated

    # initiate dictionary
    d = {}
    
    # add records to dictionary, keyed on index
    key_l = []
    for recnum,record in enumerate(l):
        if len(index_l) > 1:
            # for multi-indices, pull out all index values as tuple
            key = tuple([record[i] for i in index_l])
        else:
            # for single indices, use the index value alone
            key = record[index_l[0]]

        # populate the dictionary
        d[key] = record
        
        # check the keys for duplicates
        # print sheet,key       # this will tell you the duplicated key
        assert key not in key_l # a key is used twice in the table
        key_l.append(key)

    return d

# def read_manure(path_xls):
def read_manure(df):
    
    # read in via pandas
    # NOTE: this won't work if you set an index...
    # df = pd.io.excel.read_excel(
        # path_xls,
        # sheetname='manure',
        # skiprows=[0])

    # convert DF to list of records
    records = df.to_dict(orient='records')
    
    # initialize manure dictionary
    manu_d = {}
    
    # loop through and retrieve field keys (scenario, site, crop ID)
    keys = set()
    for record in records:
        
        # get field keys
        scen  = record['scenario']
        meth  = record['meth']
        site  = record['site']
        id_db = record['id_db']
        
        keys.add((scen, meth, site, id_db))
        
        # populate manure dict
        manu_d[scen, meth, site, id_db] = {}

    # add years and applications to dict
    for record in records:
        
        # get field keys
        scen  = record['scenario']
        meth  = record['meth']
        site  = record['site']
        id_db = record['id_db']
        
        # get year
        year = record['year']
        
        # get application
        app = record['application']
        
        # create sub-dict by application number
        try:
            manu_d[scen, meth, site, id_db][year][app] = record
        except KeyError:
            manu_d[scen, meth, site, id_db][year] = {}
            manu_d[scen, meth, site, id_db][year][app] = record

    return manu_d
    
# def read_defaults(path_db):
def read_defaults(df):

    # initiate dictionary
    def_d = {}
    
    # read in via pandas
    # df = pd.io.excel.read_excel(path_db,sheetname='defaults')

    for index,row in df.iterrows():
        table = row.table
        field = row.field
        default = row.default

        if table not in def_d.keys():
            def_d[table] = {}
        
        def_d[table][field] = default
    
    return def_d

def insert_defaults(table_name,table_d,def_d):
    
    # get list of missing fields
    # first extract the field headers from the first record
    keys_table = table_d[table_d.keys()[0]].keys()
    
    # then get the fields from the default table
    try:
        keys_default = def_d[table_name].keys()
    except KeyError:
        return
    
    # then get the difference between them
    keys_update = list(set(keys_default) - set(keys_table))

    # add values to table
    for key in keys_update:
        
        for index_value in table_d.keys():
            
            # create the sub-dictionary
            if key not in table_d[index_value].keys():
                table_d[index_value][key] = {}
            
            table_d[index_value][key] = def_d[table_name][key]

    return

def check_db(data_d):
    
    ''' RULES:
    site-scenario combinations:
        must have at least 1 field
    feedlot manure removal:
        fractions must sum to 1.0
    lagoon, compost, and digester removals:
        fractions must sum to 1.0
    fields:
        manure share must sum to 100
    see also:
        xls2dict: duplicate field names not allowed
        xls2dict: duplicate keys not allowed
    ''' 
    
    # generate all site-scenario combinations
    key_l = [(site,scen) for site in data_d['site'].keys() for scen in data_d['scenario'].keys()]
    key_s = set(key_l)

    ''' CHECK FIELD MANAGEMENT DATA '''
    field_s = set()
    total_manure_d = {}
    for site,scen,field in data_d['field'].keys():
        field_s.add((site,scen))
        
        manu_frac = data_d['field'][site,scen,field]['manu_frac']
        try:
            total_manure_d[site,scen]+=manu_frac
        except KeyError:
            total_manure_d[site,scen] = manu_frac
        
    missing_field_data = list(key_s - field_s)
    # for key in missing_field_data: print key  # this will tell you the missing keys
    assert missing_field_data == []             # if this list is not empty there is missing field data

    for key in total_manure_d:
        # print key                         # this will tell you which key doesn't add up
        assert total_manure_d[key] == 1.0   # the total manure fraction is not 100%

    return

def read_db(path_db):
    
    # initiate data dictionary
    data_d = {}

    ''' READ IN TABLES '''
    print '    read in tables'
    
    # had to re-write the following to accommodate Pandas 15.2
    # df_dict = pd.read_excel(
        # path_db,
        # sheetname=None,
        # skiprows=[0])
    df_dict = {}
    sheets = [
        'site',
        'soils',
        'field',
        'crops',
        'soil_lu',
        'fertilizer',
        'defaults']
    for sheet in sheets:
        df_dict[sheet] = pd.read_excel(
            path_db,
            sheetname=sheet,
            skiprows=[0])
    
    # scen_d  = xls2dict(df_dict['scenario'],   ['scenario']               ) # SCENARIO
    site_d  = xls2dict(df_dict['site'],       ['site']                     ) # SITE
    soil_d  = xls2dict(df_dict['soils'],      ['site','soil']              ) # SOIL
    field_d = xls2dict(df_dict['field'],      ['system','partition','year']) # FIELD
    crop_d  = xls2dict(df_dict['crops'],      ['id_db']                    ) # CROPS
    lu_d    = xls2dict(df_dict['soil_lu'],    ['texture']                  ) # SOIL LOOKUP
    fert_d  = xls2dict(df_dict['fertilizer'], ['fert']                     ) # FERTILIZER

    ''' READ IN DEFAULTS '''
    print '    set defaults'
    def_d = read_defaults(df_dict['defaults'])

    ''' INSERT_DEFAULTS '''
    # insert_defaults('scenario',scen_d,def_d)
    insert_defaults('site',  site_d,  def_d)
    insert_defaults('soil',  soil_d,  def_d)
    insert_defaults('field', field_d, def_d)
    insert_defaults('crop', crop_d, def_d)

    ''' BUILD DATA DICTIONARY '''
    print '    build data dictionary'
    # data_d['scenario']  = scen_d
    data_d['site']      = site_d
    data_d['soil']      = soil_d
    data_d['crop']      = crop_d
    data_d['field']     = field_d
    data_d['soil_lu']   = lu_d
    data_d['fert']      = fert_d

    ''' RUN DATABASE CHECKS '''
    # NOTE: skip these for now...
    # check_db(data_d)
    
    return data_d

def main():

    ''' SET PATHS '''
    path_db = os.path.join(PATH,DATABASE)

    ''' READ IN DATABASE '''
    data_d = read_db(path_db)
    
    print data_d.keys()

if __name__ == '__main__':
    main()
