import os, sys
from os.path import join as opj
import pandas as pd
from db import read_db
import dnd
import ags_network

# global variables for network paths
PROJECT = 'dairy_cap'
PATH    = ags_network.paths(PROJECT)

# database name
DATABASE = 'database.xlsx'

# task name
TASK = 'task1a_arlington'

def main():

    ''' SET PATHS '''
    path_db = opj(
        PATH.projects,
        TASK,
        'calibration',
        DATABASE)
        
    path_inputs = opj(
        PATH.scratch,
        TASK,
        'inputs')

    ''' READ IN DATABASE '''
    print 'read-in database'
    data_d = read_db(path_db)

    ''' GET A DICTIONARY OF YEARS '''
    years = {}
    for system, part, year in data_d['field'].keys():
        try:
            years[system, part].append(year)
        except KeyError:
            years[system, part] = [year]
    
    for key in years.keys():
        years[key] = (
            min(years[key]),
            max(years[key]),
            len(years[key]))

    ''' WRITE INPUTS '''
    # create batch list
    batch_l = []

    print 'create inputs'
    for site in sorted(data_d['site'].keys()):
        
        site = int(site)
        
        # simulate this?
        if data_d['site'][site]['simulate'] == 0: continue

        for soil_key in sorted(data_d['soil'].keys()):

            soil_site, soil = soil_key
            soil_site, soil = int(soil_site), int(soil)
            
            if soil_site != site: continue
        
            # simulate this?
            if data_d['soil'][soil_key]['simulate'] == 0: continue

            for system, part in sorted(years.keys()):
                
                # if 'cs4' not in system:
                    # print 'CAUTION - ONLY INCLUDING CS4'
                    # continue
                
                part = int(part)
                print '   ', site, soil, system, part

                # set keys
                keys = {
                    'site':     site,
                    'soil':     soil,
                    'system':   system,
                    'part':     part}
                
                # instantiate Dnd_File object
                dnd_file = dnd.Dnd_File(keys,years[system, part],PROJECT,data_d)

                # set site name
                dnd_name = dnd_file.dnd_name(filename=True)

                # set input file path
                path_sys = os.path.join(
                    path_inputs,
                    system)
                if not os.path.exists(path_sys):
                    os.mkdir(path_sys)

                path_dnd = os.path.join(path_sys,dnd_name)

                # write DND file
                dnd_file.write_dnd(path_dnd)
                
                # removed this - can't write to C:\ from Nile...
                # path_c = os.path.join(
                    # 'C:\\DNDC\\project\\wicst_n2o\\inputs',
                    # system)
                # if not os.path.exists(path_c):
                    # os.makedirs(path_c)
                # dnd_file.write_dnd(os.path.join(path_c, dnd_file.dnd_name(filename=True)))
                
                # get local (C) path
                path_dnd_c = dnd_file.dnd_path()

                # append path to batch list
                batch_l.append(path_dnd_c)

    # write batch file
    print 'create batch files'
    path_batch = os.path.join(path_inputs,'batch.txt')
    batch_file = open(path_batch,'wb')
    batch_file.write('{0}\r\n'.format(len(batch_l)))
    for line in batch_l:
        batch_file.write('{0}\r\n'.format(line))
    batch_file.close()

    # write batch file for all Windows instances
    w = ['oxbow']

    batch_length =    len(batch_l) / len(w)
    batch_remainder = len(batch_l) % len(w)

    for filenum, file in enumerate(w):

        start = batch_length * filenum
        end = batch_length * (filenum+1)
        if file == w[-1]:
            end = end + batch_remainder

        dnd_l = batch_l[start:end]
        
        path_batch = os.path.join(path_inputs,'batch_' + file + '.txt')
        batch_file = open(path_batch,'wb')
        batch_file.write('{0}\r\n'.format(len(dnd_l)))
        for line in dnd_l:
            batch_file.write('{0}\r\n'.format(line))
        batch_file.close()
    
if __name__ == '__main__':
    main()
