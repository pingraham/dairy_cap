
import os, copy
import datetime as dt
import numpy as np

LINE_LEN = 72
PATH_DATA_C = 'C:\\DNDC\\project'

def line_length(line):
    
    # correct line length
    ll = len(line)
    if ll < LINE_LEN:
        line = line.replace(' ',' '*(LINE_LEN - ll))
    
    return line

def make_line(line,d):

    # replace line variables
    line = line.format(**d)
    
    # correct length
    line = line_length(line)
    
    return line

def make_lines(text_l,d):
    
    dnd_l = []
    
    for line in text_l:
        line = make_line(line,d)
        dnd_l.append(line)
    
    return dnd_l

class Dnd_File:

    def __init__(self,keys,years,project,data_d):
        
        # define DND properties
        self.site       = keys['site']
        # self.scen       = keys['scenario']
        self.soil       = keys['soil']
        self.system     = keys['system']
        self.part       = keys['part']
        self.data       = data_d
        self.project    = project
        
        # define total years
        # years_init = self.data['scenario'][self.scen]['years_init']
        # self.years_init = years_init
        # year_start = self.data['scenario'][self.scen]['year_cal_start']
        # year_end = self.data['scenario'][self.scen]['year_cal_end']
        
        # self.years = year_end - year_start + 1 + years_init
        # self.data['scenario'][self.scen]['years'] = self.years

        self.years      = years[2]

        # equate simulation years and calendar years
        
        # get scenario years info
        # year_cal_start = self.data['scenario'][self.scen]['year_cal_start']
        # year_cal_end = self.data['scenario'][self.scen]['year_cal_end']

        year_cal_start = years[0]
        year_cal_end   = years[1]
        
        # add all summary years to list (in reverse)
        year_l = []
        for year in xrange(year_cal_end,year_cal_start-1,-1):
            year_l.append(year)
        
        # calculate number of summary years
        years_sum = year_cal_end - year_cal_start + 1
        # create a list of leftover years
        year_remainder = year_l[0:self.years%years_sum]
        # extend the list of years
        year_l = year_l * (self.years/years_sum) + year_remainder
        # reverse the list of years
        year_l.reverse()
        
        # add to dictionary
        d = {}
        for num,year_cal in enumerate(year_l):
            year_sim = num + 1
            d[year_sim] = year_cal
        self.year_d = d
        
        return

    def site_lines(self):

        # create site info dict
        site_info_d = {}
    
        # pull out data
        site_info_d['site_name'] = self.dnd_name()

        site_info_d['years'] = self.years
        site_info_d['lat'] = self.data['site'][self.site]['lat']
        site_info_d['daily'] = self.data['site'][self.site]['daily']
        site_info_d['units'] = self.data['site'][self.site]['site_units']
        
        return make_lines(SITE,site_info_d)

    def clim_lines(self):

        # set "clim_files" variable (always equal to "years")
        self.data['site'][self.site]['clim_files'] = self.years
    
        # make climate lines, part 1
        clim_lines = make_lines(CLIMATE1,self.data['site'][self.site])
        
        # get climate file ID
        try:
            clim_id = int(self.data['site'][self.site]['clim_id'])
        except KeyError:
            clim_id = copy.deepcopy(self.site)
        
        # create list of climate files
        for year in xrange(1,self.years+1):
            year_cal = self.year_d[year]
            path_clim = os.path.join(
                PATH_DATA_C + '\\' + 
                self.project + '\\' + 
                'weather' + '\\' + 
                str(year_cal) + '\\' + 
                str(clim_id) + '.txt')
            
            line = str(year) + ' ' + path_clim
            clim_lines.append(line_length(line))
        
        # make climate lines, part 2
        clim_lines.extend(
            make_lines(CLIMATE2,self.data['site'][self.site]))
        
        return clim_lines
        
    def soil_lines(self):
        
        # convert site data to soil dictionary
        soil_d = copy.deepcopy(self.data['soil'][self.site,self.soil])

        # add land-use code to soil dictionary
        soil_d['lu_code'] = self.data['site'][self.site]['lu_code']
        
        # extract clay fraction
        clay = soil_d['clay']

        # extract anncillary soil data from soil table
        for texture in self.data['soil_lu']:
            clay_min = self.data['soil_lu'][texture]['clay_min']
            clay_max = self.data['soil_lu'][texture]['clay_max']
            
            if clay > clay_min and clay <= clay_max:
                soil_d['texture']       = self.data['soil_lu'][texture]['texture']
                soil_d['texture_class'] = self.data['soil_lu'][texture]['texture_class']
                # soil_d['field_cap']     = self.data['soil_lu'][texture]['field_cap']      # USE DB-SPECIFIED VALUES
                # soil_d['wilt_pt']       = self.data['soil_lu'][texture]['wilt_pt']        # USE DB-SPECIFIED VALUES
                # soil_d['hyd_con']       = self.data['soil_lu'][texture]['hyd_con']        # USE DB-SPECIFIED VALUES
                soil_d['porosity']      = self.data['soil_lu'][texture]['porosity']
                break
        
        # make soil lines
        soil_lines = make_lines(
            SOIL,
            soil_d)
        
        return soil_lines

    def crop_lines(self):

        crop_lines = []

        # initial crop info
        crop_lines.extend(
            make_lines(CROP_DATA,{'rotations':self.years}))

        # loop through years
        for year in range(1,self.years+1):

            year_cal = self.year_d[year]
        
            # get field key
            key_field = (self.system, self.part, year_cal)
            
            # set rotations
            crop_lines.extend(
                make_lines(CROP_ROTATIONS,{
                    'rotation': year,
                    'years':    1,
                    'cycles':   1}))
            
            # set cycles
            crops = int(self.data['field'][key_field]['crops'])
            crop_lines.extend(
                make_lines(CROP_CYCLE,{
                    'cycle': 1,
                    'crops': crops}))

            # loop through crops
            for crop in range(1,crops+1):
                
                # initiate crop dictionary
                crop_d = {'crop':crop}
                
                # get crop data based on crop prefix (cp)
                cp = 'crop_' + str(crop) + '_'

                id_db = self.data['field'][key_field][cp + 'id_db']

                plant_date = self.data['field'][key_field][cp + 'plant_date']
                harv_date = self.data['field'][key_field][cp + 'harv_date']
                
                plant_day = plant_date.day
                plant_mo = plant_date.month
                harv_day = harv_date.day 
                harv_mo = harv_date.month
                
                if np.isnan(harv_mo): harv_mo = 12
                if np.isnan(harv_day): harv_day = 31
                
                crop_d['plant_day'] = plant_day
                crop_d['plant_mo'] = plant_mo
                crop_d['harv_day'] = harv_day
                crop_d['harv_mo'] = harv_mo
                
                plant_dt = dt.date(2000,plant_mo,plant_day)
                harv_dt = dt.date(2000,harv_mo,harv_day)
                
                if harv_dt < plant_dt:
                    crop_d['harv_year'] = 2
                else:
                    crop_d['harv_year'] = 1
                    
                crop_d['cover'] = self.data['field'][key_field][cp + 'cover']
                crop_d['residue'] = self.data['field'][key_field][cp + 'residue']
                
                # combine crop record with crop dict
                for key,value in self.data['crop'][id_db].iteritems():
                    
                    if key == 'max_bm':
                        max_bm = value
                    
                    if key == 'frac_grain':
                        frac_grain = value
                    
                    crop_d[key] = value
                
                # convert maximum biomass to maximum yield
                max_yield = max_bm * frac_grain
                
                crop_d['max_yield'] = max_yield

                crop_lines.extend(
                    make_lines(CROPS,crop_d))

            # tillage
            crop_lines.extend(self.till_lines(year_cal))
            
            # fertilization
            crop_lines.extend(self.fert_lines(year_cal))
            
            # manure
            crop_lines.extend(self.manu_lines(year_cal))
            
            # mulch
            crop_lines.extend(self.mulch_lines(year_cal))
            
            # flooding
            crop_lines.extend(self.flood_lines(year_cal))
            
            # irrigation
            crop_lines.extend(self.irri_lines(year_cal))
            
            # grazing
            crop_lines.extend(self.graze_lines(year_cal))

            # cutting
            crop_lines.extend(self.cut_lines(year_cal))
        
        crop_lines.extend(END_OF_FILE)
        
        return crop_lines
        
    def till_lines(self, year_cal):

        # get field key
        key_field = (self.system, self.part, year_cal)
        
        tillages = int(self.data['field'][key_field]['tillages'])
        till_lines = make_lines(TILL,{'tillages':tillages})

        for till in range(1,tillages+1):
        
            # get till data based on crop prefix (cp)
            tp = 'till_' + str(till) + '_'
            
            date = self.data['field'][key_field][tp + 'date']
            day = date.day
            mo = date.month

            # set depth
            depth = self.data['field'][key_field][tp + 'depth']
            terminator = self.data['field'][key_field][tp + 'terminator']
            if terminator == 1:
                depth = 1000

            # determine closest category
            depth_d = {
                0:1,
                5:2,
                10:3,
                20:4,
                30:5,
                1000:7}
            # don't really understand the following, but it works...
            #http://stackoverflow.com/questions/12141150/from-list-of-integers-get-number-closest-to-a-given-value
            cat = min(depth_d, key=lambda x:abs(x-depth))
            depth_code = depth_d[cat]

            till_lines.extend(
                make_lines(TILLS,{
                    'till':till,
                    'day':day,
                    'mo':mo,
                    'depth_code':depth_code}))
        
        return till_lines

    def fert_lines(self, year_cal):

        # get field key
        key_field = (self.system, self.part, year_cal)
    
        ferts = self.data['field'][key_field]['fertilizations']
        fert_lines = make_lines(FERT,{'fertilizations':ferts})

        for fert in xrange(1,int(ferts)+1):
            
            fert_d = {}
            
            fert_d['fert'] = fert
            
            # get fert data based on crop prefix (fp)
            fp = 'fert_' + str(fert) + '_'
            
            date = self.data['field'][key_field][fp + 'date']
            fert_d['day'] = date.day
            fert_d['mo'] = date.month
            depth = self.data['field'][key_field][fp + 'depth']
            
            if depth == 0.0:
                fert_d['meth'] = 0
                fert_d['depth'] = 0.2
            else:
                fert_d['meth'] = 1
                fert_d['depth'] = depth
            
            # get fertilizer type and amount
            fert_type = self.data['field'][key_field][fp + 'type'].lower()
            fert_amt = self.data['field'][key_field][fp + 'amt']

            fert_d['nit']       = self.data['fert'][fert_type]['nit'] * fert_amt
            fert_d['ammbic']    = self.data['fert'][fert_type]['ammbic'] * fert_amt
            fert_d['urea']      = self.data['fert'][fert_type]['urea'] * fert_amt
            fert_d['anh']       = self.data['fert'][fert_type]['anh'] * fert_amt
            fert_d['amm']       = self.data['fert'][fert_type]['amm'] * fert_amt
            fert_d['sulf']      = self.data['fert'][fert_type]['sulf'] * fert_amt
            fert_d['phos']      = self.data['fert'][fert_type]['phos'] * fert_amt
            
            for k,v in self.data['field'][key_field].iteritems():
                fert_d[k] = v
            
            fert_lines.extend(
                make_lines(FERTS,fert_d))
        
        fert_lines.extend(
            make_lines(FERT_OPTION,{'fert_option':0}))
        
        return fert_lines

    def manu_lines(self, year_cal):

        # get field key
        key_field = (self.system, self.part, year_cal)
        
        # get manure data for the year...
        manus = self.data['field'][key_field]['manures']

        # initiate manure lines
        # manus = len(manures)
        manu_lines = make_lines(MANU,{'manus':manus})
        
        for manu in xrange(1,int(manus)+1):
            
            # get manure data based on prefix (mp)
            mp = 'manu_' + str(manu) + '_'
            
            # manure dictionary
            manu_d = {'application':manu}

            # get application date
            date = self.data['field'][key_field][mp + 'date']
            manu_d['day'] = date.day
            manu_d['mo'] = date.month

            # retrieve data
            manu_d['date'] =    self.data['field'][key_field][mp + 'date']
            manu_d['type'] =    self.data['field'][key_field][mp + 'type']
            manu_d['dm'] =      self.data['field'][key_field][mp + 'dm']
            manu_d['orgc'] =    self.data['field'][key_field][mp + 'orgc']
            manu_d['totaln'] =  self.data['field'][key_field][mp + 'totaln']
            manu_d['cn'] =      self.data['field'][key_field][mp + 'cn']
            manu_d['orgn'] =    self.data['field'][key_field][mp + 'orgn']
            manu_d['nh4'] =     self.data['field'][key_field][mp + 'nh4']
            manu_d['no3'] =     self.data['field'][key_field][mp + 'no3']
            manu_d['meth'] =    self.data['field'][key_field][mp + 'meth']
            manu_d['depth'] =   self.data['field'][key_field][mp + 'depth']
            
            # make lines
            manu_lines.extend(
                make_lines(MANUS,manu_d))

        return manu_lines
    
    def mulch_lines(self, year_cal):
        
        ''' NOT FULLY IMPLEMENTED '''
        return make_lines(MULCH,{})
        
    def flood_lines(self, year_cal):
    
        ''' NOT FULLY IMPLEMENTED '''
        return make_lines(FLOOD,self.data['field'][(self.system,self.part,year_cal)])
    
    def irri_lines(self, year_cal):

        # get field key
        key_field = (self.system, self.part, year_cal)
        
        # check for irrigation use
        irri_use = int(self.data['field'][key_field]['irri_use'])
        
        # get irrigation info
        if irri_use == 1:
            irris = int(self.data['field'][key_field]['irris'])
            irri_type = int(self.data['field'][key_field]['irri_type'])
            irri_index = int(self.data['field'][key_field]['irri_index'])
            irri_meth = int(self.data['field'][key_field]['irri_meth'])
        else:
            irris = 0
            irri_type = 0 
            irri_index = 0.0
            irri_meth = 0
        
        irri_d = {
            'irris':irris,
            'irri_type':irri_type,
            'irri_index':irri_index,
            'irri_meth':irri_meth}
        
        irri_lines = make_lines(IRRI,irri_d)
        
        # get application info
        for irri in range(1,irris+1):
            
            # get irri data based on prefix (ip)
            ip = 'irri_' + str(irri) + '_'
            
            date = self.data['field'][key_field][ip + 'date']
            day = date.day
            mo = date.month
            amt = self.data['field'][key_field][ip + 'amt']
            meth = self.data['field'][key_field][ip + 'meth']
            
            app_d = {
                'irri':irri,
                'mo':mo,
                'day':day,
                'amt':amt,
                'meth':meth}
            
            irri_lines.extend(
                make_lines(IRRIS,app_d))
        
        return irri_lines

    def graze_lines(self, year_cal):
        
        ''' NOT FULLY IMPLEMENTED '''
        return make_lines(GRAZE,{})

    def cut_lines(self, year_cal):

        ''' NOTE: IMPLEMENTED SO THAT THE WHOLE PLANT IS CUT ABOVE 
                  SOME HEIGHT (I.E. ALL GRAIN IS CUT, AND THE GIVEN
                  CUT FRACTION IS TAKEN FROM LEAF AND STEM) '''
    
        # get field key
        key_field = (self.system, self.part, year_cal)

        cuts = int(self.data['field'][key_field]['cuts'])
        
        # double the cuts
        cuts = cuts * 2
        
        cut_lines = make_lines(CUT,{'cuts':cuts})
        
        for cut in range(1,cuts+1):

            cut_d = {'cut':cut}

            # get cut data based on crop prefix (cp)
            cp = 'cut_' + str((cut + 1) / 2) + '_'

            # on evens, cut 100% of grain
            if cut % 2 == 0:
                cut_d['frac'] = 1.0
                cut_d['part'] = 1   # only grain
            
            # on odds (actual applications),
            # cut leaf and stem by actual fraction
            else:
                frac = self.data['field'][key_field][cp + 'frac']
                cut_d['frac'] = frac
                cut_d['part'] = 8   # leaf & stem

            date = self.data['field'][key_field][cp + 'date']
            cut_d['day'] = date.day
            cut_d['mo'] = date.month
                
            cut_lines.extend(
                make_lines(CUTS,cut_d))
        
        return cut_lines 
    
    def dnd_lines(self):
        
        dnd_lines = self.site_lines()
        dnd_lines.extend(self.clim_lines())
        dnd_lines.extend(self.soil_lines())
        dnd_lines.extend(self.crop_lines())
        
        return dnd_lines
    
    def write_dnd(self, path_file):

        file = open(path_file,'wb')
        for line in self.dnd_lines():
            file.write('{0}\r\n'.format(line))
        file.close()
        
        return
    
    def dnd_name(self,filename=False):

        dnd_name = (
            str(self.site) + '-' + 
            str(self.soil) + '-' + 
            str(self.system) + '-' + 
            str(self.part))
        
        if filename:
            dnd_name = (
                dnd_name + 
                '.dnd')                
        
        return dnd_name
    
    def dnd_path(self):
        
        dnd_path = os.path.join(
            PATH_DATA_C + '\\' + 
            self.project + '\\' + 
            'inputs' + '\\' + 
            str(self.system) + '\\' + 
            self.dnd_name(filename=True))
        
        return dnd_path

''' MANURE DNDC TEXT '''        

SITE = [
    'DNDC_Input_Parameters',
    '----------------------------------------',
    'Site_infomation',
    '',
    '__Site_name {site_name}',      
    '__Simulated_years {years:.0f}',
    '__Latitude {lat:.4f}',         
    '__Daily_record {daily:.0f}',   
    '__Unit_system {units:.0f}',    
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0']

CLIMATE1 = [
    '----------------------------------------',
    'Climate_data',
    '',
    '__Climate_data_type {clim_format:.0f}',
    '__N_in_rainfall {ndep:.4f}',
    '__Air_NH3_concentration {clim_nh3:.4f}',
    '__Air_CO2_concentration {clim_co2:.4f}',
    '__Climate_files {clim_files:.0f}']

CLIMATE2 = [
    '__Climate_file_mode {clim_mode:.0f}',
    '__CO2_increase_rate {clim_co2_inc:.4f}',
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0']

SOIL = [
    '----------------------------------------',
    'Soil_data',
    '',
    '__Land_use_ID {lu_code:.0f}',
    '__Soil_texture_ID {texture:.0f}',
    '__Bulk_density {dens:.4f}',
    '__pH {ph:.4f}',
    '__Clay_fraction {clay:.4f}',
    '__Porosity {porosity:.4f}',
    '__Bypass_flow {bypass:.4f}',
    '__Field_capacity {field_cap:.4f}',
    '__Wilting_point {wilt_pt:.4f}',
    '__Hydro_conductivity {hyd_con:.4f}',
    '__Top_layer_SOC {soc:.4f}',
    '__Litter_fraction {frac_litter:.4f}',
    '__Humads_fraction {frac_humads:.4f}',
    '__Humus_fraction {frac_humus:.4f}',
    '__Adjusted_litter_factor {adj_litter:.4f}',
    '__Adjusted_humads_factor {adj_humads:.4f}',
    '__Adjusted_humus_factor {adj_humus:.4f}',
    '__Humads_C/N {cn_humads:.4f}',
    '__Humus_C/N {cn_humus:.4f}',
    '__Black_C {black_c:.4f}',
    '__Black_C_C/N {black_cn:.4f}',
    '__SOC_profile_A {soc_profile_a:.4f}',
    '__SOC_profile_B {soc_profile_b:.4f}',
    '__Initial_nitrate_ppm {nit_ppm:.4f}',
    '__Initial_ammonium_ppm {amm_ppm:.4f}',
    '__Soil_microbial_index {soil_micr_ind:.4f}',
    '__Soil_slope {slope:.4f}',
    '__Lateral_influx_index {lat_inf_ind:.4f}',
    '__Watertable_depth {wt_depth:.4f}',
    '__Water_retension_layer_depth {wrl_depth:.4f}',
    '__Soil_salinity {salinity:.4f}',
    '__SCS_curve_use {scs_curve:.0f}',
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0',
    '__None 0']

CROP_DATA = [
    '----------------------------------------',
    'Crop_data',
    '',
    'Cropping_systems {rotations:.0f}']

CROP_ROTATIONS = [
    '',
    '__Cropping_system {rotation:.0f}',
    '__Total_years {years:.0f}',
    '__Years_of_a_cycle {cycles:.0f}']

CROP_CYCLE = [
    '',
    '____Year {cycle:.0f}',
    '____Crops {crops:.0f}']

CROPS = [
    '______Crop# {crop:.0f}',
    '______Crop_ID {id_dndc:.0f}',
    '______Planting_month {plant_mo:.0f}',
    '______Planting_day {plant_day:.0f}',
    '______Harvest_month {harv_mo:.0f}',
    '______Harvest_day {harv_day:.0f}',
    '______Harvest_year {harv_year:.0f}',
    '______Residue_left_in_field {residue:.4f}',
    '______Maximum_yield {max_yield:.4f}',
    '______Leaf_fraction {frac_leaf:.4f}',
    '______Stem_fraction {frac_stem:.4f}',
    '______Root_fraction {frac_root:.4f}',
    '______Grain_fraction {frac_grain:.4f}',
    '______Leaf_C/N {cn_leaf:.4f}',
    '______Stem_C/N {cn_stem:.4f}',
    '______Root_C/N {cn_root:.4f}',
    '______Grain_C/N {cn_grain:.4f}',
    '______Accumulative_temperature {tdd:.4f}',
    '______Optimum_temperature {optt:.4f}',
    '______Water_requirement {h2o_demand:.4f}',
    '______N_fixation_index {nfix:.4f}',
    '______Vascularity {vascularity:.4f}',
    '______If_cover_crop {cover:.0f}',
    '______If_perennial_crop {perennial:.0f}',
    '______If_transplanted {transplanted:.0f}',
    '______Tree_maturity_age {tree_maturity:.4f}',
    '______Tree_current_age {tree_current_age:.4f}',
    '______Tree_max_leaf {tree_max_leaf:.4f}',
    '______Tree_min_leaf {tree_min_leaf:.4f}',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '']

TILL = [
    '----------------------------------------',
    '____Till_applications {tillages:.0f}']

TILLS =[
    '______Till# {till:.0f}',
    '______Till_month {mo:.0f}',
    '______Till_day {day:.0f}',
    '______Till_method {depth_code:.0f}']

FERT = [
    '----------------------------------------',
    '____Fertilizer_applications {fertilizations:.0f}']

FERTS = [
    '______Fertilizing# {fert:.0f}',
    '______Fertilizing_month {mo:.0f}',
    '______Fertilizing_day {day:.0f}',
    '______Fertilizing_method {meth:.0f}',
    '______Fertilizing_depth {depth:.4f}',
    '______Nitrate {nit:.4f}',
    '______Ammonium_bicarbonate {ammbic:.4f}',
    '______Urea {urea:.4f}',
    '______Anhydrous_ammonia {anh:.4f}',
    '______Ammonium {amm:.4f}',
    '______Sulphate {sulf:.4f}',
    '______Phosphate {phos:.4f}',
    '______Slow_release_rate {slow_rel:.4f}',
    '______Nitrification_inhibitor_efficiency {nitr_inhib_eff:.4f}',
    '______Nitrification_inhibitor_duration {nitr_inhib_dur:.4f}',
    '______Urease_inhibitor_efficiency {urease_inhib_eff:.4f}',
    '______Urease_inhibitor_duration {urease_inhib_dur:.4f}',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0']

FERT_OPTION = [
    '____Fertilization_option {fert_option:.0f}']                  

MANU = [
    '----------------------------------------',
    '____Manure_applications {manus:.0f}']

MANUS = [
    '______Manuring# {application:.0f}',
    '______Manuring_month {mo:.0f}',
    '______Manuring_day {day:.0f}',
    '______Manure_amount {orgc:.5f}',
    '______Manure_C/N {cn:.4f}',
    '______Manure_type {type:.0f}',
    '______Manuring_method {meth:.0f}',
    '______Manure_depth {depth:.4f}',
    '______Manure_OrgN {orgn:.4f}',
    '______Manure_NH4 {nh4:.4f}',
    '______Manure_NO3 {no3:.4f}',
    '______None 0']

MULCH = [
    '----------------------------------------',
    '____Film_applications 0',
    '____Method 0',
    '----------------------------------------']

FLOOD = [
    '____Flood_applications {floods:.0f}',
    '____Water_control {water_cont:.0f}',
    '____Flood_water_N {flood_n:.4f}',
    '____Leak_rate {leak_rate:.4f}',
    '____Water_gather_index {water_gather:.4f}',
    '____Watertable_file None',
    '____Empirical_para_1 0.0000',
    '____Empirical_para_2 0.0000',
    '____Empirical_para_3 0.0000',
    '____Empirical_para_4 0.0000',
    '____Empirical_para_5 0.0000',
    '____Empirical_para_6 0.0000']

FLOODS = [
    '______Flooding# {flood:.0f}',
    '______Start_month {mo_start:.0f}',
    '______Start_day {day_start:.0f}',
    '______End_month {mo_end:.0f}',
    '______End_day {day_end:.0f}',
    '______Water_N 0.0000',
    '______Alter_wet_dry {awd:.0f}',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0']

IRRI = [
    '----------------------------------------',
    '____Irrigation_applications {irris:.0f}',
    '____Irrigation_control {irri_type:.0f}',
    '____Irrigation_index {irri_index:.4f}',
    '____Irrigation_method {irri_meth:.0f}']

IRRIS = [
    '______Irrigation# {irri:.0f}',
    '______Irri_month {mo:.0f}',
    '______Irri_day {day:.0f}',
    '______Water_amount {amt:.4f}',
    '______Irri_method {meth:.0f}',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0']

GRAZE = [
    '----------------------------------------',
    '____Grazing_applications 0',
    '----------------------------------------']

CUT = [
    '____Cut_applications {cuts:.0f}']

CUTS = [
    '______Cut# {cut:.0f}',
    '______Cut_month {mo:.0f}',
    '______Cut_day {day:.0f}',
    '______Cut_fraction {frac:.4f}',
    '______Cut_part {part:.0f}',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0',
    '______None 0']

END_OF_FILE = [
    '----------------------------------------']