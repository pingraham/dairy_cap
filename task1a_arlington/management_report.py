
import os, sys
import copy
from os.path import join as opj
import datetime as dt
import pandas as pd
import numpy as np

'''
THIS IS HIGHLY CUSTOMIZED CODE TO EXTRACT MANAGEMENT DATA TO PUT WITH ASSUMPTIONS...
'''

PATH = 'I:\\projects\\wicst'
RUN=1

def standardize_date(date):

    date = dt.date(
        2015,
        date.month,
        date.day)
    
    return date

def main():
    
    path_run = opj(
        PATH,
        'run'+str(RUN))
    path_db = opj(
        path_run,
        'database.xlsx')
    
    # get notes
    df_field = pd.read_excel(
        path_db,
        sheetname='field')
    
    notes = list(df_field.columns)

    # get all data
    df_field = pd.read_excel(
        path_db,
        sheetname='field',
        skiprows=[0])
    
    # get a list of systems
    systems = list(set(df_field['system'].values))
    systems.sort()
    
    # specify output header
    header = [
        'system',
        'year(s)',
        'rotation year',
        'rotation crop',
        'date',
        'event',
        'description']
    
    # initalize rows
    rows = []
    
    # loop through systems and create a report for each one based on specified years
    for system in systems:
        
        df_sys = df_field.loc[df_field['system'] == system]
        
        # loop through records
        for rec, data in df_sys.iterrows():
            
            phase_crop = data['phase_crop']
            
            year = data['year']
            
            if year < 2010:
                year = '1993-2009'
            
            # build first (repeated) part of row
            row_init = [
                data['system'],
                str(year),
                data['phase_year'],
                phase_crop]
            
            # crop planting
            crops = data['crops']
            if crops > 0:
                row = copy.deepcopy(row_init)
                plant_date = standardize_date(data['crop_1_plant_date'])
                row.append(plant_date)
                row.append('plant ' + data['crop_1_name'])
                rows.append(row)
            
            # crop harvesting
            if phase_crop != 'alfalfa':
                row = copy.deepcopy(row_init)
                harv_date = standardize_date(data['crop_1_harv_date'])
                row.append(harv_date)
                row.append('harvest ' + data['crop_1_name'])
                residue = data['crop_1_residue']
                crop_yield = data['crop_1_yield']
                try:
                    crop_yield = str(round(crop_yield, 2))
                except TypeError:
                    pass
                moisture = data['crop_1_moisture']
                desc = (
                    'residue fraction=' + str(round(residue, 2)))
                if year == 2010 or year == 2011 or year == 2012 or year == 2013 or year == 2014:
                    desc = (
                        desc + ', ' +
                        'yield (Mg/ha)=' + crop_yield)
                    if not np.isnan(moisture):
                        desc = (
                            desc + ', ' +
                            'moisture fraction=' + str(round(moisture, 3)))
                row.append(desc)
                rows.append(row)
            
            # tillage
            tillages = data['tillages']
            for tillage in range(1, tillages+1):
                row = copy.deepcopy(row_init)
                row.append(standardize_date(data['till_' + str(tillage) + '_date']))
                row.append('till')
                depth = data['till_' + str(tillage) + '_depth']
                if np.isnan(depth): continue
                row.append(str(int(depth)) + ' cm depth')
                rows.append(row)
                
            # fertilization
            fertilizations = data['fertilizations']
            for fertilization in range(1, fertilizations+1):
                row = copy.deepcopy(row_init)
                row.append(standardize_date(data['fert_' + str(fertilization) + '_date']))
                row.append('apply fertilizer')
                depth = data['fert_' + str(fertilization) + '_depth']
                fert_type = data['fert_' + str(fertilization) + '_type']
                amt = data['fert_' + str(fertilization) + '_amt']
                desc = (
                    fert_type + '@' + 
                    str(round(amt, 1)) + ' kgN/ha to ' + 
                    str(int(depth)) + ' cm depth')
                row.append(desc)
                rows.append(row)
            
            # manure
            manures = data['manures']
            for manure in range(1, manures+1):
                row = copy.deepcopy(row_init)

                row.append(standardize_date(data['manu_' + str(manure) + '_date']))
                row.append('broadcast manure')
                
                amt = data['manu_' + str(manure) + '_dm']

                orgc     = data['manu_' + str(manure) + '_orgc']
                totaln   = data['manu_' + str(manure) + '_totaln']
                cn       = data['manu_' + str(manure) + '_cn']
                orgn     = data['manu_' + str(manure) + '_orgn']
                nh4      = data['manu_' + str(manure) + '_nh4']
                no3      = data['manu_' + str(manure) + '_no3']
                
                orgn_frac = orgn / totaln
                nh4_frac  = nh4 / totaln
                no3_frac  = no3 / totaln
                
                
                desc = (
                    str(int(amt)) + ' kgDM/ha ' + 
                    'dairy slurry (' + 
                    'C=' + str(round(orgc, 1)) + ' kgC/ha, ' + 
                    'N=' + str(round(orgn, 1)) + ' kgN/ha, ' + 
                    'C:N=' + str(round(cn, 1)) + ', ' + 
                    'org N./NH4/NO3 fractions=' + 
                    str(round(orgn_frac, 2)) + '/' + 
                    str(round(nh4_frac, 2)) + '/' + 
                    str(round(no3_frac, 2)) + ')')
                
                row.append(desc)
                rows.append(row)
            
            # cuts
            cuts = data['cuts']
            if phase_crop == 'alfalfa' and cuts > 0:
                
                for cut in range(1, cuts+1):
                    row = copy.deepcopy(row_init)
                    row.append(standardize_date(data['cut_' + str(cut) + '_date']))
                    row.append('harvest alfalfa')
                    frac = data['cut_' + str(cut) + '_frac']
                    desc = (
                        'cut fraction=' + str(round(frac, 2))) 
                    row.append(desc)
                    rows.append(row)
    
    # pare-down rows
    rows = list(set([tuple(row) for row in rows]))
    rows.sort()
    
    # write out data
    path_mgt = opj(
        path_run,
        'dndc_management.csv')
    df = pd.DataFrame(rows,columns=header)
    df.to_csv(path_mgt, index=False)
        
    return

if __name__ == '__main__':
    main()