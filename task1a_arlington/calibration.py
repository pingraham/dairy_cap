#!/usr/bin/env python

import os, sys
from os.path import join as opj
import convert
import glob
import pandas as pd
import numpy as np
import datetime as dt
import ags_network

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

# global variables for network paths
PROJECT = 'dairy_cap'
PATH    = ags_network.paths(PROJECT)

# database name
DATABASE = 'database.xlsx'

# task name
TASK = 'task1a_arlington'

def tdd():
    
    ''' GET WEATHER '''
    # read in weather spreadsheet as DF
    path_xlsx = opj(
        PATH,
        'management',
        'WICST_Weather.xlsx')
    df_weather = pd.read_excel(
        path_xlsx,
        sheetname='WICST flagged weather')
    
    # add year
    df_weather['year'] = df_weather['Date'].dt.year
    
    # calculate mean T
    df_weather['TMEAN'] = (df_weather['TMAX'] + df_weather['TMIN']) / 2.0
    
    # calculate DD
    df_weather['dd'] = 0.
    df_weather.loc[df_weather['TMEAN'] > 0., 'dd'] = df_weather['TMEAN']
    
    ''' GET MANAGEMENT DATA '''
    # read-in management data
    path_db = os.path.join(PATH_RUN,DATABASE)
    data_dict = pd.read_excel(path_db, sheetname=None, skiprows=[0])
    df_field = data_dict['field']
    
    # filter out partition 2 (always has same management as 1)
    df_field = df_field.loc[df_field['partition'] == 1]
    
    # keep key fields
    df_field = df_field[[
        'system',
        'year',
        'phase_year',
        'phase_crop',
        'crop_1_name',
        'crop_1_plant_date',
        'crop_1_harv_date',
        'cut_1_date',
        'cut_2_date',
        'cut_3_date',
        'cut_4_date']]
    
    # set system name
    df_field['system_name'] = df_field['system'].str[:3]
    
    # initialize list of lists
    lol = [[
        'system_name',
        'crop',
        'year',
        'tdd']]
    
    # loop through management records and relate to weather
    for mgt_rec, data in df_field.iterrows():
        system_name       = data['system_name']
        year              = data['year']
        phase_year        = data['phase_year']
        phase_crop        = data['phase_crop']
        crop_1_name       = data['crop_1_name']
        crop_1_plant_date = data['crop_1_plant_date']
        crop_1_harv_date  = data['crop_1_harv_date']
        cut_1_date        = data['cut_1_date']
        cut_2_date        = data['cut_2_date']
        cut_3_date        = data['cut_3_date']
        cut_4_date        = data['cut_4_date']

        # handle alfalfa
        if crop_1_name == 'fallow' or crop_1_name == 'alfalfa':
            crop_1_plant_date = dt.date(year, 4, 1)
            crop_1_harv_date = cut_1_date

        df_season = df_weather.loc[
            (df_weather['Date'] >=  crop_1_plant_date) &
            (df_weather['Date'] <=  crop_1_harv_date)
                ].copy(deep=True)
        
        tdd = df_season['dd'].sum()

        # record data
        lol.append([system_name, phase_crop, year, tdd])

    # tie 'em together
    df = pd.DataFrame(lol[1:], columns=lol[0])
    
    # write it out
    path_tdd = opj(
        PATH_RUN,
        'results',
        'tdd.csv')
    df.to_csv(path_tdd, index=False)
    
    # get minimum TDD by system and crop
    index_fields = [
        'system_name',
        'crop']
    
    df_min = df.groupby(index_fields).min()[['tdd']]
    
    print df_min
    
    path_min = opj(
        PATH_RUN,
        'results',
        'tdd_min.csv')
    df_min.to_csv(path_min)
    
    return

def graph_daily_meas_sim():
    
    ### SET PATHS ###
    path_cal = opj(
        PATH.projects,
        TASK,
        'calibration')

    path_results = opj(
        path_cal,
        'results')

    path_charts = opj(
        path_results,
        'charts')

    ### READ-IN MEASURED VS. SIMULATED ###
    path_mvs = opj(
        path_results,
        'results_daily_whole_field.csv')
    df_mvs = pd.read_csv(path_mvs)

    df_mvs['date'] = pd.to_datetime(df_mvs['date'])

    ### LOOP THROUGH SITES (SOILS) ###
    for site in df_mvs['site'].drop_duplicates():

        ### LOOP THROUGH ATTRIBUTES ###
        
        atts = {
            'n2o': {
                'simulated': 'N2O-flux', 
                'observed':  'obs_n2o',
                'units':     'kgN/ha/d'},
            'soil_temp':  {
                'simulated': 'temp_0-10cm', 
                'observed':  'obs_soiltemp_c',
                'units':     'deg. C'},
            'soil_moist': {
                'simulated': 'wfps_0-10cm', 
                'observed':  'obs_wfps',
                'units':     'WFPS'},
            'co2':        {
                # 'simulated': 'Eco-respiration', 
                'simulated': 'Soil-heterotrophic-respiration',
                'observed':  'obs_co2',
                'units':     'kgC/ha/d'},
            'ch4':        {
                'simulated': 'CH4-flux', 
                'observed':  'obs_ch4',
                'units':     'kgC/ha/d'},
            'nh4':        {
                'simulated': 'NH4+', 
                'observed':  'obs_NH4',
                'units':     'kgN/ha/d'},
            'no3':        {
                'simulated': 'NO3-', 
                'observed':  'obs_NO3',
                'units':     'kgN/ha/d'}}

        for att in atts.keys():
            
            ### CREATE FIGURE ###
            fig = plt.figure(figsize=(20, 12))
            
            ### LOOP THROUGH SYSTEMS
            for sys_num, sys in enumerate(df_mvs['system'].drop_duplicates()):
                
                position = sys_num + 1
                
                # offset cs4
                if position > 3: position+=1
                
                # set calibration years
                if sys == 'cs1':
                    year_cals = [2010]
                if 'cs2' in sys:
                    year_cals = [2010]
                if 'cs4' in sys:
                    year_cals = [2010, 2011, 2012, 2013]
                
                df_grapher = df_mvs.loc[
                    (df_mvs['site'] == site) & 
                    (df_mvs['system'] == sys) & 
                    (df_mvs['year_cal'].isin(year_cals)) & 
                    (df_mvs['obs_n2o_est'].notnull())
                        ].copy(deep=True)

                print '    ', site, sys, att
                
                dict = atts[att]
                
                att_sim = dict['simulated']
                att_meas = dict['observed']
                units = dict['units']

                ### ADD SUBPLOT ###
                ax = fig.add_subplot(2,4,position)
                
                ### MAKE DATA FOR PLOT ###
                mean = pd.rolling_mean(df_grapher[att_sim], 7)
                sd   = pd.rolling_std(df_grapher[att_sim], 7)
                low  = mean - sd * 1.96
                high = mean + sd * 1.96
    
                
                # plot simulated raw daily data
                ax.plot(
                    df_grapher['date'], 
                    df_grapher[att_sim],
                    color='darkgray')
                
    
                # plot simulated 7-day mean
                ax.plot(
                    df_grapher['date'], 
                    mean,
                    color='black')

                # plot simulated 7-day low
                ax.plot(
                    df_grapher['date'], 
                    low,
                    color='grey')
                
                # plot simulated 7-day high
                ax.plot(
                    df_grapher['date'], 
                    high,
                    color='grey')
                
                ax.fill_between(df_grapher['date'].tolist(), high, low, color='grey')

    
                # plot measured daily data
                ax.plot(
                    df_grapher['date'], 
                    df_grapher[att_meas],
                    linestyle='None',
                    marker='o',
                    color='orange',
                    markersize=5)


                # title
                title = (
                    att + 
                    ', sys: ' + sys + 
                    ', site: ' + str(site))
                ax.set_title(title, size=10)
                
                # X-axis
                # if position in [3,4]:
                for label in ax.get_xticklabels():
                    label.set_rotation('vertical')
                    label.set_size(8)
                    label.set_horizontalalignment('center')
                # else:
                    # ax.set_xticklabels('')

                # Y-axis
                for label in ax.get_yticklabels():
                    label.set_size(8)
                
                # if position in [1,3]:
                ax.set_ylabel(units, size=9)
                # plt.ylim(0.0, plt.ylim()[1])


                if att == 'n2o':
                    ax.set_ylim(0, 0.5)


                # grid
                ax.grid()

            # tight layout
            # plt.tight_layout()
            fig.tight_layout()
                
            # export
            png_name = (
                'mvs_' + 
                att + '_' + 
                str(site) + 
                '.png')
            plt.savefig(opj(path_charts, png_name))

            # close
            plt.close()
        
    return

def soil_moist():
    
    ''' SET PATHS '''
    path_db = opj(
        PATH.projects,
        TASK,
        'calibration',
        DATABASE)
        
    path_results = opj(
        PATH.projects,
        TASK,
        'calibration',
        'results')

    ''' READ IN DAILY RESULTS '''
    path_daily = opj(
        path_results,
        'results_daily_whole_field.csv')
    df_daily = pd.read_csv(path_daily)
    
    ''' CALCULATE MEAN DAILY WFPS '''
    df = df_daily.loc[df_daily['obs_n2o_est'].notnull()]
    
    gb = df.groupby(['site','system','year_cal'], as_index=False).mean()
    
    path_wfps = opj(
        path_results,
        'wfps_daily_by_system_and_year.csv')
    gb[['site','system','year_cal','wfps_0-10cm','obs_wfps']].to_csv(
        path_wfps,
        index=False)
    
    
    return
    
def annual_yield():
    
    ''' SET PATHS '''
    path_db = opj(
        PATH.projects,
        TASK,
        'calibration',
        DATABASE)
        
    path_results = opj(
        PATH.projects,
        TASK,
        'calibration',
        'results')

    path_batch = opj(
        path_results,
        'Batch')

    ''' READ IN DATABASE '''
    data_dict = {}
    sheets = ['field']
    for sheet in sheets:
        data_dict[sheet] = pd.read_excel(
            path_db, 
            sheetname=sheet, 
            skiprows=[0])
    df_field = data_dict['field']
    # replace "na" values in yield
    df_field['crop_1_yield'].replace(to_replace="na", value=np.nan, inplace=True)
    
    ''' READ-IN RESULTS '''
    path_glob = opj(
        path_batch,
        'Case*',
        'Multi_year_summary.csv')
    globber = glob.glob(path_glob)

    results = []
    for path_file in globber:
        
        # get the directory name
        path_dir,filename = os.path.split(path_file)
        dirname = os.path.basename(path_dir)
        
        case, site, soil, system, part = dirname.split('-')
        site, soil, part = int(site), int(soil), int(part)
        
        # extract results as DataFrame
        df = pd.read_csv(
            path_file,
            skiprows=[0,1,3,4])
        df.columns = [c.strip() for c in df.columns]
        
        # replace bad values in stress fields
        stress_fields = ['WaterStress', 'N_Stress']
        for c in stress_fields:
            df[c].replace(to_replace="  -nan(ind)", value=np.nan, inplace=True)
            df[c] = df[c].astype(float)

        df['site'] = site
        
        # extract database field subset
        df_subset = df_field.loc[
            (df_field['system'] == system) & 
            (df_field['partition'] == part)].copy(deep=True)
        df_subset.index = [i for i in range(len(df))]

        subset_fields = [
            'system',
            'year',
            'phase_year',
            'phase_crop',
            'partition',
            'partition_name',
            'fraction',
            'crop_1_yield',
            'crop_1_moisture']

        # join field info to results
        df = df.join(df_subset[subset_fields])
        
        # append DF to list
        results.append(df)
    
    ''' CONCATENATE RESULTS '''
    df = pd.concat(results)
    df['year_cal'] = df['year']
    del df['year']
    df['year_sim'] = df['Year']
    del df['Year']

    ''' SELECT CALIBRATION YEARS '''
    # set calibration years
    df_cs1 = df.loc[
        (df['system'] == 'cs1') & 
        (df['year_cal'].isin(range(2000, 2010+1)))
            ].copy(deep=True)
    df_cs2 = df.loc[
        (df['system'].isin(['cs2a','cs2b'])) & 
        (df['year_cal'].isin(range(2000, 2010+1)))
            ].copy(deep=True)
    df_cs4 = df.loc[
        (df['system'].isin(['cs4a','cs4b','cs4c','cs4d'])) & 
        (df['year_cal'].isin(range(2000, 2013+1)))
            ].copy(deep=True)
    
    df = pd.concat([df_cs1, df_cs2, df_cs4])
    
    ''' WEIGHT RESULTS ACROSS PARTITIONS '''
    index_fields = [
        'site',
        'system',
        'phase_year',
        'phase_crop',
        'year_sim',
        'year_cal']

    part_fields = [
        'partition',
        'partition_name',
        'fraction'] 
    
    value_fields = [i for i in df.columns if i not in part_fields]
    value_fields = [i for i in value_fields if i not in index_fields]

    for c in value_fields:
        df[c] = df[c] * df['fraction']
    
    df_weighted = df.groupby(index_fields, as_index=False).sum()
    
    ''' SET-UP YIELD '''
    # create a measured yield field
    df_weighted['yield_meas'] = df_weighted['crop_1_yield']
    
    # convert measured yield to dry matter
    df_weighted['yield_meas'] = df_weighted['yield_meas'] * (1.0 - df_weighted['crop_1_moisture'])
    
    # create a simulated yield field
    df_weighted['yield_sim'] = df_weighted['GrainC1']
    
    # set alfalfa yield to "Cut_CropC"
    df_weighted.loc[
        df_weighted['phase_crop'] == 'alfalfa',
            'yield_sim'] = df_weighted['Cut_CropC']
    
    # convert simulated yield from Mg/ha to kg/ha
    df_weighted['yield_sim'] = convert.units(df_weighted['yield_sim'], 'kg/ha', 'Mg/ha')
    
    # convert simulated yield to dry matter
    df_weighted['yield_sim'] = df_weighted['yield_sim'] * 0.4**-1.
    
    ''' SUM ACROSS SYSTEMS '''
    # NOTE: THIS IS DONE FOR CS4 ALFALFA WHICH ALWAYS HAS 3 ALFALFA 
    #       CROPS IN A GIVEN YEAR
    df_weighted['system_phase'] = df_weighted['system']
    df_weighted['system'] = df_weighted['system_phase'].str[:3]
    
    df_weighted['crop'] = df_weighted['phase_crop']
    
    df_weighted['count'] = 1
    
    index_fields = [
        'site',
        'system',
        'crop',
        'year_sim',
        'year_cal']

    # calculate mean stress
    df_stress = df_weighted[
        index_fields + ['ColdStress','WaterStress','N_Stress']
            ].groupby(index_fields).mean()
        
    # calculate summed yield (really only for Alfalfa)
    df_weighted = df_weighted[
        index_fields + ['yield_meas', 'yield_sim', 'count']
            ].groupby(index_fields, as_index=False).sum()

    df_weighted = df_weighted.join(
        df_stress,
        on=index_fields)
    
    ''' MAKE IT PRETTY '''
    df_weighted.sort(['site', 'system', 'year_cal'], inplace=True)
    df_weighted['sq'] = (df_weighted['yield_meas'] - df_weighted['yield_sim'])**2.
    
    # write it out
    path_file = os.path.join(
        path_results,
        'yield',
        'annual_yield.csv')
    df_weighted.to_csv(path_file, index=False)
    
    # write out by site, system and crop
    for site in set(df_weighted['site'].values):
        df_site = df_weighted.loc[df_weighted['site'] == site]
        for system in set(df_weighted['system'].values):
            df_sys = df_site.loc[df_site['system'] == system]
            for crop in set(df_sys['crop'].values):
                print site, system, crop
                df_sys_crop = df_sys.loc[df_sys['crop'] == crop].copy(deep=True)
                path_file = opj(
                    path_results,
                    'yield',
                    'annual_yield_' + str(site) + '_' + system + '_' + crop + '.csv')
                df_sys_crop.to_csv(path_file, index=False)
    
    ''' CALCULATE RELATIVE RMSE BY SYSTEM '''
    df_weighted['sq'] = (df_weighted['yield_meas'] - df_weighted['yield_sim'])**2.

    df_ss = df_weighted.groupby(['site', 'system','crop']).mean()[['yield_meas', 'sq']]
    df_ss['rmse'] = df_ss['sq']**0.5 # square root
    df_ss['rmse_rel'] = df_ss['rmse'] / df_ss['yield_meas']

    df_corr = df_weighted.groupby(['site', 'system','crop'])[['yield_meas', 'yield_sim']].corr()
    df_corr = df_corr.loc[df_corr['yield_meas'] != 1.0000]
    
    df_corr = df_corr[['yield_meas']]#.reset_index(inplace=True)
    df_corr.reset_index(inplace=True)
    del df_corr['level_3']
    df_corr.set_index(['site', 'system', 'crop'], inplace=True)
    
    df_corr.columns = ['r']
    
    df_ss = df_ss.join(
        df_corr)
    
    df_ss = df_ss[['yield_meas', 'rmse', 'rmse_rel', 'r']]
    
    # write out
    path_rmse = opj(
        path_results,
        'yield',
        'rmse_rel_by_system.csv')
    
    df_ss.to_csv(path_rmse)

    ''' CALCULATE RELATIVE RMSE BY CROP '''
    df_weighted['sq'] = (df_weighted['yield_meas'] - df_weighted['yield_sim'])**2.

    df_ss = df_weighted.groupby(['site', 'crop']).mean()[['yield_meas', 'sq']]
    df_ss['rmse'] = df_ss['sq']**0.5 # square root
    df_ss['rmse_rel'] = df_ss['rmse'] / df_ss['yield_meas']

    df_corr = df_weighted.groupby(['site', 'crop'])[['yield_meas', 'yield_sim']].corr()
    df_corr = df_corr.loc[df_corr['yield_meas'] != 1.0000]
    
    df_corr = df_corr[['yield_meas']]#.reset_index(inplace=True)
    df_corr.reset_index(inplace=True)
    del df_corr['level_2']
    df_corr.set_index(['site', 'crop'], inplace=True)
    
    df_corr.columns = ['r']
    
    df_ss = df_ss.join(
        df_corr)
    
    df_ss = df_ss[['yield_meas', 'rmse', 'rmse_rel', 'r']]
    
    # write out
    path_rmse = opj(
        path_results,
        'yield',
        'rmse_rel_by_crop.csv')
    
    df_ss.to_csv(path_rmse)
    
    return df_weighted

def graph_yield():
    
    ''' SET PATHS '''
    path_task = opj(
        PATH.projects,
        TASK)

    path_results = opj(
        path_task,
        'calibration',
        'results')

    path_batch = opj(
        path_results,
        'Batch')
    
    ''' GET PATHS '''
    path_chart = os.path.join(
        path_results,
        'yield')
    
    ''' GET YIELD RESULTS '''
    path_file = os.path.join(
        path_results,
        'yield',
        'annual_yield.csv')
    df_yield = pd.read_csv(path_file)

    ''' IMPORT PLOTLY '''
    # Plotly
    import plotly.plotly as py
    import plotly.graph_objs as go

    # Sign in to Plotly
    import plotly.tools as tls
    tls.set_credentials_file(username='pingraham', api_key='jyjpp486x0')
    
    ''' PROCESS DATA '''
    # get systems
    systems = list(set(df_yield['system'].values))
    systems.sort()

    # loop through systems and crops
    for system in systems:
        
        df_sys = df_yield.loc[df_yield['system'] == system].copy(deep=True)
        
        crops = list(set(df_sys['crop'].values))
        crops.sort()
        
        for crop in crops:
            
            print '   ', system, crop
            
            df_crop = df_sys.loc[df_sys['crop'] == crop].copy(deep=True)
            df_crop.sort(['year_cal'], inplace=True)

            ''' SCATTER PLOT '''
            # create a regression line
            model = pd.ols(x=df_crop['yield_meas'], y=df_crop['yield_sim'])
            x = model.beta['x']
            intercept = model.beta['intercept']
            df_crop['y'] = df_crop['yield_meas'] * x + intercept

            # create a 1:1 line
            o2omin = min(min(df_crop['yield_meas']), min(df_crop['yield_sim']))
            o2omax = max(max(df_crop['yield_meas']), max(df_crop['yield_sim']))

            # set-up the chart data
            trace1 = go.Scatter(
                x = df_crop['yield_meas'],
                y = df_crop['yield_sim'],
                mode = 'markers',
                name=system + '_' + crop)
                
            trace2 = go.Scatter(
                x = [o2omin, o2omax],
                y = [o2omin, o2omax],
                name='1:1',
                mode = 'lines',
                line = dict(
                    width=1,
                    dash='dash',
                    color='rgb(0,0,0)'))
                
            trace3 = go.Scatter(
                x = df_crop['yield_meas'],
                y = df_crop['y'],
                mode = 'lines',
                name='regression')
                
            data = [trace1, trace2, trace3]

            # set-up the layout
            layout = go.Layout(
                title=system + ', ' + crop + ' yield (MgDM/ha)',
                width=1400,
                height=900,
                yaxis=dict(
                    title='simulated yield',
                    showgrid=True,
                    rangemode='tozero'),
                xaxis=dict(
                    title='measured yield',
                    showgrid=True,
                    rangemode='tozero')
                    )
            
            # set up the figure
            fig = go.Figure(
                data=data,
                layout=layout)

            # write image out
            py.image.save_as(fig, filename=opj(path_chart,'scatter_' + system + '_' + crop + '.png'))
            
            
            
            # post the plot online
            # plot_url = py.plot(fig, filename='dairy_cap_scatter_' + system + '_' + crop)
            # sys.exit()
            
            
            
            ''' LINE PLOT '''
            # measured yield
            trace1 = go.Scatter(
                x = df_crop['year_cal'],
                y = df_crop['yield_meas'],
                mode = 'markers+lines',
                name='measured')
            
            # simulated yield
            trace2 = go.Scatter(
                x = df_crop['year_cal'],
                y = df_crop['yield_sim'],
                mode = 'markers+lines',
                name='simulated')
            
            # N stress
            trace3 = go.Scatter(
                x = df_crop['year_cal'],
                y = df_crop['N_Stress'],
                mode = 'markers+lines',
                name='N stress',
                yaxis='y2',
                marker = dict(
                    size=4),
                line = dict(
                    width=1,
                    dash='dash'))

            # moisture stress
            trace4 = go.Scatter(
                x = df_crop['year_cal'],
                y = df_crop['WaterStress'],
                mode = 'markers+lines',
                name='Moisture Stress',
                yaxis='y2',
                marker = dict(
                    size=4),
                line = dict(
                    width=1,
                    dash='dash'))

            # temperature stress
            trace5 = go.Scatter(
                x = df_crop['year_cal'],
                y = df_crop['ColdStress'],
                mode = 'markers+lines',
                name='Temperature Stress',
                yaxis='y2',
                marker = dict(
                    size=4),
                line = dict(
                    width=1,
                    dash='dash'))

            data = [trace1, trace2, trace3, trace4, trace5]
            
            # set-up the layout
            layout = go.Layout(
                title=system + ', ' + crop + ' yield (MgDM/ha)',
                xaxis=dict(
                    title='year',
                    showgrid=True),
                yaxis=dict(
                    title='yield',
                    showgrid=True,
                    rangemode='tozero'),
                yaxis2=dict(
                    title='stress index',
                    rangemode='tozero',
                    side='right',
                    overlaying='y')
                    )
            
            # set up the figure
            fig = go.Figure(
                data=data,
                layout=layout)
            
            # write image out
            py.image.save_as(fig, filename=opj(path_chart,'line_' + system + '_' + crop + '.png'))
    
    return
    
if __name__ == '__main__':
    
    # print 'TDD stats'
    # tdd()
    
    print 'graph daily measured vs. simulated'
    graph_daily_meas_sim()
    
    print 'get soil moisture'
    soil_moist()
    
    print 'get annual yield'
    annual_yield()
    
    print 'graph annual yield'
    graph_yield()
    