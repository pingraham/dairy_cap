#!/usr/bin/env python

import os, sys
from os.path import join as opj
import glob
import ags_network
import soil
import pandas as pd
import numpy as np

# CLUSTERING
import scipy.cluster.vq as vq
from scipy.spatial.distance import pdist
from scipy.spatial.distance import squareform

# global variables for network paths
PROJECT = 'dairy_cap'
PATH    = ags_network.paths(PROJECT)

# task name
TASK = 'task3_lca'

''' SELECT STATSGO SOILS BY CLIMATE DIVISION '''
HYDRIC_RATING = 'No'    # non-hydric soils
MIN_DATA      = 0.5     # minimum acceptable data
MIN_AREA      = 10.     # minimum acceptable component ag. area (ha)

''' SET THE % OF COMPONENTS TO USE AS CLUSTERS '''
COMPONENT_PCT = 0.05
ACCEPTABLES_RANGES = [
    ]

def combine():

    ### SET PATHS ###
    path_soil = opj(
        PATH.projects,
        TASK,
        'soil')
    
    ### READ IN DATA ###
    
    # STATSGO % CDL ag.
    path_ag = opj(
        path_soil,
        'statsgo_by_clim_div_by_cdl_ag.csv')
    df_ag = pd.read_csv(path_ag)
    
    # STATSGO attributes
    path_att = opj(
        path_soil,
        'attributes_by_component_10.0cm.csv')
    df_att = pd.read_csv(path_att)
    
    # STATSGO attribute availability
    path_avail = opj(
        path_soil,
        'attributes_by_component_10.0cm_available_data.csv')
    df_avail = pd.read_csv(path_avail)
    
    ### COMBINE DATA ###
    
    # combine pH data
    df_att.loc[df_att['ph1to1h2o_r'].isnull(), 'ph1to1h2o_r'] = df_att['ph01mcacl2_r']
    df_avail.loc[df_avail['ph1to1h2o_r']==0., 'ph1to1h2o_r'] = df_avail['ph01mcacl2_r']

    # pare to four attributes
    index_fields = [
        'mukey',
        'muname',
        'cokey',
        'compname',
        'comppct_r',
        'hydricrating',
        'drainagecl',
        'depth_cm']
    atts = [
        'sandtotal_r',
        'silttotal_r',
        'claytotal_r',
        'om_r',
        'dbthirdbar_r',
        'ph1to1h2o_r']
    
    df_att = df_att[index_fields + atts]
    
    # join attributes
    df_data = df_ag.join(
        df_att.set_index('mukey'),
        on='mukey',
        how='inner')
        # how='right')
    
    # join available data
    df_avail = df_avail[['mukey','cokey'] + atts]
    df_data = df_data.join(
        df_avail.set_index(['mukey','cokey']),
        on=['mukey','cokey'],
        rsuffix='_data')
    
    # calculate minimum available data
    df_data['min_data'] = df_data[['claytotal_r_data','om_r_data','dbthirdbar_r_data','ph1to1h2o_r_data']].min(axis=1)

    ### CALCULATE TEXTURE ###
    
    # create unique index
    df_data.reset_index(inplace=True)
    del df_data['index']
    
    # create texture class field
    df_data['texture_class'] = np.nan
    
    # loop through fields and set 
    for i, data in df_data.iterrows():
        
        sand = data['sandtotal_r']
        clay = data['claytotal_r']
        silt = data['silttotal_r']
        
        if np.isnan(sand): continue
        if np.isnan(silt): continue
        if np.isnan(clay): continue

        df_data.loc[i, 'texture_class'] = soil.usda_texture(sand, clay, silt, adjust=True)
    
    ### WRITE ALL DATA TO FILE ###
    df_data.sort(columns=['clim_div','mukey','cokey'], inplace=True)
    
    path_data = opj(
        path_soil,
        'statsgo_selection.csv')
    df_data.to_csv(path_data, index=False)
    
    return

def select():
    
    ### READ IN DATA ###
    path_soil = opj(
        PATH.projects,
        TASK,
        'soil')
    
    path_data = opj(
        path_soil,
        'statsgo_selection.csv')
    df_data = pd.read_csv(path_data)
    
    ### SELECT DATA ###
    
    print 'total records:', len(df_data), 'records'
    
    ### SELECT HYDRIC ###
    df_data = df_data.loc[df_data['hydricrating']==HYDRIC_RATING]
    print 'hydric soils filtered:', len(df_data), 'records'
    
    ### SELECT MINIMUM AMOUNT OF DATA ###
    df_data = df_data.loc[df_data['min_data']>=MIN_DATA]
    print 'minimal data filtered:', len(df_data), 'records'
    
    ### COMPONENT HAS TO HAVE USDA TEXTURE ###
    df_data = df_data.loc[df_data['texture_class'].notnull()]
    print 'missing texture data filtered:', len(df_data), 'records'    
    
    ### CALCULATE COMPONENT AG. AREA ###
    index_fields = ['clim_div', 'mukey']
    gb = df_data.groupby(index_fields, as_index=False)[['comppct_r']].sum()
    gb.columns = index_fields + ['comppct_r_total']
    df_data = df_data.join(
        gb.set_index(index_fields),
        on=index_fields)
    df_data['comp_fraction'] = df_data['comppct_r'] / df_data['comppct_r_total']
    df_data['comp_ag_area'] = df_data['area'] * df_data['comp_fraction']

    ### SELECT MINIMUM TOTAL AREA ###
    df_data = df_data.loc[df_data['comp_ag_area']>=MIN_AREA]
    print 'low component ag. area filtered:', len(df_data), 'records'

    ### WRITE FILE OUT ###
    path_data = opj(
        path_soil,
        'statsgo_filtered.csv')
    df_data.to_csv(path_data, index=False)
    
    return

def cluster():

    ### READ IN DATA ###
    path_soil = opj(
        PATH.projects,
        TASK,
        'soil')
    
    path_data = opj(
        path_soil,
        'statsgo_filtered.csv')
    df_data = pd.read_csv(path_data)

    ### ADD SOIL TEXTURE CLASSES ###
    path_lut = opj(
        path_soil,
        'soil_lookup.csv')
    df_lut = pd.read_csv(path_lut)
    df_lut.set_index('texture_class', inplace=True)
    
    df_data = df_data.join(
        df_lut[['texture']],
        on='texture_class')
    
    '''  DON'T DO THIS FOR NOW...
    for rec, data in df_lut.iterrows():
        clay_min       = data['clay_min']
        clay_max       = data['clay_max']
        texture        = data['texture']
        texture_class  = data['texture_class']
        
        df_data.loc[
            (df_data['claytotal_r'] >  clay_min*100.) &
            (df_data['claytotal_r'] <= clay_max*100.), 
                'texture'] = int(texture)
            
        df_data.loc[
            (df_data['claytotal_r'] >  clay_min*100.) &
            (df_data['claytotal_r'] <= clay_max*100.), 
                'texture_class'] = texture_class
    '''
    
    ### INITIALIZE CLUSTERING
    np.random.seed(8217)
    nrestart = 20

    # selected data fields
    data_fields = [
        'om_r',
        'dbthirdbar_r',
        'ph1to1h2o_r']
    
    # list of cluster data
    dfs = []
    
    ### CLUSTER WITHIN CLIMATE DIVISION & CLAY ###
    units = df_data[['clim_div', 'texture', 'texture_class']].values.tolist()
    units = list(set([tuple(i) for i in units]))
    units.sort()

    for unit in units:
        
        clim_div, texture, texture_class = unit
        
        # subset to climate division
        df_subdata = df_data.loc[
            (df_data['clim_div']==clim_div) &
            (df_data['texture']==texture)].copy(deep=True)
        df_subdata.reset_index(inplace=True)

        # set number of clusters based on number of components
        nclust = int(round(len(df_subdata) * COMPONENT_PCT, 0))

        print clim_div, '-', texture_class, '-', len(df_subdata), nclust, 'clusters'
        dfs.append(df_subdata[['clim_div', 'texture', 'texture_class', 'claytotal_r', 'om_r','dbthirdbar_r','ph1to1h2o_r']])
        continue
        data = df_subdata[data_fields].as_matrix()
        
        ''' ROB'S CODE '''
        features = vq.whiten(data) # normalizes data (obs/SD)
        centroids, distortion = vq.kmeans(features, nclust, iter=nrestart)
        labels = (vq.vq(features, centroids)[0] + 1).astype('int8')
        nrow, ndim = data.shape
        
        nearestrows = []
        # for i in range(nclust):
        for i in range(len(centroids)):
            centroid = centroids[i].reshape(1, ndim)
            absdiff = np.abs((features - centroid)).sum(1)
            nearestrows.append(absdiff.argmin())
        medioids = data[nearestrows,:]
        
        ''' ROB'S CODE '''
        '''
        print 'labels:',len(labels)
        print labels
        print
        print 'centroids:'
        print centroids
        print 
        print 'mediods:'
        print medioids
        print
        print 'nearest rows:',len(nearestrows)
        print nearestrows
        print
        print 'distortion:',distortion
        '''

        df_subdata['cluster']  = labels
        df_subdata['centroid'] = 0
        
        df_subdata.loc[df_subdata.index.isin(nearestrows), 'centroid'] = 1
        
        dfs.append(df_subdata)

    ### CONCATENATE ###
    df_clustered = pd.concat(dfs)
    
    ### WRITE OUT TO FILE ###
    df_clustered.sort(
        columns=['clim_div', 'cluster', 'centroid'], 
        ascending=[True, True, False],
        inplace=True)
    
    path_clusters = opj(
        path_soil,
        'statsgo_clustered.csv')

    df_clustered.to_csv(path_clusters, index=False)
    
    ### EVALUATE CLUSTERS ###
    index_list = [
        'clim_div',
        'cluster']
    
    data_list = [
        'claytotal_r',
        'om_r',
        'dbthirdbar_r',
        'ph1to1h2o_r']
    
    # create groupby object for summaries
    gb = df_clustered.groupby(index_list)
    
    # standard deviation
    sd = gb.std()[data_list]
    
    # mean
    mean = gb.mean()[data_list]
    
    # 90% CI
    cihi = mean + sd * 1.96
    cilo = mean - sd * 1.96
    ci = cihi - cilo
    
    
    
    print ci
    
    return

def cluster_charts():
    
    ### Plot.ly ###
    # https://plot.ly/python/reference/
    import plotly
    import plotly.graph_objs as go
    
    ### READ-IN DATA ###
    path_soil = opj(
        PATH.projects,
        TASK,
        'soil')

    path_clusters = opj(
        path_soil,
        'statsgo_clustered.csv')

    df_clustered = pd.read_csv(path_clusters)
    
    # create a figure that defines subplots
    fig = plotly.tools.make_subplots(
        rows=2, 
        cols=3, 
        subplot_titles=('clay vs. OM',
                        'clay vs. BD',
                        'clay vs. pH',
                        'OM vs. BD',
                        'OM vs. pH',
                        'BD vs. pH'))
    
    # define the combinations of attributes by subplot
    atts = (
        ('claytotal_r', 'om_r'),
        ('claytotal_r', 'dbthirdbar_r'),
        ('claytotal_r', 'ph1to1h2o_r'),
        ('om_r', 'dbthirdbar_r'),
        ('om_r', 'ph1to1h2o_r'),
        ('dbthirdbar_r', 'ph1to1h2o_r'))

    # loop through climate divisions
    for clim_div in set(df_clustered['clim_div'].tolist()):
        
        print clim_div
        
        # only show legend for first trace
        show_legend=True
        
        # loop through clusters and create a scatter plot
        for cluster in set(df_clustered['cluster'].tolist()):
            
            # subset cluster data
            df_sub = df_clustered.loc[df_clustered['cluster']==cluster]
            
            # define rows and columns
            row = 1
            col = 1
            
            # loop through attributes
            for att in atts:
                
                # create scatter plot trace
                trace = go.Scatter(
                    x = df_sub[att[0]],
                    y = df_sub[att[1]],
                    mode = 'markers',
                    name=cluster,
                    showlegend=show_legend)
                
                # add trace to figure
                fig.append_trace(trace, row, col)
                
                # reset rows and columns
                if col==3:
                    row+=1
                    col=1
                else:
                    col+=1
                
                # only show legend for first trace
                if (row,col) == (1,1):
                    show_legend=False
        
        # set axes titles based on attributes
        fig['layout']['xaxis1'].update(title=atts[0][0])
        fig['layout']['xaxis2'].update(title=atts[1][0])
        fig['layout']['xaxis3'].update(title=atts[2][0])
        fig['layout']['xaxis4'].update(title=atts[3][0])
        fig['layout']['xaxis5'].update(title=atts[4][0])
        fig['layout']['xaxis6'].update(title=atts[5][0])

        fig['layout']['yaxis1'].update(title=atts[0][1])
        fig['layout']['yaxis2'].update(title=atts[1][1])
        fig['layout']['yaxis3'].update(title=atts[2][1])
        fig['layout']['yaxis4'].update(title=atts[3][1])
        fig['layout']['yaxis5'].update(title=atts[4][1])
        fig['layout']['yaxis6'].update(title=atts[5][1])

        # set figure size and title
        fig['layout'].update(
            height=600,
            width=1200, 
            title=clim_div)
        
        # write out to file
        path_chart = opj(
            path_soil,
            clim_div + '_charts.html')
            # 'test_clay_vs_soc.html')
        
        # ...in offline mode
        plotly.offline.plot(
            fig, 
            filename=path_chart,
            auto_open=False)
    
    return
    
if __name__ == '__main__':
    
    # combine()
    
    # select()
    
    cluster()
    
    # cluster_charts()