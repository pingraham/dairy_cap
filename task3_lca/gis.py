#!/usr/bin/env python

import os, sys
from os.path import join as opj
import glob
import ags_network
import pandas as pd
import numpy as np
import fiona
from gispi.raster import clip_by_shp as cxs
from gispi.raster import to_array as t2a

# global variables for network paths
PROJECT = 'dairy_cap'
PATH    = ags_network.paths(PROJECT)

# task name
TASK = 'task3_lca'

# define inputs
SHAPEFILE = 'statsgo_dairy_cap_clipped_intersect_dissolve.shp'
RASTER = 'CDL_2015_merged.tif'
RASTER_RESOLUTION = 30.

def statsgo_cdl():
    
    ### SET PATHS ###
    path_task = opj(
        PATH.projects,
        TASK)
    
    path_gis = opj(
        path_task,
        'gis')
    
    path_cdl = opj(
        path_gis,
        'cdl')
    
    path_scratch = opj(
        PATH.scratch,
        TASK,
        'gis')

    ### READ-IN CDL TABLE ###
    path_ag = opj(
        path_cdl,
        'cdl_ag.csv')
    df_ag = pd.read_csv(path_ag)
    
    ### OPEN SHAPEFILE ###
    shapefile = opj(
        path_gis,
        SHAPEFILE)
    source = fiona.open(shapefile,'r')
    source_driver = source.driver
    source_crs = source.crs
    source_schema = source.schema
    
    ### INITIALIZE TABLE ###
    dfs = []
    
    ### LOOP THROUGH RECORDS ###
    for fnum,f in enumerate(source):

        # get unique keys
        mukey = f['properties']['MUKEY_INT']
        clim_div = f['properties']['CLIM_DIV']
    
        print fnum+1, 'of', len(source), ' - ', mukey, clim_div

        # set path to new shapefile
        shape_name = '_clip_' + SHAPEFILE
        shape_new = opj(
            path_scratch,
            shape_name)
        if os.path.exists(shape_new):
            globber = glob.glob(
                opj(
                    path_scratch, 
                    shape_name.replace('.shp','*')))
            for path_file in globber:
                os.remove(path_file)
        
        # create new shapefile and write feature
        c = fiona.open(
            shape_new,
            'w',
            driver=source_driver,
            crs=source_crs,
            schema=source_schema)
        c.write(f)
        c.close()

        ### EXTRACT TIF SUBSET ###
        # use "clip_by_shp" to extract tif from shapefile
        path_raster = opj(
            path_cdl,
            RASTER)
        path_outraster = os.path.join(
            path_scratch,
            '_clip_' + RASTER)
        if os.path.exists(path_outraster):
            os.remove(path_outraster)
        cxs(path_raster, shape_new, path_outraster)

        ### READ TIF AS ARRAY ###
        ds_a = t2a(path_outraster)
        
        ### COUNT CDL CLASSES ###
        # copy CDL ag data
        df = df_ag.copy(deep=True)
        
        # add index info
        df['mukey']    = mukey
        df['clim_div'] = clim_div
        
        # count pixels by class
        raw_count = np.bincount(np.ndarray.flatten(ds_a))   # get a count of each class value
        len_df = len(df['value'])                           # get the length of all classes
        len_diff = len_df - len(raw_count)                  # get the difference between the two
        diff = np.array([0.] * len_diff)                    # create an empty array to fill the two
        count = np.concatenate((raw_count, diff))           # fill in the gap
        
        # set the count values
        df['count'] = count
        
        # calculate the area
        df['area']  = df['count'] * (RASTER_RESOLUTION**2. / 10000.)
        
        # add to list
        dfs.append(df)

    ### REFORMAT FINAL DF ###
    df = pd.concat(dfs)
    
    # reorder
    df = df[[
        'clim_div',
        'mukey',
        'value',
        'class',
        'ag',
        'count',
        'area']]
    
    # write to file
    path_df = opj(
        path_task,
        'soil',
        'statsgo_by_clim_div_by_cdl_classes.csv')
    df.to_csv(path_df, index=False)
    
    ### CALCULATE TOTAL AG AREA
    index_fields = [
        'clim_div',
        'mukey',
        'ag']
    df = df.groupby(index_fields, as_index=False).sum()
    
    # get ag rows
    df_ag = df.loc[df['ag']==1].copy(deep=True)
    df_ag.set_index(['clim_div','mukey'], inplace=True)
    
    # get not ag rows
    df_notag = df.loc[df['ag']==0].copy(deep=True)
    df_notag.set_index(['clim_div','mukey'], inplace=True)

    # calculate percent ag
    df_ag['area_total'] = df_notag['area'] + df_ag['area']
    df_ag['ag_pct'] = df_ag['count'] / (df_notag['count'] + df_ag['count'])
    del df_ag['value']
    
    # write to file
    path_df = opj(
        path_task,
        'soil',
        'statsgo_by_clim_div_by_cdl_ag.csv')
    df_ag.to_csv(path_df, index=True)
    
    return

if __name__ == '__main__':

    statsgo_cdl()
