
import os, sys
import shutil

path = '/projects/dairy_cap/task1a_marshfield/calibration/results/Batch'

dirs = os.listdir(path)

for dir in dirs:
    
    site = dir.split('-')[2]
    site=int(site)
    if site%5 == 0:

        path_dir = os.path.join(path, dir)
        shutil.rmtree(path_dir)
        print path_dir