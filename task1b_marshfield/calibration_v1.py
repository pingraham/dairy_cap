
import os, sys
from os.path import join as opj
import convert
import glob
import pandas as pd
import numpy as np
import datetime as dt
import ags_network

# global variables for network paths
PROJECT = 'dairy_cap'
PATH    = ags_network.paths(PROJECT)

# database name
DATABASE = 'database.xlsx'

# task name
TASK = 'task1a_marshfield'

def tdd():

    ''' GET WEATHER '''
    # read in weather spreadsheet as DF
    path_xlsx = opj(
        PATH.projects,
        TASK,
        'management',
        'marshfield_weather.xlsx')
    df_weather = pd.read_excel(
        path_xlsx,
        sheetname='marshfield2008')

    # calculate date
    df_weather['date'] = pd.to_datetime(
        df_weather['year'] * 10000 + df_weather['month'] * 100 + df_weather['daymonth'],
        format='%Y%m%d')

    # calculate mean T
    df_weather['mean'] = (df_weather['max (C) '] + df_weather['min (C) ']) / 2.0

    # calculate DD
    df_weather['dd'] = 0.
    df_weather.loc[df_weather['mean'] > 0., 'dd'] = df_weather['mean']

    ''' GET MANAGEMENT DATA '''
    # read-in management data
    path_db = os.path.join(
        PATH.projects,
        TASK,
        'calibration',
        DATABASE)
    df_field = pd.read_excel(path_db, sheetname='field', skiprows=[0])

    # keep key fields
    df_field = df_field[[
        'scenario',
        'scenario_name',
        'year_cal',
        'crop_year',
        'crop_1_name',
        'crop_1_plant_date',
        'crop_1_harv_date',
        'cut_1_date',
        'cut_2_date',
        'cut_3_date']]

    # initialize list of lists
    lol = [[
        'scenario',
        'scenario_name',
        'crop',
        'year',
        'date_plant',
        'date_harvest',
        'tdd']]

    # loop through management records and relate to weather
    for mgt_rec, data in df_field.iterrows():
        scenario            = data['scenario']
        scenario_name       = data['scenario_name']
        year                = data['year_cal']
        crop_year           = data['crop_year']
        crop_1_name         = data['crop_1_name']
        crop_1_plant_date   = data['crop_1_plant_date']
        crop_1_harv_date    = data['crop_1_harv_date']
        cut_1_date          = data['cut_1_date']
        cut_2_date          = data['cut_2_date']
        cut_3_date          = data['cut_3_date']

        # handle alfalfa
        if crop_1_name == 'fallow' or crop_1_name == 'alfalfa':
            crop_1_plant_date = dt.date(year, 5, 16)    # based on observed simulated start-of-growth
            crop_1_harv_date = cut_3_date

        df_season = df_weather.loc[
            (df_weather['date'] >=  crop_1_plant_date) &
            (df_weather['date'] <=  crop_1_harv_date)
                ].copy(deep=True)

        tdd = df_season['dd'].sum()

        # record data
        lol.append([scenario, scenario_name, crop_year, year, crop_1_plant_date, crop_1_harv_date, tdd])

    # tie 'em together
    df = pd.DataFrame(lol[1:], columns=lol[0])

    # write it out
    path_tdd = opj(
        PATH.projects,
        TASK,
        'calibration',
        'results',
        'tdd.csv')
    df.to_csv(path_tdd, index=False)

    # get minimum TDD by system and crop
    index_fields = [
        # 'scenario',
        # 'scenario_name',
        'crop']

    df_min = df.groupby(index_fields).min()[['tdd']]

    path_min = opj(
        PATH.projects,
        TASK,
        'calibration',
        'results',
        'tdd_min.csv')
    df_min.to_csv(path_min)

    return

def annual_yield():

    ''' SET PATHS '''
    path_db = opj(
        PATH.projects,
        TASK,
        'calibration',
        DATABASE)

    path_results = opj(
        PATH.projects,
        TASK,
        'calibration',
        'results')

    path_batch = opj(
        path_results,
        'Batch')

    ''' READ IN DATABASE '''
    data_dict = {}
    sheets = [
        'field',
        'scenario']
    for sheet in sheets:
        data_dict[sheet] = pd.read_excel(
            path_db,
            sheetname=sheet,
            skiprows=[0])
    df_field = data_dict['field']
    df_scen = data_dict['scenario']

    # replace "na" values in yield
    df_field['crop_1_yield'].replace(to_replace="na", value=np.nan, inplace=True)

    ''' READ-IN RESULTS '''
    path_glob = opj(
        path_batch,
        'Case*',
        'Multi_year_summary.csv')
    globber = glob.glob(path_glob)

    results = []
    for path_file in globber:

        # get the directory name
        path_dir,filename = os.path.split(path_file)
        dirname = os.path.basename(path_dir)

        case, scen, site = dirname.split('-')
        scen, site = int(scen), int(site)

        # extract results as DataFrame
        df = pd.read_csv(
            path_file,
            skiprows=[0,1,3,4])
        df.columns = [c.strip() for c in df.columns]

        # replace bad values in stress fields
        # stress_fields = ['WaterStress', 'N_Stress']
        # for c in stress_fields:
            # df[c].replace(to_replace="  -nan(ind)", value=np.nan, inplace=True)
            # df[c] = df[c].astype(float)

        # cut out init. years
        year_cal_start = df_scen.loc[df_scen['scenario']==scen]['year_cal_start'].iloc[0]
        year_cal_end   = df_scen.loc[df_scen['scenario']==scen]['year_cal_end'].iloc[0]

        years_summary = year_cal_end - year_cal_start + 1
        df = df.iloc[years_summary:]
        df.reset_index(inplace=True)

        df['scen'] = scen
        df['site'] = site

        # extract database field subset
        df_subset = df_field.loc[df_field['scenario'] == scen].copy(deep=True)
        # df_subset.index = [i for i in range(years_summary, len(df))]

        subset_fields = [
            'scenario',
            'year_sim',
            'year_cal',
            'crop_year',
            'crop_1_yield',
            'crop_1_yield_c',
            'crop_1_residue']

        # join field info to results
        df = df.join(
            df_subset[subset_fields].set_index('year_sim'),
            on='Year')

        # append DF to list
        results.append(df)

    ''' CONCATENATE RESULTS '''
    df = pd.concat(results)
    del df['Year']
    df.reset_index(inplace=True)

    ''' SET-UP YIELD '''
    # create a measured yield field
    df['yield_meas'] = df['crop_1_yield_c']

    # create a simulated yield field
    df['yield_sim'] = df['GrainC1']

    # set gain corn yield
    df.loc[
        (df['Crop1'] == 0) &
        (df['year_cal'] == 2010),
            'yield_sim'] = df['GrainC2']

    # set silage corn yield
    df.loc[
        (df['Crop1'] == 1) &
        (df['year_cal'] != 2010),
            'yield_sim'] = df['GrainC1'] + ((df['LeafC1'] + df['StemC1']) * (1. - df['crop_1_residue']))

    # set alfalfa yield to "Cut_CropC"
    df.loc[
        df['crop_year'] == 'alfalfa',
            'yield_sim'] = df['Cut_CropC']

    ''' MAKE IT PRETTY '''
    df.sort(['scen', 'site', 'year_cal'], inplace=True)

    # write it out
    path_file = os.path.join(
        path_results,
        'annual_yield.csv')
    df.to_csv(path_file, index=False)

    ''' CALCULATE RELATIVE RMSE '''
    df['sq'] = (df['yield_meas'] - df['yield_sim'])**2.

    df_ss = df.groupby(['scen', 'site','crop_year']).mean()[['yield_meas', 'sq']]
    df_ss['rmse'] = df_ss['sq']**0.5 # square root
    df_ss['rmse_rel'] = df_ss['rmse'] / df_ss['yield_meas']

    # df_corr = df.groupby(['scen', 'site','crop_year'])[['yield_meas', 'yield_sim']].corr()
    # df_corr = df_corr.loc[df_corr['yield_meas'] != 1.0000]

    # df_corr = df_corr[['yield_meas']]#.reset_index(inplace=True)
    # df_corr.reset_index(inplace=True)
    # del df_corr['level_3']
    # df_corr.set_index(['scen', 'site', 'crop_year'], inplace=True)

    # df_corr.columns = ['r']

    # df_ss = df_ss.join(
        # df_corr)

    df_ss = df_ss[['yield_meas', 'rmse', 'rmse_rel']]#, 'r']]

    # write out
    path_rmse = opj(
        path_results,
        'rmse_rel.csv')

    df_ss.to_csv(path_rmse)

    return df

def graph_measured_yield():

    ### Plot.ly ###
    # https://plot.ly/python/reference/
    import plotly
    import plotly.graph_objs as go


    ''' SET PATHS '''
    path_db = opj(
        PATH.projects,
        TASK,
        'calibration',
        DATABASE)

    ''' READ IN DATABASE '''
    df_field = pd.read_excel(
            path_db,
            sheetname='field',
            skiprows=[0])

    for crop in ['alfalfa', 'corn']:

        traces = []
        for scenario in set(df_field['scenario_name'].tolist()):

            df_scen = df_field.loc[df_field['scenario_name']==scenario].copy(deep=True)

            df_scen.loc[df_scen['crop_year']!=crop, 'crop_1_yield_c'] = np.nan

            df_scen = df_scen[['year_cal', 'crop_1_yield_c']]

            trace = go.Scatter(
                x = df_scen['year_cal'],
                y = df_scen['crop_1_yield_c'],
                name = scenario,
                connectgaps=True)
                # line = dict(
                    # color = ('rgb(205, 12, 24)'),
                    # width = 2))

            traces.append(trace)

        # Edit the layout
        layout = dict(title = 'measured yield for ' + crop,
                      xaxis = dict(title = 'year'),
                      yaxis = dict(title = 'yield (kgC/ha)'),
                      )

        # Plot and embed in ipython notebook!
        fig = dict(data=traces, layout=layout)

        path_chart = opj(
            PATH.projects,
            TASK,
            'calibration',
            'results',
            'chart_yield_measured_' + crop + '.html')

        # ...in offline mode
        plotly.offline.plot(
            fig,
            filename=path_chart,
            auto_open=False)

    return

def graph_simulated_yield():

    ### Plot.ly ###
    # https://plot.ly/python/reference/
    import plotly
    import plotly.graph_objs as go


    ''' SET PATHS '''
    path_cal = opj(
        PATH.projects,
        TASK,
        'calibration')

    ''' READ IN ANNUAL YIELD '''
    path_yield = opj(
        path_cal,
        'results',
        'annual_yield.csv')
    df_yield = pd.read_csv(path_yield)

    for crop in ['alfalfa', 'corn']:

        traces = []
        for scenario in set(df_field['scenario_name'].tolist()):

            df_scen = df_field.loc[df_field['scenario_name']==scenario].copy(deep=True)

            df_scen.loc[df_scen['crop_year']!=crop, 'crop_1_yield_c'] = np.nan

            df_scen = df_scen[['year_cal', 'crop_1_yield_c']]

            trace = go.Scatter(
                x = df_scen['year_cal'],
                y = df_scen['crop_1_yield_c'],
                name = scenario,
                connectgaps=True)
                # line = dict(
                    # color = ('rgb(205, 12, 24)'),
                    # width = 2))

            traces.append(trace)

        # Edit the layout
        layout = dict(title = 'measured yield for ' + crop,
                      xaxis = dict(title = 'year'),
                      yaxis = dict(title = 'yield (kgC/ha)'),
                      )

        # Plot and embed in ipython notebook!
        fig = dict(data=traces, layout=layout)

        path_chart = opj(
            PATH.projects,
            TASK,
            'calibration',
            'results',
            'chart_yield_measured_' + crop + '.html')

        # ...in offline mode
        plotly.offline.plot(
            fig,
            filename=path_chart,
            auto_open=False)

    return

if __name__ == '__main__':

    # print 'TDD stats'
    # tdd()

    print 'get annual yield'
    annual_yield()

    # print 'graph measured yield'
    # graph_measured_yield()

    # print 'graph measured vs. simulated'
    # graph_simulated_yield()