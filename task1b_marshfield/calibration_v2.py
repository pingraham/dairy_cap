
import os, sys
from os.path import join as opj
import convert
import glob
import pandas as pd
import numpy as np
import datetime as dt
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import ags_network

# global variables for network paths
PROJECT = 'dairy_cap'
PATH    = ags_network.paths(PROJECT)

# database name
DATABASE = 'database.xlsx'

# task name
TASK = 'task1a_marshfield'

def tdd():

    ''' GET WEATHER '''
    # read in weather spreadsheet as DF
    path_xlsx = opj(
        PATH.projects,
        TASK,
        'management',
        'marshfield_weather.xlsx')
    df_weather = pd.read_excel(
        path_xlsx,
        sheetname='marshfield2008')

    # calculate date
    df_weather['date'] = pd.to_datetime(
        df_weather['year'] * 10000 + df_weather['month'] * 100 + df_weather['daymonth'],
        format='%Y%m%d')

    # calculate mean T
    df_weather['mean'] = (df_weather['max (C) '] + df_weather['min (C) ']) / 2.0

    # calculate DD
    df_weather['dd'] = 0.
    df_weather.loc[df_weather['mean'] > 0., 'dd'] = df_weather['mean']

    ''' GET MANAGEMENT DATA '''
    # read-in management data
    path_db = os.path.join(
        PATH.projects,
        TASK,
        'calibration',
        DATABASE)
    df_field = pd.read_excel(path_db, sheetname='field', skiprows=[0])

    # keep key fields
    df_field = df_field[[
        'scenario',
        'scenario_name',
        'year_cal',
        'crop_year',
        'crop_1_name',
        'crop_1_plant_date',
        'crop_1_harv_date',
        'cut_1_date',
        'cut_2_date',
        'cut_3_date']]

    # initialize list of lists
    lol = [[
        'scenario',
        'scenario_name',
        'crop',
        'year',
        'date_plant',
        'date_harvest',
        'tdd']]

    # loop through management records and relate to weather
    for mgt_rec, data in df_field.iterrows():
        scenario            = data['scenario']
        scenario_name       = data['scenario_name']
        year                = data['year_cal']
        crop_year           = data['crop_year']
        crop_1_name         = data['crop_1_name']
        crop_1_plant_date   = data['crop_1_plant_date']
        crop_1_harv_date    = data['crop_1_harv_date']
        cut_1_date          = data['cut_1_date']
        cut_2_date          = data['cut_2_date']
        cut_3_date          = data['cut_3_date']

        # handle alfalfa
        if crop_1_name == 'fallow' or crop_1_name == 'alfalfa':
            crop_1_plant_date = dt.date(year, 5, 16)    # based on observed simulated start-of-growth
            crop_1_harv_date = cut_3_date

        df_season = df_weather.loc[
            (df_weather['date'] >=  crop_1_plant_date) &
            (df_weather['date'] <=  crop_1_harv_date)
                ].copy(deep=True)

        tdd = df_season['dd'].sum()

        # record data
        lol.append([scenario, scenario_name, crop_year, year, crop_1_plant_date, crop_1_harv_date, tdd])

    # tie 'em together
    df = pd.DataFrame(lol[1:], columns=lol[0])

    # write it out
    path_tdd = opj(
        PATH.projects,
        TASK,
        'calibration',
        'results',
        'tdd.csv')
    df.to_csv(path_tdd, index=False)

    # get minimum TDD by system and crop
    index_fields = [
        # 'scenario',
        # 'scenario_name',
        'crop']

    df_min = df.groupby(index_fields).min()[['tdd']]

    path_min = opj(
        PATH.projects,
        TASK,
        'calibration',
        'results',
        'tdd_min.csv')
    df_min.to_csv(path_min)

    return

def annual_yield():

    ''' SET PATHS '''
    path_db = opj(
        PATH.projects,
        TASK,
        'calibration',
        DATABASE)

    path_results = opj(
        PATH.projects,
        TASK,
        'calibration',
        'results')

    path_batch = opj(
        path_results,
        'Batch')

    ''' READ IN DATABASE '''
    data_dict = {}
    sheets = [
        'field',
        'scenario']
    for sheet in sheets:
        data_dict[sheet] = pd.read_excel(
            path_db,
            sheetname=sheet,
            skiprows=[0])
    df_field = data_dict['field']
    df_scen = data_dict['scenario']

    # replace "na" values in yield
    df_field['crop_1_yield'].replace(to_replace="na", value=np.nan, inplace=True)

    ''' READ-IN RESULTS '''
    path_glob = opj(
        path_batch,
        'Case*',
        'Multi_year_summary.csv')
    globber = glob.glob(path_glob)

    results = []
    for path_file in globber:

        # get the directory name
        path_dir,filename = os.path.split(path_file)
        dirname = os.path.basename(path_dir)

        case, scen, site = dirname.split('-')
        scen, site = int(scen), int(site)

        # extract results as DataFrame
        df = pd.read_csv(
            path_file,
            skiprows=[0,1,3,4])
        df.columns = [c.strip() for c in df.columns]

        # replace bad values in stress fields
        # stress_fields = ['WaterStress', 'N_Stress']
        # for c in stress_fields:
            # df[c].replace(to_replace="  -nan(ind)", value=np.nan, inplace=True)
            # df[c] = df[c].astype(float)

        # cut out init. years
        year_cal_start = df_scen.loc[df_scen['scenario']==scen]['year_cal_start'].iloc[0]
        year_cal_end   = df_scen.loc[df_scen['scenario']==scen]['year_cal_end'].iloc[0]

        years_summary = year_cal_end - year_cal_start + 1
        df = df.iloc[years_summary:]
        df.reset_index(inplace=True)

        df['scen'] = scen
        df['scen_name'] = df_scen.loc[df_scen['scenario']==scen]['name'].iloc[0]
        df['site'] = site

        # extract database field subset
        df_subset = df_field.loc[df_field['scenario'] == scen].copy(deep=True)
        # df_subset.index = [i for i in range(years_summary, len(df))]

        subset_fields = [
            'scenario',
            'year_sim',
            'year_cal',
            'crop_year',
            'crop_1_yield',
            'crop_1_yield_c',
            'crop_1_residue']

        # join field info to results
        df = df.join(
            df_subset[subset_fields].set_index('year_sim'),
            on='Year')

        # append DF to list
        results.append(df)

    ''' CONCATENATE RESULTS '''
    df = pd.concat(results)
    del df['Year']
    df.reset_index(inplace=True)

    ''' SET-UP YIELD '''
    # create a measured yield field
    df['yield_meas'] = df['crop_1_yield_c']

    # create a simulated yield field
    df['yield_sim'] = df['GrainC1']

    # set gain corn yield
    df.loc[
        (df['Crop1'] == 0) &
        (df['year_cal'] == 2010),
            'yield_sim'] = df['GrainC2']

    # set silage corn yield
    df.loc[
        (df['Crop1'] == 1) &
        (df['year_cal'] != 2010),
            'yield_sim'] = df['GrainC1'] + ((df['LeafC1'] + df['StemC1']) * (1. - df['crop_1_residue']))

    # set alfalfa yield to "Cut_CropC"
    df.loc[
        df['crop_year'] == 'alfalfa',
            'yield_sim'] = df['Cut_CropC']

    ''' MAKE IT PRETTY '''
    df.sort(['scen', 'site', 'year_cal'], inplace=True)

    # write it out
    path_file = os.path.join(
        path_results,
        'annual_yield.csv')
    df.to_csv(path_file, index=False)

    ''' CALCULATE RELATIVE RMSE '''
    df['sq'] = (df['yield_meas'] - df['yield_sim'])**2.

    df_ss = df.groupby(['scen', 'site','crop_year']).mean()[['yield_meas', 'sq']]
    df_ss['rmse'] = df_ss['sq']**0.5 # square root
    df_ss['rmse_rel'] = df_ss['rmse'] / df_ss['yield_meas']

    # df_corr = df.groupby(['scen', 'site','crop_year'])[['yield_meas', 'yield_sim']].corr()
    # df_corr = df_corr.loc[df_corr['yield_meas'] != 1.0000]

    # df_corr = df_corr[['yield_meas']]#.reset_index(inplace=True)
    # df_corr.reset_index(inplace=True)
    # del df_corr['level_3']
    # df_corr.set_index(['scen', 'site', 'crop_year'], inplace=True)

    # df_corr.columns = ['r']

    # df_ss = df_ss.join(
        # df_corr)

    df_ss = df_ss[['yield_meas', 'rmse', 'rmse_rel']]#, 'r']]

    # write out
    path_rmse = opj(
        path_results,
        'rmse_rel.csv')

    df_ss.to_csv(path_rmse)

    return df

def graph_measured_yield():

    ### IMPORT LIBRARIES ###
    from graph import basic_timeline
    
    ### SET PATHS ###
    path_cal = opj(
        PATH.projects,
        TASK,
        'calibration')
    path_db = opj(
        path_cal,
        DATABASE)

    ### READ IN DATABASE ###
    df_field = pd.read_excel(
            path_db,
            sheetname='field',
            skiprows=[0])

    for crop in ['alfalfa', 'corn']:

        y = {}
        for scenario in set(df_field['scenario_name']):
            df_scen = df_field.loc[df_field['scenario_name']==scenario].copy(deep=True)
            df_scen.reset_index(inplace=True)
            df_scen.loc[df_scen['crop_year']!=crop, 'crop_1_yield_c'] = np.nan
            y[scenario] = df_scen['crop_1_yield_c']

        basic_timeline(
            x = df_scen['year_cal'],
            y = y,
            path_export = opj(
                path_cal, 
                'results', 
                'charts',
                'measured_yield_' + crop + '.png'),
            title='measured yield for ' + crop, 
            xlabel='year',
            ylabel='yield (kgC/ha/y)',
            markers=True)

    return

def graph_simulated_yield():

    ### IMPORT LIBRARIES ###
    from graph import basic_timeline
    
    ### SET PATHS ###
    path_cal = opj(
        PATH.projects,
        TASK,
        'calibration')
    path_results = opj(
        path_cal,
        'results')

    ### READ IN YIELD DATA ###
    path_yield = opj(
        path_results,
        'annual_yield.csv')
    df_yield = pd.read_csv(path_yield)

    for crop in ['alfalfa', 'corn']:

        for scenario in set(df_yield['scen_name']):

            df_scen = df_yield.loc[df_yield['scen_name']==scenario].copy(deep=True)
            df_scen.loc[df_scen['crop_year']!=crop, 'yield_meas'] = np.nan
            df_scen.loc[df_scen['crop_year']!=crop, 'yield_sim'] = np.nan

            y = {}
            y['measured'] = df_scen['yield_meas']
            y['simulated'] = df_scen['yield_sim']
            
            basic_timeline(
                x = df_scen['year_cal'],
                y = y,
                path_export = opj(
                    path_cal, 
                    'results', 
                    'charts',
                    'measured_vs_simulated_' + crop + '_' + scenario + '.png'),
                title='measured vs. simulated yield for ' + crop + ', ' + scenario + ' treatment', 
                xlabel='year',
                ylabel='yield (kgC/ha/y)',
                markers=True)
            
            # scatter plot
            plt.scatter(
                df_scen['yield_sim'],
                df_scen['yield_meas'])
            plt.grid()
            plt.tight_layout()
            plt.savefig(
                opj(
                    path_results,
                    'charts',
                    'measured_vs_simulated_scatter_' + crop + '_' + scenario + '.png'))
            plt.close()

    return

def combine_meas_sim():

    ### IMPORT LIBRARIES ###
    from collections import OrderedDict as od

    ### SET PATHS ###
    path_cal = opj(
        PATH.projects,
        TASK,
        'calibration')
    path_mgt = opj(
        PATH.projects,
        TASK,
        'management')
    path_results = opj(
        path_cal,
        'results')

    ### READ-IN MEASUREMENT DATA ###
    path_meas = opj(
        path_mgt,
        'Marshfield_observations_1.1.xlsx')
    df_meas = pd.read_excel(
        path_meas,
        sheetname='daily obs final')
    df_meas['treatment'] = df_meas['treatment'].str.lower()
    
    attributes = od()
    attributes['n2o']        = 'n2o (g N/ha/day)'
    attributes['soil_temp']  = 'Soil Temp (C) 5cm'
    attributes['soil_moist'] = 'Soil Moisture (%) 5cm'
    attributes['ch4']        = 'ch4 (gC/ha/day)'
    attributes['co2']        = 'co2 (gC/ha/day)'
    attributes['nh4']        = 'nh4 (mgN/kg soil)'
    attributes['no3']        = 'no3 (mgN/kg soil)'
    
    for att in attributes:
        meas_field = attributes[att]
        df_meas[att + '_meas'] = df_meas[meas_field]
        del df_meas[meas_field]

    # get scenario info
    scenarios = od()
    scenarios[1] = 'cont'
    scenarios[2] = '120n'
    scenarios[3] = 'brsf'
    scenarios[4] = 'bric'

    years = od()
    years[14] = 2013
    years[15] = 2014
    
    dfs = []
    
    # for site in range(1,10+1):
    for site in [1]:
        for scenario in scenarios:

            scen_name = scenarios[scenario]
            path_case = glob.glob(
                opj(
                    path_results, 
                    'Batch', 
                    'Case*-' + str(scenario) + '-' + str(site)))[0]
            
            for year in years:
                
                ### SOIL N ###
                path_n = opj(
                    path_case,
                    'Day_SoilN_' + str(year) + '.csv')
                
                df_n = pd.read_csv(
                    path_n,
                    skiprows=[0,1,3,4])
                df_n.set_index('Day', inplace=True)
                
                df_n = df_n[[
                    'NH4+',
                    'NO3-',
                    'N2O-flux']]
                
                ### SOIL C ###
                path_c = opj(
                    path_case,
                    'Day_SoilC_' + str(year) + '.csv')
                
                df_c = pd.read_csv(
                    path_c,
                    skiprows=[0])

                df_c.set_index('Day', inplace=True)
                
                df_c = df_c[[
                    'Eco-respiration',
                    'CH4-flux']]
                
                ### SOIL WEATHER ###
                path_w = opj(
                    path_case,
                    'Day_SoilClimate_' + str(year) + '.csv')
                
                df_w = pd.read_csv(
                    path_w,
                    skiprows=[0, 1, 3])
                df_w.set_index('Day', inplace=True)

                df_w['bd'] = (df_w['1cm.5'] + df_w['10cm.5']) / 2.0

                df_w = df_w[[
                    'bd',
                    '5cm',         # soil temp
                    '5cm.1']]      # soil moisture, wfps

                df_w.columns = ['bd','soil_temp','soil_moist']
                
                ### COMBINE ###
                df = pd.concat([df_n, df_c, df_w], axis=1)

                ### ADD OTHER INFO ###
                df['year'] = years[year]
                df['date'] = df['year'] * 1000 + df.index
                df['date'] = pd.to_datetime(df['date'], format='%Y%j')
                
                df['scenario'] = scenario
                df['scen_name'] = scen_name
                
                df['site'] = site
                
                ### FORMAT ###
                df.reset_index(inplace=True)

                df.columns = [
                    'doy',
                    'nh4_sim',
                    'no3_sim',
                    'n2o_sim',
                    'co2_sim',
                    'ch4_sim',
                    'bd_sim',
                    'soil_temp_sim',
                    'soil_moist_sim',
                    'year',
                    'date',
                    'scenario',
                    'scen_name',
                    'site']
                
                df = df[[
                    'scenario',
                    'scen_name',
                    'site',
                    'year',
                    'date',
                    'doy',
                    'bd_sim',
                    'n2o_sim',
                    'soil_temp_sim',
                    'soil_moist_sim',
                    'co2_sim',
                    'ch4_sim',
                    'nh4_sim',
                    'no3_sim']]

                ### APPEND ###
                dfs.append(df)
            
    ### CONCATENATE ###
    df = pd.concat(dfs)

    ### JOIN MEASURED ###
    df = df.join(
        df_meas.set_index(['treatment','date']),
        on=['scen_name','date'])
    
    
    ### CONVERT MEASURED UNITS ###
    
    # N2O: gN/ha to kgN/ha
    df['n2o_meas'] = df['n2o_meas'] * 1000.**-1.
    
    # soil moisture: VWC% to WFPS - using BD
    df['soil_moist_meas'] = (df['soil_moist_meas'] / 100.) / (1.-(df['bd_sim']/2.65))
    
    # soil moisture: VWC% to WFPS - using porosity
    # df['soil_moist_meas'] = (df['soil_moist_meas'] / 100.) / 0.50487

    # CO2: gC/ha to kgC/ha
    df['co2_meas'] = df['co2_meas'] * 1000.**-1.
        
    # CH4: gC/ha to kgC/ha
    df['ch4_meas'] = df['ch4_meas'] * 1000.**-1.
    
    # NH4: mgN/kg soil to kgN/ha
    df['soil_weight'] = df['bd_sim'] * (5.*10.**8.) * 1000.**-1. # bd (g/cm3) * depth (cm) * cm2/ha * kg/g = kg/ha
    df['nh4_meas'] = (df['nh4_meas'] * 10.**6.) / df['soil_weight']
    
    # NO3: mgN/kg soil to kgN/ha
    df['no3_meas'] = (df['no3_meas'] * 10.**6.) / df['soil_weight']
    del df['soil_weight']
    
    # export
    path_df = opj(
        path_results,
        'measured_vs_simulated.csv')
    df.to_csv(path_df, index=False)
    
    return

def graph_daily_meas_sim():
    
    ### SET PATHS ###
    path_cal = opj(
        PATH.projects,
        TASK,
        'calibration')
    path_mgt = opj(
        PATH.projects,
        TASK,
        'management')
    path_results = opj(
        path_cal,
        'results')
    path_charts = opj(
        path_results,
        'charts')
    
    ### READ-IN MEASURED VS. SIMULATED ###
    path_mvs = opj(
        path_results,
        'measured_vs_simulated.csv')
    df_mvs = pd.read_csv(path_mvs)

    df_mvs['date'] = pd.to_datetime(df_mvs['date'])

    ### LOOP THROUGH SITES (SOILS) ###
    # for site in range(1, 10+1):
    for site in [1]:

        ### LOOP THROUGH SCENARIOS
        for scen in df_mvs['scenario'].drop_duplicates():

            df_grapher = df_mvs.loc[
                (df_mvs['site'] == site) & 
                (df_mvs['scenario'] == scen)
                    ].copy(deep=True)
            
            scen_name = df_grapher['scen_name'].iloc[0]

            ### LOOP THROUGH ATTRIBUTES ###
            
            atts = {
                'n2o':        'kgN/ha/d',
                'soil_temp':  'deg. C',
                'soil_moist': 'WFPS',
                'co2':        'kgC/ha/d',     
                'ch4':        'kgC/ha/d',
                'nh4':        'kgN/ha/d',
                'no3':        'kgN/ha/d'}
            
            for att in atts:
                
                print '    ', site, scen, att
                
                units = atts[att]

                att_sim = att + '_sim'
                att_meas = att + '_meas'
                
                mean = pd.rolling_mean(df_grapher[att_sim], 7)
                sd   = pd.rolling_std(df_grapher[att_sim], 7)
                low  = mean - sd * 1.96
                high = mean + sd * 1.96

                # plot measured daily data
                plt.plot(
                    df_grapher['date'], 
                    df_grapher[att_meas],
                    linestyle='None',
                    marker='o',
                    color='orange',
                    markersize=5)
                
                # plot simulated raw daily data
                plt.plot(
                    df_grapher['date'], 
                    df_grapher[att_sim],
                    color='darkgray')
                
                # plot simulated 7-day mean
                plt.plot(
                    df_grapher['date'], 
                    mean,
                    color='black')

                # plot simulated 7-day low
                plt.plot(
                    df_grapher['date'], 
                    low,
                    color='grey')
                
                # plot simulated 7-day high
                plt.plot(
                    df_grapher['date'], 
                    high,
                    color='grey')
                
                plt.fill_between(df_grapher['date'].tolist(), high, low, color='grey')

                # title
                plt.title(
                    att + 
                    ', scen: ' + scen_name + 
                    ' (' + str(scen) + ')' + 
                    ', site: ' + str(site))

                # Y-axis
                plt.ylabel(units)
                # plt.ylim(0.0, plt.ylim()[1])

                # grid
                plt.grid()

                # tight layout
                plt.tight_layout()
                
                # export
                png_name = (
                    'mvs_' + 
                    att + '_' + 
                    str(scen) + '_' + 
                    scen_name + '_' +
                    str(site) + 
                    '.png')
                plt.savefig(opj(path_charts, png_name))

                # close
                plt.close()
        
    return

def graph_annual_meas_sim():
    
    ### SET PATHS ###
    path_cal = opj(
        PATH.projects,
        TASK,
        'calibration')
    path_mgt = opj(
        PATH.projects,
        TASK,
        'management')
    path_results = opj(
        path_cal,
        'results')
    path_charts = opj(
        path_results,
        'charts')
    
    ### READ-IN MEASURED VS. SIMULATED ###
    path_mvs = opj(
        path_results,
        'measured_vs_simulated.csv')
    df_mvs = pd.read_csv(path_mvs)

    df_mvs['date'] = pd.to_datetime(df_mvs['date'])
    
    ### CALCULATE ANNUAL VALUES BASED ON MEASURED PERIOD ###
    
    
    return

if __name__ == '__main__':

    # print 'TDD stats'
    # tdd()

    # print 'get annual yield'
    # annual_yield()

    # print 'graph measured yield'
    # graph_measured_yield()

    # print 'graph measured vs. simulated yield'
    # graph_simulated_yield()
    
    print 'combine measured and simulated'
    combine_meas_sim()
    
    print 'graph daily soil measurements'
    graph_daily_meas_sim()
    
    print 'graph annual soil measurements'
    graph_annual_meas_sim()