
import os, sys
from os.path import join as opj
import convert
import glob
import math
import pandas as pd
import numpy as np
import datetime as dt
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import ags_network

# global variables for network paths
PROJECT = 'dairy_cap'
PATH    = ags_network.paths(PROJECT)

# database name
DATABASE = 'database.xlsx'

# task name
TASK = 'task1a_marshfield'

### SET PATHS ###
path_cal = opj(
    PATH.projects,
    TASK,
    'calibration')
path_mgt = opj(
    PATH.projects,
    TASK,
    'management')
path_results = opj(
    path_cal,
    'results')
path_charts = opj(
    path_results,
    'charts')

### READ-IN MEASURED VS. SIMULATED ###
path_mvs = opj(
    path_results,
    'measured_vs_simulated.csv')
df = pd.read_csv(path_mvs)

df['date'] = pd.to_datetime(df['date'])

# dates where measurements were made 
dr2013 = pd.date_range(dt.date(2013,6,13), dt.date(2013,11,15))
dr2014 = pd.date_range(dt.date(2014,4,15), dt.date(2014,12,15))
df['inseason'] = False
df.loc[df['date'].isin(dr2013), 'inseason'] = True
df.loc[df['date'].isin(dr2014), 'inseason'] = True

### GAP-FILL CO2 AND CH4 WITH LINEAR INTERPOLATION ###
print '    gap-filling C...'
df.loc[df['inseason'], 'co2_est'] = df['co2_meas'].interpolate()
df.loc[df['inseason'], 'ch4_est'] = df['ch4_meas'].interpolate()

### GAP-FILL MEASURED N2O WITH ATTENUATION ###
print '    gap-filling N...'

# initialize day-after-measurement
df['dam'] = np.nan

# initialize estimated N2O column
df['n2o_est'] = np.nan

# initialize estimate N2O variable
n2o_est = 0.

# loop through rows
i_n2o_meas = df.columns.tolist().index('n2o_meas') + 1

for t in df.loc[df['inseason']].itertuples():

    # extract measured N2O
    i = t[0]
    n2o_meas = t[i_n2o_meas]

    print i, n2o_meas, n2o_est
    
    # if N2O was not measured on this day...
    if np.isnan(n2o_meas):

        # ...calculate estimated N2O based on 
        # prior day estimated (or actual) N2O
        n2o_est = math.exp(1.)**-0.223 * n2o_est
        # incidentially this is an exponential decay
        # function based on data from Michel Cavigelli

    # if N2O was measured on this day...
    else:

        # set estimated N2O to measured N2O
        n2o_est = n2o_meas

    # populate DF with estimated value
    df.loc[i, 'n2o_est'] = n2o_est

print '    done.'