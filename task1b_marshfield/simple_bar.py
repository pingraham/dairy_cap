
import os, sys
from os.path import join as opj
import pandas as pd
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

df = pd.DataFrame([])

year    = [  2013,   2014,   2013,   2014,   2013,   2014,   2013,   2014]
n2o_sim = [   0.1,   0.15,    0.1,   0.15,    0.1,   0.15,    0.1,   0.15]
n2o_est = [  0.15,    0.2,   0.15,    0.2,   0.15,    0.2,   0.15,    0.2]
scen    = ['base', 'base', 'alt1', 'alt1', 'alt2', 'alt2', 'alt3', 'alt3']

df['scen']    = scen
df['year']    = year
df['n2o_sim'] = n2o_sim
df['n2o_est'] = n2o_est


fig = plt.figure()
ind = np.arange(2)
width = 0.35

for scen_num, scen in enumerate(df['scen'].drop_duplicates()):

    ax = fig.add_subplot(2,2,scen_num+1)

    df1 = df.loc[df['scen']==scen]

    bar1 = ax.bar(ind, df1['n2o_est'], width, label='meas.', color='rosybrown')
    bar2 = ax.bar(ind + width, df1['n2o_sim'], width, label='sim.', color='lemonchiffon')

    ax.set_title(scen)
    ax.set_xticks(ind + width)
    ax.set_xticklabels(df1['year'].values.tolist())
    
# legend
fig.legend(
    (bar1, bar2),
    ('meas.','sim.'), 
    'lower center', 
    ncol=2,
    bbox_to_anchor=[0.5,0])

# tight layout
# plt.tight_layout()

plt.savefig('test.png')

# close
plt.close()