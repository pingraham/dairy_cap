import os, sys
from os.path import join as opj
import pandas as pd
from db import read_db
import dnd
import ags_network

# global variables for network paths
PROJECT = 'dairy_cap'
PATH    = ags_network.paths(PROJECT)

# database name
DATABASE = 'database.xlsx'

# task name
TASK = 'task1b_marshfield'

def main():

    ''' SET PATHS '''
    path_db = opj(
        PATH.projects,
        TASK,
        'calibration',
        DATABASE)
    
    # print 'WRITING TO PROJECTS...'
    path_inputs = opj(
        PATH.scratch,
        # PATH.projects,
        TASK,
        # 'calibration',
        'inputs')

    ''' READ IN DATABASE '''
    print 'read-in database'
    data_d = read_db(path_db)

    ''' WRITE INPUTS '''
    # create batch list
    batch_l = []

    print 'create inputs'
    for scen in sorted(data_d['scenario'].keys()):
        
        scen = int(scen)

        # simulate this?
        if data_d['scenario'][scen]['simulate'] == 0: continue
        
        for site in sorted(data_d['site'].keys()):
            
            site = int(site)
            
            # simulate this?
            if data_d['site'][site]['simulate'] == 0: continue

            print '   ', scen, site

            # instantiate Dnd_File object
            dnd_file = dnd.Dnd_File(PROJECT, scen, site, data_d)

            # set site name
            dnd_name = dnd_file.dnd_name(filename=True)

            # set input file path
            path_scen = os.path.join(
                path_inputs,
                str(scen))
            if not os.path.exists(path_scen):
                os.mkdir(path_scen)

            path_dnd = os.path.join(path_scen,dnd_name)

            # write DND file
            dnd_file.write_dnd(path_dnd)

            # get local (C) path
            path_dnd_c = dnd_file.dnd_path()

            # append path to batch list
            batch_l.append(path_dnd_c)

    # write batch file
    print 'create batch files'
    path_batch = os.path.join(path_inputs,'batch.txt')
    batch_file = open(path_batch,'wb')
    batch_file.write('{0}\r\n'.format(len(batch_l)))
    for line in batch_l:
        batch_file.write('{0}\r\n'.format(line))
    batch_file.close()

    # write batch file for all Windows instances
    w = ['oxbow']

    batch_length =    len(batch_l) / len(w)
    batch_remainder = len(batch_l) % len(w)

    for filenum, file in enumerate(w):

        start = batch_length * filenum
        end = batch_length * (filenum+1)
        if file == w[-1]:
            end = end + batch_remainder

        dnd_l = batch_l[start:end]
        
        path_batch = os.path.join(path_inputs,'batch_' + file + '.txt')
        batch_file = open(path_batch,'wb')
        batch_file.write('{0}\r\n'.format(len(dnd_l)))
        for line in dnd_l:
            batch_file.write('{0}\r\n'.format(line))
        batch_file.close()
    
if __name__ == '__main__':
    main()
