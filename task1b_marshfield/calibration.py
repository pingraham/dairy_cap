
import os, sys
from os.path import join as opj
import convert
import glob
import math
import pandas as pd
import numpy as np
import datetime as dt
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import ags_network

# global variables for network paths
PROJECT = 'dairy_cap'
PATH    = ags_network.paths(PROJECT)

# database name
DATABASE = 'database.xlsx'

# task name
TASK = 'task1b_marshfield'

def tdd():

    ''' GET WEATHER '''
    # read in weather spreadsheet as DF
    path_xlsx = opj(
        PATH.projects,
        TASK,
        'management',
        'Marshfield Weather-from_Curtis.xlsx')
    df_weather = pd.read_excel(
        path_xlsx,
        sheetname='Sheet1',
        skiprows=[0])

    # calculate date
    df_weather['date'] = pd.to_datetime(
        df_weather['Y'] * 10000 + df_weather['M'] * 100 + df_weather['D'],
        format='%Y%m%d')

    # calculate mean T
    df_weather['mean'] = (df_weather['TMX'] + df_weather['TMN']) / 2.0

    # calculate DD
    df_weather['dd'] = 0.
    df_weather.loc[df_weather['mean'] > 0., 'dd'] = df_weather['mean']

    ''' GET MANAGEMENT DATA '''
    # read-in management data
    path_db = os.path.join(
        PATH.projects,
        TASK,
        'calibration',
        DATABASE)
    df_field = pd.read_excel(path_db, sheetname='field', skiprows=[0])

    # keep key fields
    df_field = df_field[[
        'scenario',
        'scenario_name',
        'year_cal',
        'crop_year',
        'crop_1_name',
        'crop_1_plant_date',
        'crop_1_harv_date',
        'cut_1_date',
        'cut_2_date',
        'cut_3_date']]

    # initialize list of lists
    lol = [[
        'scenario',
        'scenario_name',
        'crop',
        'year',
        'date_plant',
        'date_harvest',
        'tdd']]

    # loop through management records and relate to weather
    for mgt_rec, data in df_field.iterrows():
        scenario            = data['scenario']
        scenario_name       = data['scenario_name']
        year                = data['year_cal']
        crop_year           = data['crop_year']
        crop_1_name         = data['crop_1_name']
        crop_1_plant_date   = data['crop_1_plant_date']
        crop_1_harv_date    = data['crop_1_harv_date']
        cut_1_date          = data['cut_1_date']
        cut_2_date          = data['cut_2_date']
        cut_3_date          = data['cut_3_date']

        # handle alfalfa
        if crop_1_name == 'fallow' or crop_1_name == 'alfalfa':
            
            crop_1_plant_date = dt.date(year, 5, 16)    # based on observed simulated start-of-growth
            cut_3_date = dt.date(year, cut_3_date.month, cut_3_date.day)
            crop_1_harv_date = cut_3_date

        df_season = df_weather.loc[
            (df_weather['date'] >=  crop_1_plant_date) &
            (df_weather['date'] <=  crop_1_harv_date)
                ].copy(deep=True)

        tdd = df_season['dd'].sum()

        # record data
        lol.append([scenario, scenario_name, crop_year, year, crop_1_plant_date, crop_1_harv_date, tdd])

    # tie 'em together
    df = pd.DataFrame(lol[1:], columns=lol[0])

    # write it out
    path_tdd = opj(
        PATH.projects,
        TASK,
        'calibration',
        'results',
        'tdd.csv')
    df.to_csv(path_tdd, index=False)

    # get minimum TDD by system and crop
    index_fields = [
        # 'scenario',
        # 'scenario_name',
        'crop']

    df_min = df.groupby(index_fields).min()[['tdd']]

    path_min = opj(
        PATH.projects,
        TASK,
        'calibration',
        'results',
        'tdd_min.csv')
    df_min.to_csv(path_min)

    return

def analyze_climate():
    
    ''' GET WEATHER '''
    # read in weather spreadsheet as DF
    path_xlsx = opj(
        PATH.projects,
        TASK,
        'management',
        'Marshfield Weather-from_Curtis.xlsx')
    df_weather = pd.read_excel(
        path_xlsx,
        sheetname='Sheet1',
        skiprows=[0])

    # calculate date
    df_weather['date'] = pd.to_datetime(
        df_weather['Y'] * 10000 + df_weather['M'] * 100 + df_weather['D'],
        format='%Y%m%d')

    # calculate mean T
    df_weather['mean'] = (df_weather['TMX'] + df_weather['TMN']) / 2.0

    # calculate DD
    df_weather['dd'] = 0.
    df_weather.loc[df_weather['mean'] > 0., 'dd'] = df_weather['mean']

    years = [y for y in range(2000, 2014+1)]

    rows = []    
    for year in years:
        
        df = df_weather.loc[df_weather['Y'] == year].copy(deep=True)
        
        tmean = df['mean'].mean()
        ppt = df['PRCP'].sum()
        
        # growing season
        df = df_weather.loc[
            (df_weather['date'] > dt.date(year, 4, 30)) & 
            (df_weather['date'] < dt.date(year, 10, 1))]
        
        tmean_gs = df['mean'].mean()
        ppt_gs = df['PRCP'].sum()
        
        row = [year, tmean, ppt, tmean_gs, ppt_gs]
        rows.append(row)

    df_annual = pd.DataFrame(rows, columns=['year','tmean','ppt','tmean_gs', 'ppt_gs'])

    path_clim = opj(
        PATH.projects,
        TASK,
        'calibration',
        'results',
        'climate_analysis.csv')
    df_annual.to_csv(path_clim, index=False)

    return
    
def annual_yield():

    ''' SET PATHS '''
    path_db = opj(
        PATH.projects,
        TASK,
        'calibration',
        DATABASE)

    path_results = opj(
        PATH.projects,
        TASK,
        'calibration',
        'results')

    path_batch = opj(
        path_results,
        'Batch')

    ''' READ IN DATABASE '''
    data_dict = {}
    sheets = [
        'field',
        'scenario']
    for sheet in sheets:
        data_dict[sheet] = pd.read_excel(
            path_db,
            sheetname=sheet,
            skiprows=[0])
    df_field = data_dict['field']
    df_scen = data_dict['scenario']

    # replace "na" values in yield
    df_field['crop_1_yield'].replace(to_replace="na", value=np.nan, inplace=True)

    ''' READ-IN RESULTS '''
    path_glob = opj(
        path_batch,
        'Case*',
        'Multi_year_summary.csv')
    globber = glob.glob(path_glob)

    results = []
    for path_file in globber:

        # get the directory name
        path_dir,filename = os.path.split(path_file)
        dirname = os.path.basename(path_dir)

        case, scen, site = dirname.split('-')
        scen, site = int(scen), int(site)

        # extract results as DataFrame
        df = pd.read_csv(
            path_file,
            skiprows=[0,1,3,4])
        df.columns = [c.strip() for c in df.columns]

        # add scenario info
        df['scen'] = scen
        df['scen_name'] = df_scen.loc[df_scen['scenario']==scen]['name'].iloc[0]
        df['site'] = site

        # extract database field subset
        df_subset = df_field.loc[df_field['scenario'] == scen].copy(deep=True)

        subset_fields = [
            'scenario',
            'year_sim',
            'year_cal',
            'crop_year',
            'crop_1_yield',
            'crop_1_yield_c',
            'crop_1_residue']

        # join field info to results
        df = df.join(
            df_subset[subset_fields].set_index('year_sim'),
            on='Year')

        # append DF to list
        results.append(df)

    ''' CONCATENATE RESULTS '''
    df = pd.concat(results)
    del df['Year']
    df.reset_index(inplace=True)

    ''' SET-UP YIELD '''
    # create a measured yield field
    df['yield_meas'] = df['crop_1_yield_c']

    # create a simulated yield field
    df['yield_sim'] = df['GrainC1']

    # set silage corn yield
    df.loc[
        (df['Crop1'] == 1) &
        (df['year_cal'] != 2010),
            'yield_sim'] = df['GrainC1'] + ((df['LeafC1'] + df['StemC1']) * (1. - df['crop_1_residue']))

    # set alfalfa yield to "Cut_CropC"
    df.loc[
        df['crop_year'] == 'alfalfa',
            'yield_sim'] = df['Cut_CropC']

    ''' MAKE IT PRETTY '''
    df.sort(['scen', 'site', 'year_cal'], inplace=True)

    # write it out
    path_file = os.path.join(
        path_results,
        'annual_yield.csv')
    df.to_csv(path_file, index=False)

    ''' CALCULATE RELATIVE RMSE '''
    
    # set calibration years (2008-2014 - 2015 is validation year)
    df = df.loc[df['year_cal'].isin(range(2008, 2014+1))].copy(deep=True)

    # RMSE
    df['sq'] = (df['yield_meas'] - df['yield_sim'])**2.

    df_ss = df.groupby(['scen', 'site','crop_year']).mean()[['yield_meas', 'sq']]
    df_ss['rmse'] = df_ss['sq']**0.5 # square root
    df_ss['rmse_rel'] = df_ss['rmse'] / df_ss['yield_meas']

    # df_corr = df.groupby(['scen', 'site','crop_year'])[['yield_meas', 'yield_sim']].corr()
    # df_corr = df_corr.loc[df_corr['yield_meas'] != 1.0000]

    # df_corr = df_corr[['yield_meas']]#.reset_index(inplace=True)
    # df_corr.reset_index(inplace=True)
    # del df_corr['level_3']
    # df_corr.set_index(['scen', 'site', 'crop_year'], inplace=True)

    # df_corr.columns = ['r']

    # df_ss = df_ss.join(
        # df_corr)

    df_ss = df_ss[['yield_meas', 'rmse', 'rmse_rel']]#, 'r']]

    # write out
    path_rmse = opj(
        path_results,
        'rmse_rel.csv')

    df_ss.to_csv(path_rmse)

    return df

def graph_measured_yield():

    ### IMPORT LIBRARIES ###
    from graph import basic_timeline
    
    ### SET PATHS ###
    path_cal = opj(
        PATH.projects,
        TASK,
        'calibration')
    path_db = opj(
        path_cal,
        DATABASE)

    ### READ IN DATABASE ###
    df_field = pd.read_excel(
            path_db,
            sheetname='field',
            skiprows=[0])

    df_field = df_field.loc[df_field['year_cal'].isin(range(2008, 2014+1))].copy(deep=True)
            
    for crop in ['alfalfa', 'corn']:

        y = {}
        for scenario in set(df_field['scenario_name']):
            df_scen = df_field.loc[df_field['scenario_name']==scenario].copy(deep=True)
            df_scen.reset_index(inplace=True)
            df_scen.loc[df_scen['crop_year']!=crop, 'crop_1_yield_c'] = np.nan
            y[scenario] = df_scen['crop_1_yield_c']

        y_values = []
        
        colors = ['b', 'g', 'r', 'c']
        
        for linenum,line in enumerate(y):

            for v in y[line]:   
                if not np.isnan(v):
                    y_values.append(v)
            
            # markers
            plt.plot(
                df_scen['year_cal'], # X
                y[line],             # Y
                linestyle='None',
                marker='o',
                markersize=5,
                markerfacecolor=colors[linenum],
                markeredgecolor='black')
            
            # lines
            plt.plot(
                df_scen['year_cal'], # X
                y[line].interpolate(method='linear'), # Y
                label=line,
                linestyle='dashed',
                color=colors[linenum],
                marker=None)
        
        # this keeps Matplotlib from fucking up the axes
        # why do you need this?  completely. undocumented.
        ax = plt.gca()
        try:
            ax.get_xaxis().get_major_formatter().set_useOffset(False)
        except:
            pass

        # title
        plt.title('measured yield for ' + crop)

        # X-axis
        if len(df_scen['year_cal']) < 20:
            plt.xticks(df_scen['year_cal'])
        else:
            plt.xticks(rotation='vertical')
        plt.xlabel('year')
        try:
            plt.xlim(
                min(df_scen['year_cal']) - 1,
                max(df_scen['year_cal']) + 1)
        except:
            pass

        # Y-axis
        plt.ylabel('yield (kgC/ha/y)')
        plt.ylim(
            min(y_values)*0.95, # set min to 5% below minimum Y value
            max(y_values)*1.05) # set max to 5% above maximum Y value
        
        # legend
        plt.legend(loc='best')
        
        # grid
        plt.grid()

        # export
        plt.tight_layout()
        
        path_export = opj(
            path_cal, 
            'results', 
            'charts',
            'measured_yield_' + crop + '.png')
        
        plt.savefig(path_export)

        # close
        plt.close()
            
    return

def graph_simulated_yield():

    ### IMPORT LIBRARIES ###
    from graph import basic_timeline
    
    ### SET PATHS ###
    path_cal = opj(
        PATH.projects,
        TASK,
        'calibration')
    path_results = opj(
        path_cal,
        'results')

    ### READ IN YIELD DATA ###
    path_yield = opj(
        path_results,
        'annual_yield.csv')
    df_yield = pd.read_csv(path_yield)

    
    df_yield = df_yield.loc[df_yield['year_cal'].isin(range(2008, 2014+1))].copy(deep=True)
    
    for crop in ['alfalfa', 'corn']:
        
        for site in set(df_yield['site']):

            for scenario in set(df_yield['scen_name']):

                df_scen = df_yield.loc[
                    (df_yield['scen_name']==scenario) & 
                    (df_yield['site']==site) 
                        ].copy(deep=True)
                df_scen.loc[df_scen['crop_year']!=crop, 'yield_meas'] = np.nan
                df_scen.loc[df_scen['crop_year']!=crop, 'yield_sim'] = np.nan

                y = {}
                y['measured'] = df_scen['yield_meas']
                y['simulated'] = df_scen['yield_sim']
                
                basic_timeline(
                    x = df_scen['year_cal'],
                    y = y,
                    path_export = opj(
                        path_cal, 
                        'results', 
                        'charts',
                        'measured_vs_simulated_' + crop + '_' + scenario + '_' + str(site) + '.png'),
                    title='measured vs. simulated yield for ' + crop + ', ' + scenario + ' treatment', 
                    xlabel='year',
                    ylabel='yield (kgC/ha/y)',
                    markers=True)
                
                # scatter plot
                plt.scatter(
                    df_scen['yield_sim'],
                    df_scen['yield_meas'])
                plt.grid()
                plt.tight_layout()
                plt.savefig(
                    opj(
                        path_results,
                        'charts',
                        'measured_vs_simulated_scatter_' + crop + '_' + scenario + '_' + str(site) + '.png'))
                plt.close()

    return

def combine_meas_sim():

    ### IMPORT LIBRARIES ###
    from collections import OrderedDict as od

    ### SET PATHS ###
    path_cal = opj(
        PATH.projects,
        TASK,
        'calibration')
    path_mgt = opj(
        PATH.projects,
        TASK,
        'management')
    path_results = opj(
        path_cal,
        'results')

    ### READ-IN MEASUREMENT DATA ###
    print '    read-in data...'
    path_meas = opj(
        path_mgt,
        'Marshfield_observations_1.1.xlsx')
    df_meas = pd.read_excel(
        path_meas,
        sheetname='daily obs final')
    df_meas['treatment'] = df_meas['treatment'].str.lower()
    
    attributes = od()
    attributes['n2o']        = 'n2o (g N/ha/day)'
    attributes['soil_temp']  = 'Soil Temp (C) 5cm'
    attributes['soil_moist'] = 'Soil Moisture (%) 5cm'
    attributes['ch4']        = 'ch4 (gC/ha/day)'
    attributes['co2']        = 'co2 (gC/ha/day)'
    attributes['nh4']        = 'nh4 (mgN/kg soil)'
    attributes['no3']        = 'no3 (mgN/kg soil)'
    
    for att in attributes:
        meas_field = attributes[att]
        df_meas[att + '_meas'] = df_meas[meas_field]
        del df_meas[meas_field]

    # get scenario info
    scenarios = od()
    scenarios[1] = 'cont'
    scenarios[2] = '120n'
    scenarios[3] = 'brsf'
    scenarios[4] = 'bric'

    years = od()
    years[14] = 2013
    years[15] = 2014
    
    # glob results
    globber = glob.glob(
        opj(
            path_results, 
            'Batch', 
            'Case*-*'))

    dfs = []
    
    for path_case in globber:
        
        casename = os.path.basename(path_case)
        case, scen, site = casename.split('-')
        scen, site = int(scen), int(site)

        for year in years:
            
            ### SOIL N ###
            path_n = opj(
                path_case,
                'Day_SoilN_' + str(year) + '.csv')
            
            df_n = pd.read_csv(
                path_n,
                skiprows=[0,1,3,4])
            df_n.set_index('Day', inplace=True)
            
            df_n = df_n[[
                'NH4+',
                'NO3-',
                'N2O-flux']]
            
            ### SOIL C ###
            path_c = opj(
                path_case,
                'Day_SoilC_' + str(year) + '.csv')
            
            df_c = pd.read_csv(
                path_c,
                skiprows=[0])

            df_c.set_index('Day', inplace=True)
            
            df_c = df_c[[
                'Eco-respiration',
                'CH4-flux']]
            
            ### SOIL WEATHER ###
            path_w = opj(
                path_case,
                'Day_SoilClimate_' + str(year) + '.csv')
            
            df_w = pd.read_csv(
                path_w,
                skiprows=[0, 1, 3])
            df_w.set_index('Day', inplace=True)

            df_w['bd'] = (df_w['1cm.5'] + df_w['10cm.5']) / 2.0

            df_w = df_w[[
                'bd',
                '5cm',         # soil temp
                '5cm.1']]      # soil moisture, wfps

            df_w.columns = ['bd','soil_temp','soil_moist']
            
            ### COMBINE ###
            df = pd.concat([df_n, df_c, df_w], axis=1)

            ### ADD OTHER INFO ###
            df['year'] = years[year]
            df['date'] = df['year'] * 1000 + df.index
            df['date'] = pd.to_datetime(df['date'], format='%Y%j')
            
            df['scenario'] = scen
            df['scen_name'] = scenarios[scen]
            
            df['site'] = site
            
            ### FORMAT ###
            df.reset_index(inplace=True)

            df.columns = [
                'doy',
                'nh4_sim',
                'no3_sim',
                'n2o_sim',
                'co2_sim',
                'ch4_sim',
                'bd_sim',
                'soil_temp_sim',
                'soil_moist_sim',
                'year',
                'date',
                'scenario',
                'scen_name',
                'site']
            
            df = df[[
                'scenario',
                'scen_name',
                'site',
                'year',
                'date',
                'doy',
                'bd_sim',
                'n2o_sim',
                'soil_temp_sim',
                'soil_moist_sim',
                'co2_sim',
                'ch4_sim',
                'nh4_sim',
                'no3_sim']]

            ### APPEND ###
            dfs.append(df)

    ### CONCATENATE ###
    print '    process data...'
    df = pd.concat(dfs)
    df.reset_index(inplace=True)

    ### JOIN MEASURED ###
    df = df.join(
        df_meas.set_index(['treatment','date']),
        on=['scen_name','date'])

    ### CONVERT MEASURED UNITS ###
    
    # N2O: gN/ha to kgN/ha
    df['n2o_meas'] = df['n2o_meas'] * 1000.**-1.
    
    # soil moisture: VWC% to WFPS - using BD
    df['soil_moist_meas'] = (df['soil_moist_meas'] / 100.) / (1.-(df['bd_sim']/2.65))
    
    # soil moisture: VWC% to WFPS - using porosity
    # df['soil_moist_meas'] = (df['soil_moist_meas'] / 100.) / 0.50487

    # CO2: gC/ha to kgC/ha
    df['co2_meas'] = df['co2_meas'] * 1000.**-1.
        
    # CH4: gC/ha to kgC/ha
    df['ch4_meas'] = df['ch4_meas'] * 1000.**-1.
    
    # NH4: mgN/kg soil to kgN/ha
    df['soil_weight'] = df['bd_sim'] * (5.*10.**8.) * 1000.**-1. # bd (g/cm3) * depth (cm) * cm2/ha * kg/g = kg/ha
    df['nh4_meas'] = (df['nh4_meas'] * 10.**6.) / df['soil_weight']
    
    # NO3: mgN/kg soil to kgN/ha
    df['no3_meas'] = (df['no3_meas'] * 10.**6.) / df['soil_weight']
    del df['soil_weight']
    
    # dates where measurements were made 
    dr2013 = pd.date_range(dt.date(2013,6,13), dt.date(2013,11,15))
    dr2014 = pd.date_range(dt.date(2014,4,15), dt.date(2014,12,15))
    df['inseason'] = False
    df.loc[df['date'].isin(dr2013), 'inseason'] = True
    df.loc[df['date'].isin(dr2014), 'inseason'] = True

    ### GAP-FILL CO2 AND CH4 WITH LINEAR INTERPOLATION ###
    print '    gap-filling C...'
    df.loc[df['inseason'], 'co2_est'] = df['co2_meas'].interpolate()
    df.loc[df['inseason'], 'ch4_est'] = df['ch4_meas'].interpolate()

    ### GAP-FILL MEASURED N2O WITH ATTENUATION ###
    print '    gap-filling N...'

    # initialize estimated N2O column
    df['n2o_est'] = np.nan

    # initialize estimate N2O variable
    n2o_est = 0.

    # loop through rows
    i_n2o_meas = df.columns.tolist().index('n2o_meas') + 1
    # for i,data in df.loc[df['inseason']].iterrows():
    for t in df.loc[df['inseason']].itertuples():

        # extract measured N2O
        i = t[0]
        n2o_meas = t[i_n2o_meas]

        # if N2O was not measured on this day...
        if np.isnan(n2o_meas):

            # ...calculate estimated N2O based on 
            # prior day estimated (or actual) N2O
            n2o_est = math.exp(1.)**-0.223 * n2o_est
            # incidentially this is an exponential decay
            # function based on data from Michel Cavigelli

        # if N2O was measured on this day...
        else:

            # set estimated N2O to measured N2O
            n2o_est = n2o_meas

        # populate DF with estimated value
        df.loc[i, 'n2o_est'] = n2o_est

    # export
    print '    write results...'
    path_df = opj(
        path_results,
        'measured_vs_simulated.csv')
    df.to_csv(path_df, index=False)

    return

def graph_daily_meas_sim():
    
    ### SET PATHS ###
    path_cal = opj(
        PATH.projects,
        TASK,
        'calibration')
    path_mgt = opj(
        PATH.projects,
        TASK,
        'management')
    path_results = opj(
        path_cal,
        'results')
    path_charts = opj(
        path_results,
        'charts')
    
    ### READ-IN MEASURED VS. SIMULATED ###
    path_mvs = opj(
        path_results,
        'measured_vs_simulated.csv')
    df_mvs = pd.read_csv(path_mvs)

    df_mvs['date'] = pd.to_datetime(df_mvs['date'])

    ### LOOP THROUGH SITES (SOILS) ###
    for site in df_mvs['site'].drop_duplicates():

        ### LOOP THROUGH ATTRIBUTES ###
        
        atts = {
            'n2o':        'kgN/ha/d',
            # 'soil_temp':  'deg. C',
            'soil_moist': 'WFPS',
            # 'co2':        'kgC/ha/d',     
            # 'ch4':        'kgC/ha/d',
            'nh4':        'kgN/ha/d',
            'no3':        'kgN/ha/d'}
        
        for att in atts:
            
            ### CREATE FIGURE ###
            fig = plt.figure(figsize=(13, 9))
            
            ### LOOP THROUGH SCENARIOS
            for scen_num, scen in enumerate(df_mvs['scenario'].drop_duplicates()):
                
                position = scen_num + 1
                
                df_grapher = df_mvs.loc[
                    (df_mvs['site'] == site) & 
                    (df_mvs['scenario'] == scen)
                        ].copy(deep=True)
                
                scen_name = df_grapher['scen_name'].iloc[0]

                print '    ', site, scen, att
                
                units = atts[att]

                att_sim = att + '_sim'
                att_meas = att + '_meas'
                
                ### ADD SUBPLOT ###
                ax = fig.add_subplot(2,2,position)
                
                ### MAKE DATA FOR PLOT ###
                mean = pd.rolling_mean(df_grapher[att_sim], 7)
                sd   = pd.rolling_std(df_grapher[att_sim], 7)
                low  = mean - sd * 1.96
                high = mean + sd * 1.96

                # plot measured daily data
                # plt.plot(
                ax.plot(
                    df_grapher['date'], 
                    df_grapher[att_meas],
                    linestyle='None',
                    marker='o',
                    color='orange',
                    markersize=5)
                
                # plot simulated raw daily data
                # plt.plot(
                ax.plot(
                    df_grapher['date'], 
                    df_grapher[att_sim],
                    color='darkgray')
                
                # plot simulated 7-day mean
                # plt.plot(
                ax.plot(
                    df_grapher['date'], 
                    mean,
                    color='black')

                # plot simulated 7-day low
                # plt.plot(
                ax.plot(
                    df_grapher['date'], 
                    low,
                    color='grey')
                
                # plot simulated 7-day high
                # plt.plot(
                ax.plot(
                    df_grapher['date'], 
                    high,
                    color='grey')
                
                # plt.fill_between(df_grapher['date'].tolist(), high, low, color='grey')
                ax.fill_between(df_grapher['date'].tolist(), high, low, color='grey')

                # title
                # plt.title(
                title = (
                    att + 
                    ', scen: ' + scen_name + 
                    ' (' + str(scen) + ')' + 
                    ', site: ' + str(site))
                ax.set_title(title, size=10)
                
                # X-axis
                if position in [3,4]:
                    for label in ax.get_xticklabels():
                        label.set_rotation('vertical')
                        label.set_size(8)
                        label.set_horizontalalignment('center')
                else:
                    ax.set_xticklabels('')

                # Y-axis
                # plt.ylabel(units)
                for label in ax.get_yticklabels():
                    label.set_size(8)
                
                if position in [1,3]:
                    ax.set_ylabel(units, size=9)
                # plt.ylim(0.0, plt.ylim()[1])

                
                if att in ['nh4', 'no3']:
                    ax.set_ylim(0,120)

                if att == 'n2o':
                    ax.set_ylim(0,2)
                    
                
                # grid
                # plt.grid()
                ax.grid()

            # tight layout
            # plt.tight_layout()
            fig.tight_layout()
                
            # export
            png_name = (
                'mvs_' + 
                att + '_' + 
                # str(scen) + '_' + 
                # scen_name + '_' +
                str(site) + 
                '.png')
            plt.savefig(opj(path_charts, png_name))

            # close
            plt.close()
        
    return

def graph_annual_meas_sim():
    
    ### SET PATHS ###
    path_cal = opj(
        PATH.projects,
        TASK,
        'calibration')
    path_mgt = opj(
        PATH.projects,
        TASK,
        'management')
    path_results = opj(
        path_cal,
        'results')
    path_charts = opj(
        path_results,
        'charts')
    
    ### READ-IN MEASURED VS. SIMULATED ###
    path_mvs = opj(
        path_results,
        'measured_vs_simulated.csv')
    df_mvs = pd.read_csv(path_mvs)

    df_mvs['date'] = pd.to_datetime(df_mvs['date'])

    ### CALCULATE ANNUAL ATTRIBUTES AND GRAPH ###

    ### LOOP THROUGH SITES (SOILS) ###
    print '     graphing...'
    for site in df_mvs['site'].drop_duplicates():
        
        ### LOOP THROUGH ATTRIBUTES ###
        atts = {
            'n2o': 'kgN/ha/d',
            'co2': 'kgC/ha/d',     
            'ch4': 'kgC/ha/d'}
        
        for att in atts:
            
            ### CREATE FIGURE ###
            fig = plt.figure(figsize=(13, 9))
            ind = np.arange(2)
            width = 0.35
            
            ### LOOP THROUGH SCENARIOS
            for scen_num, scen in enumerate(df_mvs['scenario'].drop_duplicates()):

                position = scen_num + 1

                df1 = df_mvs.loc[
                    (df_mvs['scenario']==scen) & 
                    df_mvs['inseason']].copy(deep=True)
                df1 = df1.groupby(['scen_name','year'], as_index=False).sum()[['scen_name', 'year', att+'_est', att+'_sim']]
                
                scen_name = df1['scen_name'].iloc[0]
            
                ax = fig.add_subplot(2,2,scen_num+1)

                bar1 = ax.bar(ind, df1[att+'_est'], width, label='meas.', color='rosybrown')
                bar2 = ax.bar(ind + width, df1[att+'_sim'], width, label='sim.', color='lemonchiffon')

                ax.set_title(scen)
                ax.set_xticks(ind + width)
                ax.set_xticklabels(df1['year'].values.tolist())
                
            # legend
            fig.legend(
                (bar1, bar2),
                ('meas.','sim.'), 
                'lower center', 
                ncol=2,
                bbox_to_anchor=[0.5,0])

            # tight layout
            plt.tight_layout()

            # export
            path_png = opj(
                path_charts,
                'mvs_annual_' + att + '_' + str(site) + '.png')
            
            plt.savefig(path_png)

            # close
            plt.close()

    return

if __name__ == '__main__':

    print 'TDD stats'
    tdd()

    print 'analyze climate'
    analyze_climate()

    print 'get annual yield'
    annual_yield()

    print 'graph measured yield'
    graph_measured_yield()
    
    print 'graph measured vs. simulated yield'
    graph_simulated_yield()

    # COMBINE=False
    COMBINE=True
    if COMBINE:
        print 'combine measured and simulated'
        combine_meas_sim()
    
    print 'graph daily soil measurements'
    graph_daily_meas_sim()
    
    print 'graph annual soil measurements'
    graph_annual_meas_sim()
